package itgeeks.info.itgeeksmarket.General;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;

import java.util.Locale;
import java.util.prefs.Preferences;

public class LocaleHelper {
    private static final String SELECTED_LANGUAGE = "Locale.Helper.Selected.Language";

    public static Context onAttach(Context context) {
        String lang = getPersistedData(context, Locale.getDefault().getLanguage());
        return setLocale(context, lang);
    }

    public static Context onAttach(Context context, String DefaultLanguage) {
        String lang = getPersistedData(context, DefaultLanguage);
        return setLocale(context, lang);
    }

    public static Context setLocale(Context context, String lang) {
        persiste(context, lang);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return updateResurces(context, lang);
        }
        return updateResurcesLegacy(context,lang);
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Context updateResurces(Context context, String lang) {
        Locale locale;
        if (lang.trim().equals("ar")){
             locale = new Locale(lang,"EG");
        }else {
             locale = new Locale(lang,"US");
        }
        Locale.setDefault(locale);

        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        configuration.setLayoutDirection(locale);

        return context.createConfigurationContext(configuration);
    }
    @TargetApi(Build.VERSION_CODES.N)
    private Context updateResourcesLocale(Context context, Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        configuration.setLocale(locale);
        return context.createConfigurationContext(configuration);
    }
    @SuppressWarnings("Deprecation")
    private static Context updateResurcesLegacy(Context context, String lang) {
        Locale locale;
        if (lang.trim().equals("ar")){
            locale = new Locale(lang,"EG");
        }else {
            locale = new Locale(lang,"US");
        }
        Locale.setDefault(locale);

        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        configuration.locale = locale;
        configuration.setLayoutDirection(locale);

        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
        return context;
    }

    private static void persiste(Context context, String lang) {
        SharedPreferences sharedPrefrances = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefrances.edit();
        editor.putString(SELECTED_LANGUAGE, lang);
        editor.apply();
    }

    private static String getPersistedData(Context context, String language) {
        SharedPreferences sharedPrefrances = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefrances.getString(SELECTED_LANGUAGE, language);
    }

}
