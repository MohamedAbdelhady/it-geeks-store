package itgeeks.info.itgeeksmarket.General

object PopularKeys {

    //    Apis Actions
    const val REGISTER = "register"
    const val LOGIN_ACTION = "login"
    const val FORGET_PASSWORD = "forgetPassword"
    const val GET_ALL_PRODUCTS_HOME_PAGE = "getAllProducts"
    const val GET_SALE_TODAY = "getSaleToday"
    const val SET_FAVORITE_PRODUCT = "setFavoriteProduct"
    const val GET_FAVORITE_PRODUCT = "getFavoriteProduct"
    const val REMOVE_FAVORITE_PRODUCT = "removeFavoriteProduct"
    const val GET_NEW_PRODUCT = "getNewProducts"
    const val GET_MY_ORDERS = "getMyOrders"
    const val GET_MY_ORDER_DETAILS = "getMyOrderDetails"
    const val SET_USER_REVIEW = "setUserReview"
    const val GET_PRODUCT_BY_ID = "getProductById"
    const val GET_PRODUCT_BY_CATEGORY_SLUG = "getProductByCatSlug"
    const val GET_TOP_SELECTION = "getTopSelections"
    const val GET_ALL_PRODUCT_REVIEWS = "getAllProductReviews"
    const val GET_ALL_PRODUCT_RELATED = "getAllProductRelated"
    const val UPDATE_USER_DATA = "updateUserData"
    const val SET_USER_ADDRESS = "setUserAddress"
    const val GET_COLOR_SIZE_LISTS = "getColorSizeLists"
    const val APPLY_COUPON = "applyCoupon"

    //    Catching products Home Page
    const val DATABASE_NAME = "homePageCaching"
    const val DATABASE_VERSION = 1
    object homePage {
        const val TABLE_NAME = "product"
        const val SLIDERS_NAME = "sliders"
        const val CATEGORIES_NAME = "categories"
        const val LATEST_NAME = "product"
    }

}