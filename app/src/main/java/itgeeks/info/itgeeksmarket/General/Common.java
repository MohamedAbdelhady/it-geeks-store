package itgeeks.info.itgeeksmarket.General;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import androidx.appcompat.app.AppCompatActivity;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;

import java.util.Locale;


public class Common {

    private static Common common;
    private Context context;
    private String Lang;

    private Common(Context context) {
        this.context = context;
    }

    public static Common getInstance(Context context) {
        if (common == null) {
            common = new Common(context);
        }
        return common;
    }

    // to change status bar color in fragments || activities if wanted
    public void changeStatusBarColor(String color, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = ((AppCompatActivity) context).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

}
