package itgeeks.info.itgeeksmarket.Models

class term_colors_sizes(
    term_id: Int?,
    name: String?,
    slug: String?,
    type: String?,
    parent: Int?,
    status: Int?,
    created_at: String?,
    updated_at: String?,
    color: String?
) {
    var term_id = term_id
    var name = name
    var slug = slug
    var type = type
    var parent = parent
    var status = status
    var created_at = created_at
    var updated_at = updated_at
    var color = color
}