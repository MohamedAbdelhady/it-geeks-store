package itgeeks.info.itgeeksmarket.Models

class orderDatails(
    order_id: Int,
    payment_method: String,
    total: String,
    subtotal: String,
    discount: String,
    status: String,
    shipping_cost: String,
    address: String,
    name: String,
    phone: String,
    date: String
) {
    var order_id = order_id
    var payment_method = payment_method
    var total = total
    var subtotal = subtotal
    var discount = discount
    var status = status
    var shipping_cost = shipping_cost
    var address = address
    var name = name
    var phone = phone
    var date = date
}
