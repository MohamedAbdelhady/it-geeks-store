package itgeeks.info.itgeeksmarket.Models

import java.io.Serializable

class categories(
    name: String?,
    slug: String?,
    products_count: Int?,
    image: String?,
    category_image: String?,
    sub: ArrayList<categories>?
) : Serializable {
    var name = name
    var slug = slug
    var products_count = products_count
    var image = image
    var category_image = category_image
    var sub = sub
}