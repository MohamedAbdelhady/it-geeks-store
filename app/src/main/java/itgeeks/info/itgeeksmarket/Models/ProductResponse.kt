package itgeeks.info.itgeeksmarket.Models

class ProductResponse(
    var products: List<product>?,
    var singleProduct: singleProduct?,
    var orders: List<orders>?,
    var message: String?,
    var status: Boolean?,
    var per_page: Int?,
    var last_page: Int?,
    var total: Int?,
    var order : order
)


