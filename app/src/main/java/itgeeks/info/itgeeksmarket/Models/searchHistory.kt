package itgeeks.info.itgeeksmarket.Models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class searchHistory(
    id: Int?,
    searchValue: String?
) {
    @PrimaryKey
    @ColumnInfo
    var id = id;
    @ColumnInfo
    var searchValue = searchValue;
}