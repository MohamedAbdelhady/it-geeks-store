package itgeeks.info.itgeeksmarket.Models

class checkCoupon(
    message: String,
    status: Boolean,
    is_coupon_applyed: Boolean,
    coupon_max_amount: String,
    coupon_min_amount: String,
    coupon_type: String,
    coupon_amount: String
) {
    var message = message
    var status = status
    var is_coupon_applyed = is_coupon_applyed
    var coupon_max_amount = coupon_max_amount
    var coupon_min_amount = coupon_min_amount
    var coupon_type = coupon_type
    var coupon_amount = coupon_amount
}
