package itgeeks.info.itgeeksmarket.Models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class recently(
    product_id: Int?,
    id: Int?,
    varition_id: Int?,
    product_type: String?,
    product_regular_price: Double?,
    product_sale_price: Double?,
    product_sku: String?,
    product_price: Double?,
    product_img: String?,
    product_title: String?,
    product_brands: String?,
    product_rate: Int?,
    product_rate_total: Int?,
    product_description:String?,
    isWished: Boolean?,
    quantity:Int?
) {
    @PrimaryKey
    @ColumnInfo
    var id = id;
    @ColumnInfo
    var varition_id = varition_id;
    @ColumnInfo
    var product_type = product_type;
    @ColumnInfo
    var product_id = product_id;
    @ColumnInfo
    var product_title = product_title;
    @ColumnInfo
    var product_img = product_img;
    @ColumnInfo
    var product_regular_price = product_regular_price;
    @ColumnInfo
    var product_sale_price = product_sale_price;
    @ColumnInfo
    var product_price = product_price;
    @ColumnInfo
    var product_sku = product_sku;
    @ColumnInfo
    var product_brands = product_brands
    @ColumnInfo
    var product_rate = product_rate
    @ColumnInfo
    var product_rate_total = product_rate_total
    @ColumnInfo
    var product_description = product_description
    @ColumnInfo
    var isWished = isWished
    @ColumnInfo
    var quantity = quantity
}