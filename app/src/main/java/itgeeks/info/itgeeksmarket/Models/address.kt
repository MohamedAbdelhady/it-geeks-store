package itgeeks.info.itgeeksmarket.Models

class address(
    account_bill_first_name: String?,
    account_bill_last_name: String?,
    account_bill_email_address: String?,
    account_bill_phone_number: String?,
    account_bill_address_line_1: String?,
    account_shipping_first_name: String?,
    account_shipping_last_name: String?,
    account_shipping_email_address: String?,
    account_shipping_phone_number: String?,
    account_shipping_address_line_1: String?
) {
    var account_bill_first_name = account_bill_first_name
    var account_bill_last_name = account_bill_last_name
    var account_bill_email_address = account_bill_email_address
    var account_bill_phone_number = account_bill_phone_number
    var account_bill_address_line_1 = account_bill_address_line_1
    var account_shipping_first_name = account_shipping_first_name
    var account_shipping_last_name = account_shipping_last_name
    var account_shipping_email_address = account_shipping_email_address
    var account_shipping_phone_number = account_shipping_phone_number
    var account_shipping_address_line_1 = account_shipping_address_line_1
}