package itgeeks.info.itgeeksmarket.Models

class orders(order_id: Int?, order_date: String?, order_total: String?, order_status: String?, order_process_key: String?) {
    var order_id = order_id
    var order_date = order_date
    var order_total = order_total
    var order_status = order_status
    var order_process_key = order_process_key
}