package itgeeks.info.itgeeksmarket.Models

class singleProduct(
    details: details?,
    sizes: List<term_colors_sizes>?,
    colors: List<term_colors_sizes>?,
    reviews: reviews?,
    related: List<product>?,
    isThereColor: Boolean?,
    isThereSize: Boolean?
) {
    var details = details
    var sizes = sizes
    var colors = colors
    var reviews = reviews
    var related = related
    var isThereColor = isThereColor
    var isThereSize = isThereSize
}