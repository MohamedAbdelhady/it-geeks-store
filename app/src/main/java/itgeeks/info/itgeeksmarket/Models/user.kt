package itgeeks.info.itgeeksmarket.Models

class user(user_id: Int?, api_token: String?, email: String?, username: String?, display_name: String?, image: String?) {
    var user_id = user_id
    var api_token = api_token
    var email = email
    var username = username
    var display_name = display_name
    var image = image
}