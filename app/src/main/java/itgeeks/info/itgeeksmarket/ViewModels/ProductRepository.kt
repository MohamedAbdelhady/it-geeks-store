package itgeeks.info.itgeeksmarket.ViewModels

import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import itgeeks.info.itgeeksmarket.Models.product
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.ProductDao
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.ProductDatabase

class ProductRepository {

    var allProduct: LiveData<List<product>>
    var productDao: ProductDao

    constructor(application: Context) {
        productDao = ProductDatabase.getInstance(application).productDao();
        allProduct = productDao.getProducts()
    }

    @WorkerThread
    fun insertProduct(product: product) {
        productDao.insertProduct(product)
    }

    @WorkerThread
    fun removeProduct(product: product) {
        productDao.removeProduct(product)
    }

    @WorkerThread
    fun removeProductsList(productList: List<product>) {
        productDao.removeProductsList(productList)
    }

    @WorkerThread
    fun getProductByID(productID: Int): LiveData<product>? {
        return productDao.getProductByID(productID)
    }

    @WorkerThread
    fun updateFavoriteIcon(isWished: Boolean, productID: Int) {
        productDao.updateFavoriteIcon(isWished, productID)
    }
    @WorkerThread
   public fun updateProductCount(productCount: Int, productID: Int) {
        productDao.updateProductCount(productCount, productID)
    }
}

//class ProductRepository {
//    private var productDao: ProductDao? = null
//    private val allProducts: LiveData<List<product>>? = null
//
//    constructor(application: Application) {
//        var cartProdutDatabase: CartProdutDatabase? = CartProdutDatabase.getInstance(application)
//        productDao = cartProdutDatabase!!.productDao()
//    }
//
//    fun insertProduct(product: product) {
//        thread {
//            productDao!!.insertProduct(product)
//        }
//    }
//
//    fun removeProduct(product: product) {
//        productDao!!.removeProduct(product)
//    }
//
//    fun removeProductsList(product: List<product>) {
//        productDao!!.removeProductsList(product)
//    }
//
//    fun getProducts(): LiveData<List<product>> {
//        return productDao!!.getProducts()
//    }
//
//    fun getProductByID(productID: Int): product {
//        return productDao!!.getProductByID(productID)
//    }
//}