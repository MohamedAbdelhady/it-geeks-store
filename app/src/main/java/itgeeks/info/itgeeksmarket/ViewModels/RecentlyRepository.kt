package itgeeks.info.itgeeksmarket.ViewModels

import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import itgeeks.info.itgeeksmarket.Models.product
import itgeeks.info.itgeeksmarket.Models.recently
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.ProductDao
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.ProductDatabase
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.RecentlyDao

class RecentlyRepository {

    var allProduct: LiveData<List<recently>>
    var recentlyDao: RecentlyDao

    constructor(application: Context) {
        recentlyDao = ProductDatabase.getInstance(application).recentlyDao();
        allProduct = recentlyDao.getProducts()
    }

    @WorkerThread
    fun insertProduct(recently: recently) {
        recentlyDao.insertProduct(recently)
    }

    @WorkerThread
    fun removeProduct(recently: recently) {
        recentlyDao.removeProduct(recently)
    }

    @WorkerThread
    fun removeProductsList(productList: List<recently>) {
        recentlyDao.removeProductsList(productList)
    }

    @WorkerThread
    fun getProductByID(productID: Int): LiveData<recently>? {
        return recentlyDao.getProductByID(productID)
    }

    @WorkerThread
    fun updateFavoriteIcon(isWished: Boolean, productID: Int) {
        recentlyDao.updateFavoriteIcon(isWished, productID)
    }
    @WorkerThread
   public fun updateProductCount(productCount: Int, productID: Int) {
        recentlyDao.updateProductCount(productCount, productID)
    }
}

//class ProductRepository {
//    private var productDao: ProductDao? = null
//    private val allProducts: LiveData<List<product>>? = null
//
//    constructor(application: Application) {
//        var cartProdutDatabase: CartProdutDatabase? = CartProdutDatabase.getInstance(application)
//        productDao = cartProdutDatabase!!.productDao()
//    }
//
//    fun insertProduct(product: product) {
//        thread {
//            productDao!!.insertProduct(product)
//        }
//    }
//
//    fun removeProduct(product: product) {
//        productDao!!.removeProduct(product)
//    }
//
//    fun removeProductsList(product: List<product>) {
//        productDao!!.removeProductsList(product)
//    }
//
//    fun getProducts(): LiveData<List<product>> {
//        return productDao!!.getProducts()
//    }
//
//    fun getProductByID(productID: Int): product {
//        return productDao!!.getProductByID(productID)
//    }
//}