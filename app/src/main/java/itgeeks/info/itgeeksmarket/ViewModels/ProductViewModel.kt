package itgeeks.info.itgeeksmarket.ViewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import itgeeks.info.itgeeksmarket.Models.product

class ProductViewModel : AndroidViewModel {

    private var productRepository: ProductRepository

    constructor(application: Application) : super(application) {
        productRepository = ProductRepository(application)
    }

    fun insertProduct(product: product) {
        productRepository.insertProduct(product)
    }


    fun removeProduct(product: product) {
        productRepository.removeProduct(product)
    }

    fun removeProductsList(productList: List<product>) {
        productRepository.removeProductsList(productList)
    }

    fun getProducts(): LiveData<List<product>>? {
        return productRepository.allProduct
    }

    fun getProductByID(productID: Int): LiveData<product>? {
        return productRepository.getProductByID(productID)
    }

    fun updateFavoriteIcon(isWished: Boolean, productID: Int) {
        productRepository.updateFavoriteIcon(isWished, productID)
    }

    fun updateProductCount(productCount: Int, productID: Int) {
        productRepository.updateProductCount(productCount, productID)
    }

}