package itgeeks.info.itgeeksmarket.ViewModels

import android.content.Context
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import itgeeks.info.itgeeksmarket.Models.searchHistory
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.ProductDatabase
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SearchHistoryDao

class SearchHistoryRepository {

    var allSearch: LiveData<List<searchHistory>>
    var searchHistoryDao: SearchHistoryDao

    constructor(application: Context) {
        searchHistoryDao = ProductDatabase.getInstance(application).searchHistoryDao();
        allSearch = searchHistoryDao.getSearchHistory();
    }

    @WorkerThread
    fun insertSearch(searchHistory: searchHistory) {
        searchHistoryDao.insertProduct(searchHistory)
    }

    @WorkerThread
    fun removeSearch(searchHistory: searchHistory) {
        searchHistoryDao.removeProduct(searchHistory)
    }

    @WorkerThread
    fun removeSearchesList(productList: List<searchHistory>) {
        searchHistoryDao.removeSearchHistoriesList(productList)
    }

    @WorkerThread
    fun getSearchByID(id: Int): LiveData<searchHistory>? {
        return searchHistoryDao.getSearchHistoryByID(id)
    }

    @WorkerThread
    fun removeAllSearchHistory() {
        searchHistoryDao.removeAllSearchHistory()
    }

}

//class ProductRepository {
//    private var productDao: ProductDao? = null
//    private val allProducts: LiveData<List<product>>? = null
//
//    constructor(application: Application) {
//        var cartProdutDatabase: CartProdutDatabase? = CartProdutDatabase.getInstance(application)
//        productDao = cartProdutDatabase!!.productDao()
//    }
//
//    fun insertProduct(product: product) {
//        thread {
//            productDao!!.insertProduct(product)
//        }
//    }
//
//    fun removeProduct(product: product) {
//        productDao!!.removeProduct(product)
//    }
//
//    fun removeProductsList(product: List<product>) {
//        productDao!!.removeProductsList(product)
//    }
//
//    fun getProducts(): LiveData<List<product>> {
//        return productDao!!.getProducts()
//    }
//
//    fun getProductByID(productID: Int): product {
//        return productDao!!.getProductByID(productID)
//    }
//}