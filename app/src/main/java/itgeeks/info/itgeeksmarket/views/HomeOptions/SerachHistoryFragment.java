package itgeeks.info.itgeeksmarket.views.HomeOptions;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.Models.searchHistory;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.SearchHistoryRepository;
import itgeeks.info.itgeeksmarket.adapters.BestForYouAdapter;
import itgeeks.info.itgeeksmarket.adapters.SearchHistoryAdapter;

import java.util.List;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;


public class SerachHistoryFragment extends Fragment {
    TextView tvSearchHistory , tvClear ;
    private RecyclerView searchHistoryTagList;

    public SerachHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_serach_history, container, false);

        initViews(view);

        updateView(Paper.book().read("language").toString());

        // Inflate the layout for this fragment
        return view;
    }

    private void initViews(View view) {
        tvSearchHistory = view.findViewById(R.id.tv_search_history);
        tvClear = view.findViewById(R.id.tv_clear);
        searchHistoryTagList = view.findViewById(R.id.search_history_tag_list);

        // get All Searches Tags
        new SearchHistoryRepository(getContext()).getAllSearch().observe(getActivity(), new Observer<List<searchHistory>>() {
            @Override
            public void onChanged(List<searchHistory> searchHistories) {
                searchHistoryTagList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
                SearchHistoryAdapter searchHistoryAdapter = new SearchHistoryAdapter(getContext(), searchHistories);
                searchHistoryTagList.setAdapter(searchHistoryAdapter);
                searchHistoryTagList.scrollToPosition(searchHistories.size()-1);
            }
        });

        // Clear Al Search History
        tvClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SearchHistoryRepository(getActivity()).removeAllSearchHistory();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(getContext(), lang);
        langResources = langContext.getResources();
        try {
            tvSearchHistory.setText(langResources.getString(R.string.search_history));
            tvClear.setText(langResources.getString(R.string.clear));
        } catch (Exception e) {
        }

    }

}
