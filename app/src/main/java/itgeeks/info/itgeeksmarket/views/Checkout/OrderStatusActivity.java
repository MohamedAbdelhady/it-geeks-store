package itgeeks.info.itgeeksmarket.views.Checkout;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.Models.categories;
import itgeeks.info.itgeeksmarket.R;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class OrderStatusActivity extends AppCompatActivity {
    TextView navName, btnShowDetails, tvOrderNumber, tvRequestSuccessfully;
    private int orderID;
    private String orderProcessKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_status);

        initView();

        updateView(Paper.book().read("language").toString());
    }

    private void initView() {

        orderID = Integer.parseInt(getIntent().getExtras().get("order_id").toString());
        orderProcessKey = getIntent().getExtras().get("order_process_key").toString();

        // activity name
        navName = findViewById(R.id.nav_name);

        btnShowDetails = findViewById(R.id.show_details);
        tvOrderNumber = findViewById(R.id.tv_order_number);
        tvRequestSuccessfully = findViewById(R.id.tv_request_successfully);

        tvOrderNumber.setText(langResources.getText(R.string.order_number) + " : " + orderID);
        // init btn show details
        btnShowDetails.setText(langResources.getString(R.string.show_details));

        //back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // show details
        btnShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderStatusActivity.this, MyOrderDetailsActivity.class);
                intent.putExtra("order_id", orderID);
                intent.putExtra("order_process_key", orderProcessKey);
                startActivity(intent);
            }
        });
    }


    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(OrderStatusActivity.this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.finish));
            tvRequestSuccessfully.setText(langResources.getString(R.string.request_successfully));
        } catch (Exception e) {
        }
    }
}
