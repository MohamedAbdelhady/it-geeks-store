package itgeeks.info.itgeeksmarket.views.MainFragements;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.*;
import android.widget.*;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;
import androidx.viewpager.widget.ViewPager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.MainResponse;
import itgeeks.info.itgeeksmarket.Models.searchHistory;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.SearchHistoryRepository;
import itgeeks.info.itgeeksmarket.adapters.HomeCategoriesAdapter;
import itgeeks.info.itgeeksmarket.adapters.HomeProductAdapter;
import itgeeks.info.itgeeksmarket.adapters.SliderViewPagers.SliderAdapter;
import itgeeks.info.itgeeksmarket.adapters.SliderViewPagers.SliderDotsAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.HomeOptions.*;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.SplashActivity;

import java.util.List;
import java.util.Locale;

import static itgeeks.info.itgeeksmarket.views.MainActivity.*;


public class HomeFragment extends Fragment implements View.OnClickListener {

    ImageView open_drawer_menu;
    RecyclerView homeCategoriesList, homeProductList, SliderDots;

    private ViewPager homeSlider;
    private int sliderAutoCount = 0;
    int sliderListSize;
    private ProgressBar loadingCard;
    private LinearLayout cartFullScreen, categories, saleToday, topSelection, newProduct;
    private TextView tvCategories, tvSaleToday, tvTopSelection, tvNewProduct, tvLastestProducts,etSearch;
    public static MainResponse mainResponse;
    public static String method_title;
    public static double method_cost;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        initViews(view);

        updateView(Paper.book().read("language").toString());

        initMainRequest();

        return view;

    }

    private void initViews(View view) {
        loadingCard = view.findViewById(R.id.loading_card);
        cartFullScreen = view.findViewById(R.id.home_full_screen);
        homeSlider = view.findViewById(R.id.home_slider);
        SliderDots = view.findViewById(R.id.slider_cercle_list);

        open_drawer_menu = view.findViewById(R.id.open_drawer_menu);

        homeCategoriesList = view.findViewById(R.id.home_categories_list);
        homeProductList = view.findViewById(R.id.home_product_list);

        homeCategoriesList.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.HORIZONTAL, false));
        homeProductList.setLayoutManager(new GridLayoutManager(getContext(), 2,RecyclerView.VERTICAL, false));

        open_drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConstraintSet constraintSet = new ConstraintSet();
                TransitionManager.beginDelayedTransition(constrainLayout);
                if (Locale.getDefault().getLanguage().trim().equals("ar")){
                    constraintSet.clone(getContext(), R.layout.key_frame_one_ar);
                    constraintSet.connect(R.layout.key_frame_one_ar, ConstraintSet.LEFT, R.layout.key_frame_one_ar, ConstraintSet.RIGHT, 70);
                }else {
                    constraintSet.clone(getContext(), R.layout.key_frame_one);
                    constraintSet.connect(R.layout.key_frame_one, ConstraintSet.RIGHT, R.layout.key_frame_one, ConstraintSet.LEFT, 70);
                }
                constraintSet.applyTo(constrainLayout);
                navigation.setVisibility(View.GONE);
                menuFrame.setVisibility(View.VISIBLE);
                //menu back
                MainActivity.menuBack = true;
            }
        });

        // Search
        etSearch = view.findViewById(R.id.et_search);
        etSearch.setOnClickListener(this);

        // notification
        view.findViewById(R.id.notification_bell).setOnClickListener(this);

        categories = view.findViewById(R.id.open_categories);
        saleToday = view.findViewById(R.id.open_sale_today);
        topSelection = view.findViewById(R.id.open_top_selection);
        newProduct = view.findViewById(R.id.open_new_products);

        tvCategories = view.findViewById(R.id.tv_categories);
        tvSaleToday = view.findViewById(R.id.tv_sale_today);
        tvTopSelection = view.findViewById(R.id.tv_top_selection);
        tvNewProduct = view.findViewById(R.id.tv_new_products);
        tvLastestProducts = view.findViewById(R.id.tv_lastest_products);

        categories.setOnClickListener(this);
        saleToday.setOnClickListener(this);
        topSelection.setOnClickListener(this);
        newProduct.setOnClickListener(this);
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(getContext(), lang);
        langResources = langContext.getResources();
        try {
            tvCategories.setText(langResources.getString(R.string.categories));
            tvSaleToday.setText(langResources.getString(R.string.sale_today));
            tvTopSelection.setText(langResources.getString(R.string.top_selection));
            tvNewProduct.setText(langResources.getString(R.string.new_products));
            tvLastestProducts.setText(langResources.getString(R.string.lastest));
            etSearch.setText(langResources.getString(R.string.search));
        } catch (Exception e) {
        }
    }

    private void initMainRequest() {
        try {
            displayLoading();
            RetrofitClient.getInstance().executeConnectionToServer(getContext(), PopularKeys.GET_ALL_PRODUCTS_HOME_PAGE, SharedPrefranceManager.getInastance(getContext()).getLang(), new Request(SharedPrefranceManager.getInastance(getContext()).getUser().getUser_id(), SharedPrefranceManager.getInastance(getContext()).getUser().getApi_token()), new HandleResponse() {
                @Override
                public void handleTrueResponse(JsonObject mainObject) {
                    mainResponse = new Gson().fromJson(mainObject, MainResponse.class);
                    if (mainResponse != null) {
                        setHomeSlider(mainResponse);
                        setHomeCategories(mainResponse);
                        method_title = mainResponse.getProducts().getShipping().get(0).getMethod_title();
                        method_cost = mainResponse.getProducts().getShipping().get(0).getMethod_cost();
                    } else {
                        startActivity(new Intent(getContext(), SplashActivity.class));
                        MainActivity.mainActivity.finish();
                    }
                    hideLoading();
                }

                @Override
                public void handleEmptyResponse() {
                    hideLoading();
                }

                @Override
                public void handleConnectionErrors(String errorMessage) {
                    hideLoading();
                }
            });
        } catch (Exception e) {
        }
    }

    private void setHomeSlider(MainResponse mainResponse) {
        sliderListSize = mainResponse.getProducts().getSliders().size();

//        image
        homeSlider.setAdapter(new SliderAdapter(getContext(), mainResponse.getProducts().getSliders(), SliderDots));

//        item_dots
        SliderDots.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        SliderDotsAdapter sliderDotsAdapter = new SliderDotsAdapter(getContext(), sliderListSize, sliderAutoCount);
        SliderDots.setAdapter(sliderDotsAdapter);

//        auto play
        autoPlaySlider(sliderListSize);

        homeSlider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                AutoSliderDots(position, sliderListSize);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void autoPlaySlider(final int listSize) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                homeSlider.setCurrentItem(sliderAutoCount++);
                autoPlaySlider(listSize);
                if (listSize == sliderAutoCount) sliderAutoCount = 0;
            }
        }, 1000);
    }

    private void AutoSliderDots(int sliderAutoCount, int listSize) {
        this.sliderAutoCount = sliderAutoCount;
        SliderDotsAdapter sliderDotsAdapter = new SliderDotsAdapter(getContext(), listSize, sliderAutoCount);
        SliderDots.setAdapter(sliderDotsAdapter);
    }

    private void setHomeCategories(MainResponse mainResponse) {
        HomeCategoriesAdapter homeCategoriesAdapter = new HomeCategoriesAdapter(getContext(), mainResponse.getProducts().getCategories());
        HomeProductAdapter homeProductAdapter = new HomeProductAdapter(getContext(), mainResponse.getProducts().getProduct());

        homeCategoriesList.setAdapter(homeCategoriesAdapter);
        homeProductList.setAdapter(homeProductAdapter);
    }

    //    Home Menu > Actions
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.open_categories:
                intent = new Intent(getContext(), AllCategoriesActivity.class);
                intent.putExtra("category", mainResponse.getProducts().getCategories());
                intent.putExtra("category_name", langResources.getString(R.string.all_categories));
                intent.putExtra("category_slug", "null");
                startActivity(intent);
                break;
            case R.id.open_sale_today:
                intent = new Intent(getContext(), sectionProductActivity.class);
                intent.putExtra("category_name", langResources.getString(R.string.sale_today));
                intent.putExtra("request_action", PopularKeys.GET_SALE_TODAY);
                startActivity(intent);
                break;
            case R.id.open_top_selection:
                intent = new Intent(getContext(), sectionProductActivity.class);
                intent.putExtra("category_name", langResources.getString(R.string.top_selection));
                intent.putExtra("request_action", PopularKeys.GET_TOP_SELECTION);
                startActivity(intent);
                break;
            case R.id.open_new_products:
                intent = new Intent(getContext(), sectionProductActivity.class);
                intent.putExtra("category_name", langResources.getString(R.string.new_products));
                intent.putExtra("request_action", PopularKeys.GET_NEW_PRODUCT);
                startActivity(intent);
                break;
            case R.id.et_search:
                startActivity(new Intent(getContext(), SearchActivity.class));
                break;
            case R.id.notification_bell:
                ((MainActivity) getContext()).MainFragment = new NotificationFragment();
                if (((MainActivity) getContext()).MainFragment != null) {
                    ((MainActivity) getContext()).displayFragment(((MainActivity) getContext()).MainFragment);
                }
                break;

        }
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        categories.setEnabled(false); // false clickable while requset still work
        cartFullScreen.setVisibility(View.GONE);
        //     getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        categories.setEnabled(true); // true click after requset done
        cartFullScreen.setVisibility(View.VISIBLE);
        //   getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
