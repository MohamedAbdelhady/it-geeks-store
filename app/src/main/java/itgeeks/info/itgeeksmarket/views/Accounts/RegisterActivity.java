package itgeeks.info.itgeeksmarket.views.Accounts;

import android.content.Intent;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.user;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class RegisterActivity extends AppCompatActivity {
    TextView navName , tvRegisterHint1,tvRegisterHint2,tvRegisterHint3 , btnLogin;
    Button btnRegister;
    EditText etFullName, etEmail, etUsername, etPassword, etPhoneCode, etPhoneNumber;
    private static boolean SHOW_PASS_STATUS;
    private ProgressBar loadingCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_register);

        initViews();

        updateView(Paper.book().read("language").toString());
    }

    private void initViews() {
        navName = findViewById(R.id.nav_name);
        btnRegister = findViewById(R.id.btn_register);
        etFullName = findViewById(R.id.et_full_name);
        etEmail = findViewById(R.id.et_email);
        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_register_Pass);
//        etPhoneCode = findViewById(R.id.et_phone_code);
//        etPhoneNumber = findViewById(R.id.et_phone_number);
        loadingCard = findViewById(R.id.loading_card);

        tvRegisterHint1 = findViewById(R.id.tv_register_hint_1);
        tvRegisterHint2 = findViewById(R.id.tv_register_hint_2);
        tvRegisterHint3 = findViewById(R.id.tv_register_hint_3);
        btnLogin = findViewById(R.id.btn_login);


        // Hover Full Name Shape
        etFullName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etFullName.setBackground(getDrawable(R.drawable.et_shape_hover));
                    etFullName.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_person_orange, 0, 0, 0);
                } else {
                    etFullName.setBackground(getDrawable(R.drawable.et_shape));
                    etFullName.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_person, 0, 0, 0);
                }
            }
        });

        // Hover Email Shape
        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etEmail.setBackground(getDrawable(R.drawable.et_shape_hover));
                    etEmail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_mail_orange, 0, 0, 0);
                } else {
                    etEmail.setBackground(getDrawable(R.drawable.et_shape));
                    etEmail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_mail, 0, 0, 0);
                }
            }
        });

        // Hover Username Shape
        etUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etUsername.setBackground(getDrawable(R.drawable.et_shape_hover));
                    etUsername.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_person_orange, 0, 0, 0);
                } else {
                    etUsername.setBackground(getDrawable(R.drawable.et_shape));
                    etUsername.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_person, 0, 0, 0);
                }
            }
        });

        // Hover Pass Shape
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etPassword.setBackground(getDrawable(R.drawable.et_shape_hover));
                    etPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_lock_orange, 0, 0, 0);
                } else {
                    etPassword.setBackground(getDrawable(R.drawable.et_shape));
                    etPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_lock, 0, 0, 0);
                }
            }
        });


        // Hover Phone Code Shape
//        etPhoneCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    etPhoneCode.setBackground(getDrawable(R.drawable.et_shape_hover));
//                    etPhoneCode.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_phone_orange, 0, 0, 0);
//                } else {
//                    etPhoneCode.setBackground(getDrawable(R.drawable.et_shape));
//                    etPhoneCode.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_phone, 0, 0, 0);
//                }
//            }
//        });

        // Hover Phone Number Shape
//        etPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    etPhoneNumber.setBackground(getDrawable(R.drawable.et_shape_hover));
//                } else {
//                    etPhoneNumber.setBackground(getDrawable(R.drawable.et_shape));
//                }
//            }
//        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get user Data
                String fullname = etFullName.getText().toString();
                String email = etEmail.getText().toString();
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();
//                String PhoneCode = etPhoneCode.getText().toString();
//                String phone = etPhoneNumber.getText().toString();

                // Check if Empty
                if (etFullName.getText().toString().isEmpty()) {
                    etFullName.setError(langResources.getString(R.string.required));
                    etFullName.requestFocus();
                } else if (etEmail.getText().toString().isEmpty()) {
                    etEmail.setError(langResources.getString(R.string.required));
                    etEmail.requestFocus();
                } else if (etUsername.getText().toString().isEmpty()) {
                    etUsername.setError(langResources.getString(R.string.required));
                    etUsername.requestFocus();
                } else if (etPassword.getText().toString().isEmpty()) {
                    etPassword.setError(langResources.getString(R.string.required));
                    etPassword.requestFocus();
                }
//                else if (etPhoneCode.getText().toString().isEmpty()) {
//                    etPhoneNumber.setError(getString(R.string.required));
//                    etPhoneNumber.requestFocus();
//                }
                else {
                    displayLoading();
                    RetrofitClient.getInstance().executeConnectionToServer(RegisterActivity.this, PopularKeys.REGISTER, SharedPrefranceManager.getInastance(RegisterActivity.this).getLang(), new Request(email, password, fullname, username), new HandleResponse() {
                        @Override
                        public void handleTrueResponse(JsonObject mainObject) {
                            // if true
                            if (mainObject.get("status").getAsBoolean()) {
                                user user = new Gson().fromJson(mainObject.get("user"), user.class);
                                SharedPrefranceManager.getInastance(RegisterActivity.this).saveUser(user);
                                Intent next = new Intent(RegisterActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(next);
                                finish();
                            }
                            hideLoading();
                        }

                        @Override
                        public void handleEmptyResponse() {
                            hideLoading();
                        }

                        @Override
                        public void handleConnectionErrors(String errorMessage) {
                            hideLoading();
                            Toast.makeText(RegisterActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
            }
        });

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.register));
            etFullName.setHint(langResources.getString(R.string.full_name));
            etEmail.setHint(langResources.getString(R.string.email));
            etUsername.setHint(langResources.getString(R.string.user_name));
            etPassword.setHint(langResources.getString(R.string.password));
            btnRegister.setText(langResources.getString(R.string.register));
            btnLogin.setText(langResources.getString(R.string.login));
            tvRegisterHint1.setText(langResources.getString(R.string.choose_login_page_hint));
            tvRegisterHint2.setText(langResources.getString(R.string.choose_register_with_us));
            tvRegisterHint3.setText(langResources.getString(R.string.hint_register_login));
        } catch (Exception e) {
        }
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
