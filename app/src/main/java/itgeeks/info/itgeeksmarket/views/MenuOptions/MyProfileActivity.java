package itgeeks.info.itgeeksmarket.views.MenuOptions;

import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.user;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class MyProfileActivity extends AppCompatActivity {
    ImageView userProfileImage;
    TextView navName, userProfileName;
    EditText etFullname, etEmail;
    Button btnSaveProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        initView();

        updateView(Paper.book().read("language").toString());
    }

    private void initView() {
        // activity name
        navName = findViewById(R.id.nav_name);
        navName.setText(R.string.my_profile);

        etFullname = findViewById(R.id.et_full_name);
        etEmail = findViewById(R.id.et_email);
        userProfileImage = findViewById(R.id.user_profile_image);
        userProfileName = findViewById(R.id.user_profile_name);
        btnSaveProfile = findViewById(R.id.btn_save_profile);

        etFullname.setText(SharedPrefranceManager.getInastance(this).getUser().getDisplay_name());
        etEmail.setText(SharedPrefranceManager.getInastance(this).getUser().getEmail());
        userProfileName.setText(SharedPrefranceManager.getInastance(this).getUser().getDisplay_name());
        Picasso.with(this).load(SharedPrefranceManager.getInastance(this).getUser().getImage()).into(userProfileImage);


        btnSaveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etFullname.getText().toString().isEmpty()) {
                    etFullname.setError(langResources.getString(R.string.required));
                    etFullname.requestFocus();
                } else if (etEmail.getText().toString().isEmpty()) {
                    etEmail.setError(langResources.getString(R.string.required));
                    etEmail.requestFocus();
                } else {
                    RetrofitClient.getInstance().executeConnectionToServer(MyProfileActivity.this, PopularKeys.UPDATE_USER_DATA, SharedPrefranceManager.getInastance(MyProfileActivity.this).getLang(),
                            new Request(
                                    SharedPrefranceManager.getInastance(MyProfileActivity.this).getUser().getApi_token(),
                                    etEmail.getText().toString(),
                                    etFullname.getText().toString(),
                                    SharedPrefranceManager.getInastance(MyProfileActivity.this).getUser().getUser_id()
                            ), new HandleResponse() {
                                @Override
                                public void handleTrueResponse(JsonObject mainObject) {
                                    if (mainObject.get("status").getAsBoolean()) {
                                        user user = new Gson().fromJson(mainObject.get("user").getAsJsonObject(), user.class);
                                        SharedPrefranceManager.getInastance(MyProfileActivity.this).saveUser(user);
                                    }
                                    Toast.makeText(MyProfileActivity.this, mainObject.get("message").getAsString(), Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void handleEmptyResponse() {

                                }

                                @Override
                                public void handleConnectionErrors(String errorMessage) {
                                    Toast.makeText(MyProfileActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                                }
                            });
                }
            }
        });


        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.my_profile));
            etFullname.setHint(langResources.getString(R.string.full_name));
            etEmail.setHint(langResources.getString(R.string.email));
            btnSaveProfile.setText(langResources.getString(R.string.save));
        } catch (Exception e) {
        }
    }
}
