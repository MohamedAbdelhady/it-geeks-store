package itgeeks.info.itgeeksmarket.views;

import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.reviews;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ReviewAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class CustomerFeedbackActivity extends AppCompatActivity {

    TextView navName, tvProductReview, noData, noDataToSee;
    RecyclerView reviewList;
    private ProgressBar loadingCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_feedback);

        initViews();

        updateView(Paper.book().read("language").toString());

        getReviewsData();

    }

    private void initViews() {
        // activity name
        navName = findViewById(R.id.nav_name);

        //init
        reviewList = findViewById(R.id.review_list);
        loadingCard = findViewById(R.id.loading_card);
        tvProductReview = findViewById(R.id.tv_product_review);
        noDataToSee = findViewById(R.id.no_data);

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.customer_feedback));
            tvProductReview.setText(langResources.getString(R.string.product_reviews));

        } catch (Exception e) {
        }
    }

    private void getReviewsData() {
        int productID = Integer.parseInt(getIntent().getExtras().get("product_id").toString());
        reviewList.setVisibility(View.GONE);
        displayLoading();
        RetrofitClient.getInstance().executeConnectionToServer(this, PopularKeys.GET_ALL_PRODUCT_REVIEWS, SharedPrefranceManager.getInastance(this).getLang(), new Request(SharedPrefranceManager.getInastance(CustomerFeedbackActivity.this).getUser().getUser_id(), SharedPrefranceManager.getInastance(CustomerFeedbackActivity.this).getUser().getApi_token(), productID), new HandleResponse() {
            @Override
            public void handleTrueResponse(JsonObject mainObject) {
                reviews reviews = new Gson().fromJson(mainObject, reviews.class);
                hideLoading();
                if (reviews.getComments().size() == 0) {
                    // if List Empty
                    reviewList.setVisibility(View.GONE);
                    noDataToSee.setVisibility(View.VISIBLE);
                    noDataToSee.setText(langResources.getString(R.string.no_reviews_to_see));
                } else {
                    // add products to list
                    reviewList.setVisibility(View.VISIBLE);
                    reviewList.setLayoutManager(new LinearLayoutManager(CustomerFeedbackActivity.this, RecyclerView.VERTICAL, false));
                    ReviewAdapter reviewsAdapter = new ReviewAdapter(CustomerFeedbackActivity.this, reviews.getComments());
                    reviewList.setAdapter(reviewsAdapter);
                }
            }

            @Override
            public void handleEmptyResponse() {
                hideLoading();
            }

            @Override
            public void handleConnectionErrors(String errorMessage) {
                hideLoading();
            }
        });
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
