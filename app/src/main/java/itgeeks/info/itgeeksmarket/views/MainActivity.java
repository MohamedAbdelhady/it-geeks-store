package itgeeks.info.itgeeksmarket.views;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.transition.TransitionManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.squareup.picasso.Picasso;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Receivers.ConnectionChangeReceiver;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.Accounts.ChooseLoginActivity;
import itgeeks.info.itgeeksmarket.views.Checkout.MyOrdersActivity;
import itgeeks.info.itgeeksmarket.views.MainFragements.*;
import itgeeks.info.itgeeksmarket.views.MenuOptions.AboutUsActivity;
import itgeeks.info.itgeeksmarket.views.MenuOptions.ContactUsActivity;
import itgeeks.info.itgeeksmarket.views.MenuOptions.MyProfileActivity;
import itgeeks.info.itgeeksmarket.views.SettingsOptions.AddAddressActivity;
import zendesk.core.AnonymousIdentity;
import zendesk.core.Identity;
import zendesk.core.Zendesk;
import zendesk.support.Support;
import zendesk.support.request.RequestActivity;

import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static Activity mainActivity;
    public static BottomNavigationView navigation;
    public static ConstraintLayout constrainLayout;
    public static CardView mainFrame, mainFrame2;
    public static CardView menuFrame;
    public Fragment MainFragment = new HomeFragment();
    public static boolean menuBack;
    ImageView userProfileImage;
    TextView userProfileName;
    private TextView notificationNumber;
    private boolean isFromAntherPage = false;

    // connection receiver
    private View snackContainer;
    public static ImageView noInternet;
    public static TextView btnTryAgain;
    public static Context langContext;
    public static Resources langResources;
    TextView loginPage, profilePage, myOrderPage, aboutUsPage, contactUsPage, settingsPage, supportChatPage, logOut;

    private ConnectionChangeReceiver connectionChangeReceiver = new ConnectionChangeReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); // hide Input
        setContentView(R.layout.activity_main);

        mainActivity = this;

        if (savedInstanceState == null) {
            displayFragment(MainFragment);
        }

        initViews();

        initHelp();

        initLanguage();

        initNavigation();

    }

    public void initLanguage() {

        // Change Screen Dirction on API 27
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O || Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1 || Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP) {
            if (Locale.getDefault().getLanguage().trim().equals("ar")) {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            } else {
                getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            }
        }

        // init Lang
        Paper.init(this);
        String language = Paper.book().read("language").toString();
        if (language == null) {
            Paper.book().write("language", new Locale("ar"));
        }

        updateView(Paper.book().read("language").toString());
    }

    // Update Lang On Screen
    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        loginPage.setText(langResources.getString(R.string.login));
        profilePage.setText(langResources.getString(R.string.my_profile));
        myOrderPage.setText(langResources.getString(R.string.my_orders));
        aboutUsPage.setText(langResources.getString(R.string.about_us));
        contactUsPage.setText(langResources.getString(R.string.contact_us));
        settingsPage.setText(langResources.getString(R.string.settings));
        supportChatPage.setText(langResources.getString(R.string.support_chat));
        btnTryAgain.setText(langResources.getString(R.string.tryAgain));
        logOut.setText(langResources.getString(R.string.log_out));

        try {
            navigation.getMenu().getItem(0).setTitle(langResources.getString(R.string.nav_home));
            navigation.getMenu().getItem(1).setTitle(langResources.getString(R.string.nav_cart));
            navigation.getMenu().getItem(2).setTitle(langResources.getString(R.string.nav_love));
            navigation.getMenu().getItem(3).setTitle(langResources.getString(R.string.nav_profile));
        } catch (Exception e) {
        }


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase, "en"));
    }

    private void initViews() {
        navigation = findViewById(R.id.navigation);

        // registerReceiver(connectionChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        loginPage = findViewById(R.id.login_page);
        profilePage = findViewById(R.id.profile_page);
        myOrderPage = findViewById(R.id.my_order_page);
        aboutUsPage = findViewById(R.id.about_us_page);
        contactUsPage = findViewById(R.id.contact_us_page);
        settingsPage = findViewById(R.id.settings_page);
        supportChatPage = findViewById(R.id.support_chat_page);
        logOut = findViewById(R.id.log_out_page);
        notificationNumber = findViewById(R.id.notification_number);


        btnTryAgain = findViewById(R.id.btn_try_again);

        // if back by click cart on anther page
        try {
            isFromAntherPage = getIntent().getExtras().get("backFromSearch").toString().equals("yes");
            if (isFromAntherPage) {
                displayFragment(new CartFragment());
                navigation.setSelectedItemId(R.id.cart);
            } else {
                displayFragment(new HomeFragment());
                navigation.setSelectedItemId(R.id.home);
            }
        } catch (Exception e) {
            // Toast.makeText(mainActivity, e.getMessage()+"sssss", Toast.LENGTH_SHORT).show(); TODO SEE IT
        }

    }

    public View getSnackBarContainer() {
        if (snackContainer == null) {
            snackContainer = findViewById(R.id.snackbar_container);
        }
        return snackContainer;
    }

    private void initNavigation() {

        userProfileImage = findViewById(R.id.user_profile_image);
        userProfileName = findViewById(R.id.user_profile_name);

        mainFrame = findViewById(R.id.main_frame);
        mainFrame2 = findViewById(R.id.main_frame2);
        menuFrame = findViewById(R.id.menu_frame);
        noInternet = findViewById(R.id.no_internet_page);
        // not login
        if (!SharedPrefranceManager.getInastance(this).isLoggedIn()) {
            findViewById(R.id.profile_page_div).setVisibility(View.GONE);
            findViewById(R.id.my_order_page_div).setVisibility(View.GONE);
            findViewById(R.id.user_data_in_menu).setVisibility(View.GONE);
            findViewById(R.id.login_page_div).setVisibility(View.VISIBLE);
            findViewById(R.id.log_out_page_div).setVisibility(View.GONE);
        } else { // Login
            findViewById(R.id.profile_page).setVisibility(View.VISIBLE);
            findViewById(R.id.my_order_page).setVisibility(View.VISIBLE);
            findViewById(R.id.user_data_in_menu).setVisibility(View.VISIBLE);
            findViewById(R.id.login_page_div).setVisibility(View.GONE);
            findViewById(R.id.log_out_page_div).setVisibility(View.VISIBLE);

            Log.d("img_user", SharedPrefranceManager.getInastance(this).getUser().getImage());
            userProfileName.setText(SharedPrefranceManager.getInastance(this).getUser().getDisplay_name());
            Picasso.with(this).load(SharedPrefranceManager.getInastance(this).getUser().getImage()).into(userProfileImage);
        }

        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                MainFragment = null;
                switch (menuItem.getItemId()) {
                    case R.id.home:
                        MainFragment = new HomeFragment();
                        break;
                    case R.id.cart:
                        MainFragment = new CartFragment();
                        break;
                    case R.id.favorite:
                        MainFragment = new FavoriteFragment();
                        break;
                    case R.id.profile:
                        MainFragment = new ProfileFragment();
                        displayFragment(MainFragment);
                        // if Con't Connect to Data
                        MainActivity.noInternet.setVisibility(View.GONE);
                        MainActivity.menuFrame.setVisibility(View.VISIBLE);
                        MainActivity.constrainLayout.setVisibility(View.VISIBLE);
                        MainActivity.btnTryAgain.setVisibility(View.GONE);
                        break;
                }

                if (MainFragment != null) {
                    displayFragment(MainFragment);
                    return true;
                }

                return false;
            }
        });

        // Bind  constrainLayout
        constrainLayout = findViewById(R.id.constrainLayout);
        final ImageView close_drawer_menu = findViewById(R.id.close_drawer_menu);

        close_drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDrawerMenu();
            }
        });
        mainFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawerMenu();
            }
        });
        mainFrame2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawerMenu();
            }
        });

        // notification number Observe
        new ProductRepository(this).getAllProduct().observe(this, new Observer<List<product>>() {
            @Override
            public void onChanged(List<product> products) {
                if (products.size() == 0) {
                    notificationNumber.setVisibility(View.GONE);
                } else if (products.size() > 9) {
                    notificationNumber.setVisibility(View.VISIBLE);
                    notificationNumber.setText("9+");
                } else {
                    notificationNumber.setVisibility(View.VISIBLE);
                    notificationNumber.setText(String.valueOf(products.size()));
                }
            }
        });

    }

    private void closeDrawerMenu() {
        //defaultKeyFrameButton clicked create new ConstraintSet
        ConstraintSet constraintSet = new ConstraintSet();
        //Clone positioning information from @key_frame_default.xml in ConstraintSet
        constraintSet.clone(MainActivity.this, R.layout.key_frame_default);

        //beginDelayedTransition is Convenience method to animate to a new
        // scene defined by all changes within
        // the given scene root between calling
        // this method and the next rendering frame.
        TransitionManager.beginDelayedTransition(constrainLayout);

        //Then apply ConstraintSet to ConstraintLayout
        //this will move views with animation to positions
        //as defined in key_frame_default.xml layout
        constraintSet.applyTo(constrainLayout);
        navigation.setVisibility(View.VISIBLE);
        menuFrame.setVisibility(View.GONE);
    }

    public void displayFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    //    Home Menu
    public void menuOptions(View view) {
        switch (view.getId()) {
            case R.id.login_page:
                // Login
                logIn();
                break;
            case R.id.profile_page:
                startActivity(new Intent(this, MyProfileActivity.class));
                break;
            case R.id.my_order_page:
                startActivity(new Intent(this, MyOrdersActivity.class));
                break;
            case R.id.about_us_page:
                startActivity(new Intent(this, AboutUsActivity.class));
                break;
            case R.id.contact_us_page:
                startActivity(new Intent(this, ContactUsActivity.class));
                break;
            case R.id.settings_page:
                closeDrawerMenu();
                displayFragment(new ProfileFragment());
                navigation.setSelectedItemId(R.id.profile);
                break;
            case R.id.support_chat_page:
                // Customers Service
                RequestActivity.builder().show(this);
                break;
            case R.id.log_out_page:
                // Logout
                logOut();
                break;
        }
    }

    private void initHelp() {
        //Customers Service
        Zendesk.INSTANCE.init(getApplicationContext(), "https://itgeeks.zendesk.com",
                "6d1749c16b1fa13aaf7a96a39614131f8eba1e5d27ed37bb",
                "mobile_sdk_client_e65a598574b57edaf2e8");
        Identity identity = new AnonymousIdentity();
        Zendesk.INSTANCE.setIdentity(identity);
        Support.INSTANCE.init(Zendesk.INSTANCE);
    }

    public void logOut() {
        androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(MainActivity.this);
        alert.setMessage(langResources.getString(R.string.question_logout));
        alert.setPositiveButton(langResources.getString(R.string.sure), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPrefranceManager.getInastance(MainActivity.this).clearUser();
                startActivity(new Intent(MainActivity.this, MainActivity.class));
                finish();
            }
        });
        alert.setNegativeButton(langResources.getString(R.string.cancel), null);
        alert.create();
        alert.show();
    }

    public void logIn() {
        startActivity(new Intent(this, ChooseLoginActivity.class));
    }

    //    Settings On Profile Page
    public void accountSettingOptions(View view) {
        switch (view.getId()) {
            case R.id.tv_profile:
                startActivity(new Intent(this, MyProfileActivity.class));
                break;
            case R.id.tv_my_addresses:
                startActivity(new Intent(this, AddAddressActivity.class));
                break;
        }
    }

    public void tryAgain(View view) {
        recreate();
    }

    @Override
    public void onBackPressed() {
        try {
            if (menuBack) {
                closeDrawerMenu();
                menuBack = false;
            } else {
//            if == in Home > Out
                if (MainFragment.getClass().getSimpleName().equals("HomeFragment")) {
                    super.onBackPressed();
                } else {
//            else == Go Home
                    displayFragment(new HomeFragment());
                    navigation.setSelectedItemId(R.id.home);
                }
            }
        } catch (Exception e) {
            Toast.makeText(mainActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
//        try {
//            unregisterReceiver(connectionChangeReceiver);
//        } catch (IllegalArgumentException e) {
//            e.printStackTrace();
//        }
        super.onDestroy();
    }

}
