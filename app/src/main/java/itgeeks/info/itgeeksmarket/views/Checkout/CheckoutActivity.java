package itgeeks.info.itgeeksmarket.views.Checkout;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.address;
import itgeeks.info.itgeeksmarket.Models.checkCoupon;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.adapters.SummaryAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.MainFragements.CartFragment;
import itgeeks.info.itgeeksmarket.views.MainFragements.HomeFragment;
import itgeeks.info.itgeeksmarket.views.ProductActivity;
import itgeeks.info.itgeeksmarket.views.SettingsOptions.AddAddressActivity;
import kotlin.ReplaceWith;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;


public class CheckoutActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout deliveryDev, paymentDev, summaryDev;
    private TextView navName, delivery, payment, summary, tvSubTotalDelivery, tvShippingDelivery, tvTotalDelivery, tvSubTotalPayment, tvShippingPayment,
            tvTotalPayment, tvShippingCost, tvSubTotalDeliveryHint, tvShippingDeliveryHint, tvTotalDeliveryHint, tvSubTotalPaymentHint, tvShippingPaymentHint,
            tvTotalPaymentHint, tvCouponPaymentHint, tvBalancePaymentHint;
    private TextView tvUserame, tvAddress, tvPhone, tvAddressDetails, tvSelectDelivery, tvDeliveryPrice, tvPaymentMethod, tvPaymentCachOnDeliveryHint, tvUseVoucher, couponPayment, balancePayment;
    private EditText etCopoun;
    private Button btnCopounApply;
    private int stepNum;
    private String subTotal;
    private double couponAmount;
    private String couponCode = "";
    List<product> finalOrderList = new ArrayList<>();
    private boolean isSendOrderToServer = false;
    private boolean isCouponApplyed = false;
    List<address> address = new ArrayList<>();

    // start Activity For Result Actions Code
    public static final int UPDATE_ADDRESS_CODE = 1001;

    //    Delivery
    Button btnProceedToPayment;
    RadioButton domesticStandardShipping;
    private boolean domesticStandardShippingStatus;
    ImageView editAddress;

    //    Payment
    Button btnProceedToSummary;
    RadioButton cashOnDeslivery;
    private boolean cashOnDesliveryStatus;

    //    Summary
    RecyclerView summaryList;
    Button btnProceedToMyOrder;
    TextView btnScreenShot;

    private ProgressBar loadingCard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        getIntentData();

        initViews();

        updateView(Paper.book().read("language").toString());

        handlePriceData();
    }

    private void getIntentData() {
        if (getIntent().getExtras().get("Previous_Page").toString().trim().equals("cart")) {
            finalOrderList.addAll(CartFragment.ListOfOrders);
        } else if (getIntent().getExtras().get("Previous_Page").toString().trim().equals("product")) {
            finalOrderList.add(ProductActivity.singleProductOrder);
        }

        subTotal = getIntent().getExtras().get("total").toString();
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        try {
            if (SharedPrefranceManager.getInastance(CheckoutActivity.this).isLoggedIn())
                navName.setText(langResources.getString(R.string.checkout));
            else navName.setText(langResources.getString(R.string.checkoutGuest));


            delivery.setText(langResources.getString(R.string.delivery));
            payment.setText(langResources.getString(R.string.payment));
            summary.setText(langResources.getString(R.string.summary));

            tvSubTotalDeliveryHint.setText(langResources.getString(R.string.subtotal));
            tvShippingDeliveryHint.setText(langResources.getString(R.string.shipping));
            tvTotalDeliveryHint.setText(langResources.getString(R.string.total));

            tvSubTotalPaymentHint.setText(langResources.getString(R.string.subtotal));
            tvShippingPaymentHint.setText(langResources.getString(R.string.shipping));
            tvTotalPaymentHint.setText(langResources.getString(R.string.total));

            btnProceedToMyOrder.setText(langResources.getString(R.string.my_orders));
            btnProceedToPayment.setText(langResources.getString(R.string.proceed_to_payment));
            btnProceedToSummary.setText(langResources.getString(R.string.proceed_to_summary));

            domesticStandardShipping.setText(langResources.getString(R.string.domestic_standard_shipping));
            cashOnDeslivery.setText(langResources.getString(R.string.cash_on_deslivery));

            tvAddressDetails.setText(langResources.getString(R.string.address_details));
            tvSelectDelivery.setText(langResources.getString(R.string.select_a_delivery_method));
            tvDeliveryPrice.setText(langResources.getString(R.string.delivery_price));

            tvPaymentMethod.setText(langResources.getString(R.string.payment_method));
            tvPaymentCachOnDeliveryHint.setText(langResources.getString(R.string.cash_on_delivery_hint));
            tvUseVoucher.setText(langResources.getString(R.string.use_a_voucher));
            etCopoun.setHint(langResources.getString(R.string.enter_voucher_code));
            btnCopounApply.setText(langResources.getString(R.string.apply));

            tvCouponPaymentHint.setText(langResources.getString(R.string.coupons));
            tvBalancePaymentHint.setText(langResources.getString(R.string.balance));
            btnScreenShot.setText(langResources.getString(R.string.take_screen_shot));


        } catch (Exception e) {
        }
    }

    private void initViews() {
        // activity name
        navName = findViewById(R.id.nav_name);

        // init
        delivery = findViewById(R.id.delivery);
        payment = findViewById(R.id.payment);
        summary = findViewById(R.id.summary);

        tvUserame = findViewById(R.id.tv_username);
        tvAddress = findViewById(R.id.tv_address);
        tvPhone = findViewById(R.id.tv_phone);

        tvSubTotalDeliveryHint = findViewById(R.id.tv_sub_total_delivery_hint);
        tvShippingDeliveryHint = findViewById(R.id.tv_shipping_delivery_hint);
        tvTotalDeliveryHint = findViewById(R.id.tv_total_delivery_hint);

        tvSubTotalPaymentHint = findViewById(R.id.tv_sub_total_payment_hint);
        tvShippingPaymentHint = findViewById(R.id.tv_shipping_payment_hint);
        tvTotalPaymentHint = findViewById(R.id.tv_total_payment_hint);

        tvCouponPaymentHint = findViewById(R.id.tv_coupon_payment_hint);
        tvBalancePaymentHint = findViewById(R.id.tv_balance_payment_hint);
        couponPayment = findViewById(R.id.coupon_payment);
        balancePayment = findViewById(R.id.balance_payment);

        deliveryDev = findViewById(R.id.delivery_dev);
        paymentDev = findViewById(R.id.payment_dev);
        summaryDev = findViewById(R.id.summary_dev);

        tvSubTotalDelivery = findViewById(R.id.sub_total_delivery);
        tvShippingDelivery = findViewById(R.id.shipping_delivery);
        tvTotalDelivery = findViewById(R.id.total_delivery);

        tvSubTotalPayment = findViewById(R.id.sub_total_payment);
        tvShippingPayment = findViewById(R.id.shipping_payment);
        tvTotalPayment = findViewById(R.id.total_payment);

        tvShippingCost = findViewById(R.id.shipping_cost);
        tvAddressDetails = findViewById(R.id.tv_address_details);
        tvSelectDelivery = findViewById(R.id.tv_select_delivery);
        tvDeliveryPrice = findViewById(R.id.tv_delivery_price);
        tvPaymentMethod = findViewById(R.id.tv_payment_methid);
        tvPaymentCachOnDeliveryHint = findViewById(R.id.tv_payment_hint);
        tvUseVoucher = findViewById(R.id.tv_use_voucher);
        etCopoun = findViewById(R.id.et_copoun);
        btnCopounApply = findViewById(R.id.btn_copoun_apply);

        loadingCard = findViewById(R.id.loading_card);

        btnScreenShot = findViewById(R.id.btn_screen_shot);

        delivery.setOnClickListener(this);
        payment.setOnClickListener(this);
        summary.setOnClickListener(this);
        btnScreenShot.setOnClickListener(this);

        // show address details
        if (SharedPrefranceManager.getInastance(CheckoutActivity.this).isLoggedIn()) {
            if (SharedPrefranceManager.getInastance(this).isAddressSaved()) {
                tvUserame.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_first_name() + " " + SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_last_name());
                tvAddress.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_address_line_1());
                tvPhone.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_phone_number() + "");
            } else {
                tvAddress.setText(langResources.getString(R.string.no_address));
            }
        } else {
            if (SharedPrefranceManager.getInastance(this).isAddressGuestSaved()) {
                tvUserame.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_first_name() + " " + SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_last_name());
                tvAddress.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_address_line_1());
                tvPhone.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_phone_number() + "");
            } else {
                tvAddress.setText(langResources.getString(R.string.no_address));
            }
        }

        // Delivery
        btnProceedToPayment = findViewById(R.id.btn_proceed_to_payment);
        editAddress = findViewById(R.id.edit_address);
        domesticStandardShipping = findViewById(R.id.domestic_standard_shipping);
        btnProceedToPayment.setOnClickListener(this);
        editAddress.setOnClickListener(this);
        domesticStandardShipping.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setbtnProceedToPayment(isChecked);
                }
            }
        });
        ///////////////
        // Payment
        btnProceedToSummary = findViewById(R.id.btn_proceed_to_summary);
        cashOnDeslivery = findViewById(R.id.cash_on_deslivery);
        btnProceedToSummary.setOnClickListener(this);
        cashOnDeslivery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    setBtnProceedToSummary(isChecked);
                }
            }
        });
        ///////////////
        // Summary
        summaryList = findViewById(R.id.summary_list);
        btnProceedToMyOrder = findViewById(R.id.btn_proceed_to_my_order);
        if (!SharedPrefranceManager.getInastance(this).isLoggedIn())
            btnProceedToMyOrder.setVisibility(View.GONE); // Invisible ( Gone ) btn Of MyOrder if i ( Guest) cant see  the Order

        summaryList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        summaryList.setAdapter(new SummaryAdapter(this, finalOrderList));
        btnProceedToMyOrder.setOnClickListener(this);
        btnCopounApply.setOnClickListener(this);
        ////////////////
        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    // Handle Price to View
    private void handlePriceData() {
        tvSubTotalDelivery.setText(subTotal + " " + MainActivity.langResources.getString(R.string.currency));
        tvShippingDelivery.setText(Double.parseDouble(String.valueOf(HomeFragment.method_cost)) + " " + MainActivity.langResources.getString(R.string.currency));
        tvShippingCost.setText("( " + Double.parseDouble(String.valueOf(HomeFragment.method_cost)) + " " + MainActivity.langResources.getString(R.string.currency) + " )");
        tvTotalDelivery.setText((Double.parseDouble(subTotal) + HomeFragment.method_cost) + " " + MainActivity.langResources.getString(R.string.currency));
        tvSubTotalPayment.setText(Double.parseDouble(subTotal) + " " + MainActivity.langResources.getString(R.string.currency));
        tvShippingPayment.setText(Double.parseDouble(String.valueOf(HomeFragment.method_cost)) + " " + MainActivity.langResources.getString(R.string.currency));
        tvTotalPayment.setText((Double.parseDouble(subTotal) + HomeFragment.method_cost) + " " + MainActivity.langResources.getString(R.string.currency));
        couponPayment.setText(couponAmount + " " + MainActivity.langResources.getString(R.string.currency));
        balancePayment.setText((Double.parseDouble(subTotal) + HomeFragment.method_cost - couponAmount) + " " + MainActivity.langResources.getString(R.string.currency));
    }

    private void setbtnProceedToPayment(Boolean isChecked) {
        domesticStandardShippingStatus = isChecked;
        btnProceedToPayment.setEnabled(domesticStandardShippingStatus);
        btnProceedToPayment.setBackgroundColor(getResources().getColor(R.color.dusty_orange));
    }

    private void setBtnProceedToSummary(boolean isChecked) {
        cashOnDesliveryStatus = isChecked;
        btnProceedToSummary.setEnabled(cashOnDesliveryStatus);
        btnProceedToSummary.setBackgroundColor(getResources().getColor(R.color.dusty_orange));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.delivery:
                if (stepNum >= 1) {
                    setDelivery();
                }
                break;
            case R.id.payment:
                if (stepNum >= 2) {
                    setPayment();
                }
                break;
            case R.id.summary:
                if (stepNum >= 3) {
                    setSummary();
                }
                break;
            // next step
            case R.id.btn_proceed_to_payment:
                if (SharedPrefranceManager.getInastance(this).isLoggedIn()) {
                    if (SharedPrefranceManager.getInastance(this).isAddressSaved()) {
                        stepNum = 2;
                        setPayment();
                    } else
                        Toast.makeText(this, langResources.getString(R.string.complate_address_details), Toast.LENGTH_SHORT).show();
                } else {
                    if (SharedPrefranceManager.getInastance(this).isAddressGuestSaved()) {
                        stepNum = 2;
                        setPayment();
                    } else
                        Toast.makeText(this, langResources.getString(R.string.complate_address_details), Toast.LENGTH_SHORT).show();
                }
                break;
            // edit address
            case R.id.edit_address:
                Intent i = new Intent(this, AddAddressActivity.class);
                if (SharedPrefranceManager.getInastance(CheckoutActivity.this).isLoggedIn()) i.putExtra("User", "User");
                else i.putExtra("Guest", "Guest");
                startActivityForResult(i, UPDATE_ADDRESS_CODE);

                break;
            // next step
            case R.id.btn_proceed_to_summary:
                displayLoading();
                if (SharedPrefranceManager.getInastance(this).isLoggedIn()) {
                    // User
                    sendRequestToServerIfUser();
                } else {
                    // Guest
                    sendRequestToServerIfGuest();
                }
                break;
            // Check Coupon Code
            case R.id.btn_copoun_apply:
                if (!etCopoun.getText().toString().trim().equals("")) {
                    if (SharedPrefranceManager.getInastance(this).isLoggedIn()) {
                        // Coupon Code
                        sendRequestCheckCouponCode();
                    } else Toast.makeText(this, "You not user", Toast.LENGTH_SHORT).show();
                } else Toast.makeText(this, "Coupon is empty", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_proceed_to_my_order:
                startActivity(new Intent(this, MyOrdersActivity.class));
                break;
            case R.id.btn_screen_shot:
                getScreen(); // screenShot
                break;
        }
    }

    private void sendRequestCheckCouponCode() {
        findViewById(R.id.coupon_progressbar).setVisibility(View.VISIBLE);
        RetrofitClient.getInstance().executeConnectionToServer(this,
                PopularKeys.APPLY_COUPON,
                SharedPrefranceManager.getInastance(this).getLang(),
                new Request(SharedPrefranceManager.getInastance(this).getUser().getApi_token(),
                        SharedPrefranceManager.getInastance(this).getUser().getUser_id(),
                        etCopoun.getText().toString()), new HandleResponse() {
                    @Override
                    public void handleTrueResponse(JsonObject mainObject) {
                        findViewById(R.id.coupon_progressbar).setVisibility(View.GONE);
                        checkCoupon checkCoupon = new Gson().fromJson(mainObject, checkCoupon.class);
                        isCouponApplyed = checkCoupon.is_coupon_applyed();
                        if (checkCoupon.is_coupon_applyed()) {

                            if (Double.parseDouble(subTotal) < Double.parseDouble(checkCoupon.getCoupon_max_amount()) && Double.parseDouble(subTotal) > Double.parseDouble(checkCoupon.getCoupon_min_amount())) {

                                if (checkCoupon.getCoupon_type().trim().equals("discount_from_product") || checkCoupon.getCoupon_type().trim().equals("discount_from_total_cart")) {
                                    couponAmount = Double.parseDouble(checkCoupon.getCoupon_amount());
                                } else if (checkCoupon.getCoupon_type().trim().equals("percentage_discount_from_product") || checkCoupon.getCoupon_type().trim().equals("percentage_discount_from_total_cart")) {
                                    couponAmount = (Double.parseDouble(subTotal) * Double.parseDouble(checkCoupon.getCoupon_amount()) / 100);
                                }
                                couponCode = etCopoun.getText().toString();
                                etCopoun.setEnabled(false);
                                btnCopounApply.setEnabled(false);
                                handlePriceData();
                            } else {
                                if (Double.parseDouble(subTotal) > Double.parseDouble(checkCoupon.getCoupon_max_amount())) {
                                    etCopoun.setError(langResources.getString(R.string.subtotal_more_than_copun));
                                } else if (Double.parseDouble(subTotal) < Double.parseDouble(checkCoupon.getCoupon_min_amount())) {
                                    etCopoun.setError(langResources.getString(R.string.subtotal_less_than_copun));
                                }
                            }
                        } else {
                            etCopoun.setError(checkCoupon.getMessage());
                        }
                    }

                    @Override
                    public void handleEmptyResponse() {
                        findViewById(R.id.coupon_progressbar).setVisibility(View.GONE);
                    }

                    @Override
                    public void handleConnectionErrors(String errorMessage) {
                        findViewById(R.id.coupon_progressbar).setVisibility(View.GONE);
                        Toast.makeText(CheckoutActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void sendRequestToServerIfGuest() {
        if (!isSendOrderToServer) {
            // get address from cach

            address.add(
                    new address(
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_first_name(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_last_name(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_email_address(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_address_line_1(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_phone_number(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_first_name(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_last_name(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_email_address(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_address_line_1(),
                            SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_phone_number()
                    )
            );
            // Send Order To server
            RetrofitClient.getInstance().executeConnectionToServer(this, "setUserOrders", SharedPrefranceManager.getInastance(this).getLang(),
                    new Request(
                            SharedPrefranceManager.getInastance(this).getUser().getApi_token(),
                            SharedPrefranceManager.getInastance(this).getUser().getUser_id(),
                            finalOrderList,
                            address,
                            couponAmount,
                            couponCode,
                            "",
                            HomeFragment.method_title,
                            HomeFragment.method_cost,
                            "cod",
                            "cod",
                            isCouponApplyed,
                            (Double.parseDouble(subTotal) + HomeFragment.method_cost - couponAmount)
                    )
                    , new HandleResponse() {
                @Override
                public void handleTrueResponse(JsonObject mainObject) {
                    hideLoading();
                    handelResponsOrder(mainObject);
                    Intent intent = new Intent(CheckoutActivity.this, OrderStatusActivity.class);
                    intent.putExtra("order_id", mainObject.get("order_id").getAsInt());
                    intent.putExtra("order_process_key", mainObject.get("process_id").getAsString());
                    startActivity(intent);
                }

                @Override
                public void handleEmptyResponse() {
                    hideLoading();
                }

                @Override
                public void handleConnectionErrors(String errorMessage) {
                    hideLoading();
                }
            });
        } else {
            stepNum = 3;
            setSummary();
        }
    }

    private void handelResponsOrder(JsonObject mainObject) {
        isSendOrderToServer = true;
        delivery.setVisibility(View.GONE);
        payment.setVisibility(View.GONE);
        // clear cart list from Caching
        if (getIntent().getExtras().get("Previous_Page").toString().trim().equals("cart")) {
            new ProductRepository(CheckoutActivity.this).removeProductsList(finalOrderList);
        }
        Toast.makeText(CheckoutActivity.this, mainObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
        stepNum = 3;
        setSummary();
    }

    private void sendRequestToServerIfUser() {
        if (!isSendOrderToServer) {
            // Send Order To server
            RetrofitClient.getInstance().executeConnectionToServer(this, "setUserOrders", SharedPrefranceManager.getInastance(this).getLang(),
                    new Request(
                            SharedPrefranceManager.getInastance(this).getUser().getApi_token(),
                            SharedPrefranceManager.getInastance(this).getUser().getUser_id(),
                            finalOrderList,
                            address,
                            couponAmount,
                            couponCode,
                            "",
                            HomeFragment.method_title,
                            HomeFragment.method_cost,
                            "cod",
                            "cod",
                            isCouponApplyed,
                            (Double.parseDouble(subTotal) + HomeFragment.method_cost -  couponAmount)
                    ), new HandleResponse() {
                        @Override
                        public void handleTrueResponse(JsonObject mainObject) {
                            hideLoading();
                            handelResponsOrder(mainObject);
                            Intent intent = new Intent(CheckoutActivity.this, OrderStatusActivity.class);
                            intent.putExtra("order_id", mainObject.get("order_id").getAsInt());
                            intent.putExtra("order_process_key", mainObject.get("process_id").getAsString());
                            startActivity(intent);
                        }

                        @Override
                        public void handleEmptyResponse() {
                            hideLoading();
                        }

                        @Override
                        public void handleConnectionErrors(String errorMessage) {
                            hideLoading();
                        }
                    });
        } else {
            stepNum = 3;
            setSummary();
        }
    }

    public void setDelivery() {
        delivery.setBackground(getDrawable(R.drawable.checkout_hover));
        payment.setBackground(null);
        summary.setBackground(null);
        delivery.setTextColor(Color.WHITE);
        payment.setTextColor(getResources().getColor(R.color.greyish_brown));
        summary.setTextColor(getResources().getColor(R.color.greyish_brown));

        deliveryDev.setVisibility(View.VISIBLE);
        paymentDev.setVisibility(View.GONE);
        summaryDev.setVisibility(View.GONE);
    }

    public void setPayment() {
        delivery.setBackground(null);
        payment.setBackground(getDrawable(R.drawable.checkout_hover));
        summary.setBackground(null);
        delivery.setTextColor(getResources().getColor(R.color.greyish_brown));
        payment.setTextColor(Color.WHITE);
        summary.setTextColor(getResources().getColor(R.color.greyish_brown));

        deliveryDev.setVisibility(View.GONE);
        paymentDev.setVisibility(View.VISIBLE);
        summaryDev.setVisibility(View.GONE);
    }

    public void setSummary() {
        delivery.setBackground(null);
        payment.setBackground(null);
        // summary.setBackground(getDrawable(R.drawable.checkout_hover));
        // summary.setTextColor(Color.WHITE);
        delivery.setTextColor(getResources().getColor(R.color.greyish_brown));
        payment.setTextColor(getResources().getColor(R.color.greyish_brown));

        deliveryDev.setVisibility(View.GONE);
        paymentDev.setVisibility(View.GONE);
        summaryDev.setVisibility(View.VISIBLE);
    }

    // Tack Screenshot
    private void getScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 99001);
        } else {
            takeScreenshot();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 99001 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            takeScreenshot();
        }
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        View v = getWindow().getDecorView().getRootView();
        v.setDrawingCacheEnabled(true);
        Bitmap bmp = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        Toast.makeText(this, "Screenshot Done!", Toast.LENGTH_SHORT).show();

        try {
            File file = new File(Environment.getExternalStorageDirectory().toString() + "/DCIM/Screenshots/", "BayaaScreen" + now + ".jpg");
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            openScreenshot(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(Uri.parse(String.valueOf(uri).replace("file", "content")), "image/*");
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == UPDATE_ADDRESS_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                // show address details
                if (SharedPrefranceManager.getInastance(CheckoutActivity.this).isLoggedIn()) {
                    if (SharedPrefranceManager.getInastance(this).isAddressSaved()) {
                        tvUserame.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_first_name() + " " + SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_last_name());
                        tvAddress.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_address_line_1());
                        tvPhone.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_phone_number() + "");
                    } else {
                        tvAddress.setText(langResources.getString(R.string.no_address));
                    }
                } else {
                    if (SharedPrefranceManager.getInastance(this).isAddressGuestSaved()) {
                        tvUserame.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_first_name() + " " + SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_last_name());
                        tvAddress.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_address_line_1());
                        tvPhone.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_phone_number() + "");
                    } else {
                        tvAddress.setText(langResources.getString(R.string.no_address));
                    }
                }
            }
            if (resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }



}
