package itgeeks.info.itgeeksmarket.views;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.Common;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.ProductResponse;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.Models.recently;
import itgeeks.info.itgeeksmarket.Models.singleProduct;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.ViewModels.ProductViewModel;
import itgeeks.info.itgeeksmarket.ViewModels.RecentlyRepository;
import itgeeks.info.itgeeksmarket.adapters.BestForYouAdapter;
import itgeeks.info.itgeeksmarket.adapters.ColorsAdapter;
import itgeeks.info.itgeeksmarket.adapters.ReviewAdapter;
import itgeeks.info.itgeeksmarket.adapters.SizesAdapter;
import itgeeks.info.itgeeksmarket.adapters.SliderViewPagers.SliderAdapter;
import itgeeks.info.itgeeksmarket.adapters.SliderViewPagers.SliderDotsAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.Accounts.LoginActivity;
import itgeeks.info.itgeeksmarket.views.Checkout.CheckoutActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.SearchActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.sectionProductActivity;

import java.util.List;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;


public class ProductActivity extends AppCompatActivity implements View.OnClickListener {
    RecyclerView bestForYouList, homeReviewList, SliderDots, sizesList, colorsList;
    private ViewPager ProductSlider;
    private int sliderAutoCount = 0;
    TextView oldProductPrice, tvReviews, continueShopping, btnAddToCart, product_brands, product_price, product_title, product_rate_total, tvProductDescription,
            tvQuantity, tvProductDetails, tvProductReview, tvBestForYou, seeAllProductRelated, seeAllReviews, tvSizes, tvColors;
    RatingBar product_rate;
    TextView counterProductNum;
    ImageView btnAddToFavorite, btnProductSearch, btnFloatingIconCart;
    private int productID;
    Boolean isWished;
    private ProgressBar loadingCard;
    itgeeks.info.itgeeksmarket.Models.singleProduct singleProduct;
    CardView sizesCard, colorCard;
    public static product singleProductOrder;
    private TextView notificationNumber;

    // add product status
    private boolean isAddedToCart = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Common.getInstance(this).changeStatusBarColor("#ffffff", this);

        initViews();

        updateView(Paper.book().read("language").toString());

        getProductData();
    }

    private void initViews() {
        // init
        counterProductNum = findViewById(R.id.counter_product_num);
        product_title = findViewById(R.id.product_title);
        product_brands = findViewById(R.id.product_brands);
        product_price = findViewById(R.id.product_price);
        oldProductPrice = findViewById(R.id.product_sale_price);
        product_rate_total = findViewById(R.id.product_rate_total);
        product_rate = findViewById(R.id.product_rate);
        tvSizes = findViewById(R.id.tv_sizes);
        tvColors = findViewById(R.id.tv_colors);
        tvReviews = findViewById(R.id.tv_reviews);
        tvQuantity = findViewById(R.id.tv_quantity);
        tvProductDetails = findViewById(R.id.tv_product_details);
        tvProductReview = findViewById(R.id.tv_product_review);
        tvBestForYou = findViewById(R.id.tv_best_for_you);
        notificationNumber = findViewById(R.id.notification_number);

        loadingCard = findViewById(R.id.loading_card);
        tvProductDescription = findViewById(R.id.tv_product_description);
        sizesCard = findViewById(R.id.sizes_card);
        colorCard = findViewById(R.id.colors_card);

        //  productSlider = findViewById(R.id.product_slider);
        bestForYouList = findViewById(R.id.best_for_you_list);
        homeReviewList = findViewById(R.id.home_review_list);
        sizesList = findViewById(R.id.sizes_list);
        colorsList = findViewById(R.id.colors_list);

        // cart
        continueShopping = findViewById(R.id.continue_shopping);
        btnAddToCart = findViewById(R.id.btn_add_to_cart);

        //Product Slider
        ProductSlider = findViewById(R.id.product_slider);
        SliderDots = findViewById(R.id.slider_cercle_list);

        // counter Up & Down
        findViewById(R.id.count_up).setOnClickListener(this);
        findViewById(R.id.count_down).setOnClickListener(this);

        tvReviews.setOnClickListener(this);
        seeAllReviews = findViewById(R.id.see_all_reviews);
        seeAllReviews.setOnClickListener(this);
        seeAllProductRelated = findViewById(R.id.see_all_product_related);
        seeAllProductRelated.setOnClickListener(this);
        continueShopping.setOnClickListener(this);
        btnAddToCart.setOnClickListener(this);
        btnFloatingIconCart = findViewById(R.id.btn_floating_icon_cart);

        // add to favorite
        btnAddToFavorite = findViewById(R.id.btn_add_to_favorite);
        btnAddToFavorite.setOnClickListener(this);

        // search
        btnProductSearch = findViewById(R.id.product_search);
        btnProductSearch.setOnClickListener(this);

        // go to cart
        btnFloatingIconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ProductActivity.this, MainActivity.class);
                i.putExtra("backFromSearch", "yes");
                startActivity(i);
            }
        });

        //back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //  line over a Old Price
        oldProductPrice.setPaintFlags(oldProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        // notification number Observe
        new ProductRepository(this).getAllProduct().observe(this, new Observer<List<product>>() {
            @Override
            public void onChanged(List<product> products) {
                if (products.size() == 0) {
                    notificationNumber.setVisibility(View.GONE);
                } else if (products.size() > 9) {
                    notificationNumber.setVisibility(View.VISIBLE);
                    notificationNumber.setText("9+");
                } else {
                    notificationNumber.setVisibility(View.VISIBLE);
                    notificationNumber.setText(String.valueOf(products.size()));
                }
            }
        });

    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();

        tvReviews.setText(langResources.getString(R.string.reviews));
        tvQuantity.setText(langResources.getString(R.string.quantity));
        tvProductDetails.setText(langResources.getString(R.string.prodeuct_details));
        tvProductReview.setText(langResources.getString(R.string.product_reviews));
        btnAddToCart.setText(langResources.getString(R.string.add_to_cart));
        continueShopping.setText(langResources.getString(R.string.buy_now));
        tvBestForYou.setText(langResources.getString(R.string.best_for_you));
        seeAllProductRelated.setText(langResources.getString(R.string.see_all));
        seeAllReviews.setText(langResources.getString(R.string.see_all));
        tvSizes.setText(langResources.getString(R.string.sizes));
        tvColors.setText(langResources.getString(R.string.colors));

    }

    private void getProductData() {

        productID = Integer.parseInt(getIntent().getExtras().get("product_id").toString());
        displayLoading();
        RetrofitClient.getInstance().executeConnectionToServer(this, PopularKeys.GET_PRODUCT_BY_ID, SharedPrefranceManager.getInastance(this).getLang(), new Request(SharedPrefranceManager.getInastance(ProductActivity.this).getUser().getUser_id(), SharedPrefranceManager.getInastance(ProductActivity.this).getUser().getApi_token(), productID), new HandleResponse() {
            @Override
            public void handleTrueResponse(JsonObject mainObject) {

                ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                if (productResponse.getStatus()) {
                    setHomeSlider(productResponse.getSingleProduct().getDetails().getImages());
                    showProductData(productResponse.getSingleProduct());
                    handleDataInLists(productResponse.getSingleProduct());
                    singleProduct = productResponse.getSingleProduct();

                    // add to recently Viewed TODO Will Remove later
                    try {
                        singleProduct.getDetails().getProduct().setQuantity(Integer.parseInt(counterProductNum.getText().toString()));
                        singleProduct.getDetails().getProduct().setProduct_type("simple_product");
                        singleProduct.getDetails().getProduct().setVarition_id(0);
                        recently recently = new Gson().fromJson(new Gson().toJson(singleProduct.getDetails().getProduct()), recently.class);
                        new RecentlyRepository(ProductActivity.this).insertProduct(recently);
                    } catch (Exception e) {
                        Toast.makeText(ProductActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }
                hideLoading();
            }

            @Override
            public void handleEmptyResponse() {
                hideLoading();
            }

            @Override
            public void handleConnectionErrors(String errorMessage) {
                hideLoading();
            }
        });


    }

    private void showProductData(singleProduct singleProduct) {
        singleProductOrder = singleProduct.getDetails().getProduct();
        // Visable && InVisable
        if (singleProduct.isThereSize()) sizesCard.setVisibility(View.VISIBLE);
        else sizesCard.setVisibility(View.GONE);
        if (singleProduct.isThereColor()) colorCard.setVisibility(View.VISIBLE);
        else colorCard.setVisibility(View.GONE);

        /////////////
        product_brands.setText(singleProduct.getDetails().getProduct().getProduct_brands());
        product_title.setText(singleProduct.getDetails().getProduct().getProduct_title());
        product_price.setText(singleProduct.getDetails().getProduct().getProduct_price() + " " + MainActivity.langResources.getString(R.string.currency));

        // if have a discount
        if (singleProduct.getDetails().getProduct().getProduct_sale_price() == 0)
            findViewById(R.id.discount).setVisibility(View.GONE);
        else findViewById(R.id.discount).setVisibility(View.VISIBLE);

        oldProductPrice.setText(singleProduct.getDetails().getProduct().getProduct_sale_price() + "");
        product_rate_total.setText("( " + singleProduct.getReviews().getTotal() + " )");
        product_rate.setRating(singleProduct.getReviews().getAvarage());
        tvProductReview.setText(langResources.getString(R.string.product_reviews) + " ( " + singleProduct.getReviews().getTotal() + " ) ");

        // set description
        String htmlDescription = singleProduct.getDetails().getProduct().getProduct_description().replaceAll("&lt;", "<").replaceAll("&quot;", "\"").replaceAll("&gt;", ">");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvProductDescription.setText(Html.fromHtml(htmlDescription, Html.FROM_HTML_MODE_COMPACT));
        } else {
            tvProductDescription.setText(Html.fromHtml(htmlDescription));
        }

        isWished = singleProduct.getDetails().getProduct().isWished();
        // if added to favorite before
        if (isWished) {
            btnAddToFavorite.setBackground(getDrawable(R.drawable.ic_favorite_red));
        }

        // Check if product added to cart
        ProductViewModel productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        productViewModel.getProductByID(productID).observe(this, new Observer<product>() {
            @Override
            public void onChanged(product product) {
                if (product != null) {
                    btnAddToCart.setBackgroundColor(getResources().getColor(R.color.jade));
                    btnAddToCart.setTextColor(Color.WHITE);
                    btnAddToCart.setText(langResources.getString(R.string.added_to_cart));
                    isAddedToCart = true;
                } else {
                    btnAddToCart.setBackgroundColor(getResources().getColor(R.color.white));
                    btnAddToCart.setTextColor(Color.BLACK);
                    btnAddToCart.setText(langResources.getString(R.string.add_to_cart));
                    isAddedToCart = false;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int c;
        Intent intent;

        switch (v.getId()) {

            // Search
            case R.id.product_search:
                intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                break;

            // count Up
            case R.id.count_up:
                if (Integer.parseInt(counterProductNum.getText().toString()) >= 999)
                    counterProductNum.setText(999 + "");
                else {
                    c = Integer.parseInt(counterProductNum.getText().toString()) + 1;
                    counterProductNum.setText(c + "");
                }
                break;
            // count Down
            case R.id.count_down:
                if (Integer.parseInt(counterProductNum.getText().toString()) <= 1)
                    counterProductNum.setText(1 + "");
                else {
                    c = Integer.parseInt(counterProductNum.getText().toString()) - 1;
                    counterProductNum.setText(c + "");
                }
                break;

            //Reviews
            case R.id.tv_reviews:
                intent = new Intent(this, CustomerFeedbackActivity.class);
                intent.putExtra("product_id", productID);
                startActivity(intent);
                break;
            case R.id.see_all_reviews:
                intent = new Intent(this, CustomerFeedbackActivity.class);
                intent.putExtra("product_id", productID);
                startActivity(intent);
                break;

            case R.id.see_all_product_related:
                intent = new Intent(this, sectionProductActivity.class);
                intent.putExtra("category_name", getString(R.string.product_related));
                intent.putExtra("request_action", PopularKeys.GET_ALL_PRODUCT_RELATED);
                intent.putExtra("product_id", productID);
                startActivity(intent);
                break;

            //Checkout
            case R.id.continue_shopping:
                try {
                    Intent i = new Intent(ProductActivity.this, MainActivity.class);
                    i.putExtra("backFromSearch", "yes");
                    startActivity(i);
                } catch (Exception e) {
                    e.getMessage();
                }
                break;

            case R.id.btn_add_to_cart:
                // Check if product added to cart

                try {
                    if (isAddedToCart) {
                        singleProduct.getDetails().getProduct().setQuantity(Integer.parseInt(counterProductNum.getText().toString()));
                        new ProductRepository(ProductActivity.this).removeProduct(singleProduct.getDetails().getProduct());
                    } else {
                        singleProduct.getDetails().getProduct().setQuantity(Integer.parseInt(counterProductNum.getText().toString()));
                        new ProductRepository(ProductActivity.this).insertProduct(singleProduct.getDetails().getProduct());
                    }
                } catch (Exception e) {
                    e.getMessage();
                }

                break;

            case R.id.btn_add_to_favorite:
                // if not logined
                if (!SharedPrefranceManager.getInastance(this).isLoggedIn()) {
                    //startActivity(new Intent(this, LoginActivity.class));
                    Snackbar snack = Snackbar.make(btnAddToFavorite,MainActivity.langResources.getString(R.string.need_to_be_user),2000);
                    View view = snack.getView();
                    FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
                    params.setMargins(params.leftMargin,
                            params.topMargin,
                            params.rightMargin,
                            params.bottomMargin + 100);
                    view.setLayoutParams(params);
                    snack.show();
                } else {
                    try {
                        if (isWished == false) {
                            addToFavorite();
                        } else if (isWished == true) {
//                        deleteFromFavorite();
                        }
                    } catch (Exception e) {
                        e.getMessage();
                    }
                }
                break;
        }
    }

    private void setHomeSlider(final List<String> productSliderList) {
        SliderDots.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        SliderDotsAdapter sliderDotsAdapter = new SliderDotsAdapter(this, productSliderList.size(), sliderAutoCount);
        SliderDots.setAdapter(sliderDotsAdapter);
        ProductSlider.setAdapter(new SliderAdapter(this, productSliderList, SliderDots));

        autoPlaySlider(productSliderList);

        ProductSlider.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                AutoSliderDots(position, productSliderList);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void autoPlaySlider(final List<String> productSliderList) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ProductSlider.setCurrentItem(sliderAutoCount++);
                autoPlaySlider(productSliderList);
                if (productSliderList.size() == sliderAutoCount) sliderAutoCount = 0;
            }
        }, 1000);
    }

    private void AutoSliderDots(int sliderAutoCount, List<String> productSliderList) {
        this.sliderAutoCount = sliderAutoCount;
        SliderDotsAdapter sliderDotsAdapter = new SliderDotsAdapter(this, productSliderList.size(), sliderAutoCount);
        SliderDots.setAdapter(sliderDotsAdapter);
    }


    private void handleDataInLists(singleProduct singleProduct) {
        // add bestForYou to list
        if (singleProduct.getRelated().size() == 0) findViewById(R.id.best_for_you_div).setVisibility(View.GONE);
        else {
            bestForYouList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            BestForYouAdapter bestForYouAdapter = new BestForYouAdapter(this, singleProduct.getRelated());
            bestForYouList.setAdapter(bestForYouAdapter);
        }

        // add Reviews to list
        if (singleProduct.getReviews().getComments().size() == 0) {
            TextView noDataToSee = findViewById(R.id.no_data);
            noDataToSee.setVisibility(View.VISIBLE);
            noDataToSee.setText(langResources.getString(R.string.no_reviews_to_see));
        } else {
            findViewById(R.id.no_data).setVisibility(View.GONE);
            homeReviewList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
            ReviewAdapter reviewAdapter = new ReviewAdapter(this, singleProduct.getReviews().getComments());
            homeReviewList.setAdapter(reviewAdapter);
        }

        if (singleProduct.isThereSize()) {
            // add Sizes List
            sizesList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            SizesAdapter sizesAdapter = new SizesAdapter(this, singleProduct.getSizes());
            sizesList.setAdapter(sizesAdapter);
        }

        if (singleProduct.isThereColor()) {
            // add Colors List
            colorsList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
            ColorsAdapter colorsAdapter = new ColorsAdapter(this, singleProduct.getColors());
            colorsList.setAdapter(colorsAdapter);
        }
    }

    //   Add To Favorite

    private void addToFavorite() {
        btnAddToFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RetrofitClient.getInstance().executeConnectionToServer(ProductActivity.this, PopularKeys.SET_FAVORITE_PRODUCT, SharedPrefranceManager.getInastance(ProductActivity.this).getLang(), new Request(SharedPrefranceManager.getInastance(ProductActivity.this).getUser().getUser_id(), SharedPrefranceManager.getInastance(ProductActivity.this).getUser().getApi_token(), productID), new HandleResponse() {
                    @Override
                    public void handleTrueResponse(JsonObject mainObject) {
                        btnAddToFavorite.setBackground(getDrawable(R.drawable.ic_favorite_red));
                        isWished = true;
                    }

                    @Override
                    public void handleEmptyResponse() {

                    }

                    @Override
                    public void handleConnectionErrors(String errorMessage) {

                    }
                });
            }
        });
    }

//    private void deleteFromFavorite() {
//        btnAddToFavorite.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                RetrofitClient.getInstance().executeConnectionToServer(ProductActivity.this, PopularKeys.REMOVE_FAVORITE_PRODUCT, SharedPrefranceManager.getInastance(ProductActivity.this).getLang(), new Request(SharedPrefranceManager.getInastance(ProductActivity.this).getUser().getUser_id(), SharedPrefranceManager.getInastance(ProductActivity.this).getUser().getApi_token(), productID), new HandleResponse() {
//                    @Override
//                    public void handleTrueResponse(JsonObject mainObject) {
//                        btnAddToFavorite.setBackground(getDrawable(R.drawable.ic_favorite_border));
//                        isWished = false;
//                    }
//
//                    @Override
//                    public void handleEmptyResponse() {
//
//                    }
//
//                    @Override
//                    public void handleConnectionErrors(String errorMessage) {
//
//                    }
//                });
//            }
//        });


    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        findViewById(R.id.product_full_screen).setVisibility(View.GONE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        findViewById(R.id.product_full_screen).setVisibility(View.VISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

}
