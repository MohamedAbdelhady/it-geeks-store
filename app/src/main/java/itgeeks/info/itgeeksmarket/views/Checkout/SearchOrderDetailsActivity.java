package itgeeks.info.itgeeksmarket.views.Checkout;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import java.lang.ref.SoftReference;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class SearchOrderDetailsActivity extends AppCompatActivity {

    TextView navName, tvTrackingOrderHint, tvSearchOrdersDetails;
    EditText etOrderNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_order_details);

        initViews();

        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        // activity name
        navName = findViewById(R.id.nav_name);
        tvTrackingOrderHint = findViewById(R.id.tv_tracking_order_hint);
        tvSearchOrdersDetails = findViewById(R.id.tv_search_order_details);
        etOrderNumber = findViewById(R.id.et_order_number);

        tvSearchOrdersDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!etOrderNumber.getText().toString().trim().isEmpty()) {
                    Intent intent = new Intent(SearchOrderDetailsActivity.this, MyOrderDetailsActivity.class);
                    intent.putExtra("order_id", Integer.parseInt(etOrderNumber.getText().toString()));
                    intent.putExtra("order_process_key", "1561035501997593081177989347");
                    startActivity(intent);
                }else Toast.makeText(SearchOrderDetailsActivity.this, langResources.getString(R.string.required), Toast.LENGTH_SHORT).show();
            }
        });


        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.tracking_orders));
            tvTrackingOrderHint.setText(langResources.getString(R.string.tracking_orders_hint));
            tvSearchOrdersDetails.setText(langResources.getString(R.string.search));
            etOrderNumber.setHint(langResources.getString(R.string.order_number));

        } catch (Exception e) {
        }
    }
}
