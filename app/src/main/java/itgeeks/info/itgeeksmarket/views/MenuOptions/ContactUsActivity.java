package itgeeks.info.itgeeksmarket.views.MenuOptions;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.R;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class ContactUsActivity extends AppCompatActivity {

    TextView navName;
    EditText etName , etEmail , etMessage;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        initView();

        updateView(Paper.book().read("language").toString());
    }

    private void initView() {
        // activity name
        navName = findViewById(R.id.nav_name);
        navName.setText(R.string.contact_us);

        etName = findViewById(R.id.et_name);
        etEmail = findViewById(R.id.et_email);
        etMessage = findViewById(R.id.et_message);
        btnSend = findViewById(R.id.btn_send);

        //back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.contact_us));
            etName.setHint(langResources.getString(R.string.name));
            etEmail.setHint(langResources.getString(R.string.email));
            etMessage.setHint(langResources.getString(R.string.message));
            btnSend.setText(langResources.getString(R.string.send));
        } catch (Exception e) {
        }
    }

}
