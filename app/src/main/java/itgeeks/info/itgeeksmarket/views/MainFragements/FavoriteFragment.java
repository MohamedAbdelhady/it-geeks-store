package itgeeks.info.itgeeksmarket.views.MainFragements;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.ProductResponse;
import itgeeks.info.itgeeksmarket.adapters.FavoriteAdapter;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.Accounts.LoginActivity;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.SearchActivity;

import java.util.Locale;

import static itgeeks.info.itgeeksmarket.views.MainActivity.*;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;


public class FavoriteFragment extends Fragment {

    ImageView open_drawer_menu;
    Toolbar navBar;
    static RecyclerView favoriteList;
    TextView etSearch;
    private static ProgressBar loadingCard;
    private static LinearLayout favoriteFullScreen;

    public FavoriteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        // if not logined
        if (!SharedPrefranceManager.getInastance(getContext()).isLoggedIn()){
//            displayFragment(new HomeFragment());
//            MainActivity.navigation.setSelectedItemId(R.id.home);
//            startActivity(new Intent(getContext(), LoginActivity.class));
            TextView emptyPage =  view.findViewById(R.id.favorite_page_empty);
            emptyPage.setVisibility(View.VISIBLE);
            emptyPage.setText(langResources.getText(R.string.need_to_be_user));

        }else {
            initViews(view);

            updateView(Paper.book().read("language").toString());

            getFavotiteRequest();
        }
        return view;
    }

    private void getFavotiteRequest() {
        try {
            displayLoading();
            RetrofitClient.getInstance().executeConnectionToServer(getContext(), PopularKeys.GET_FAVORITE_PRODUCT, SharedPrefranceManager.getInastance(getContext()).getLang(), new Request(SharedPrefranceManager.getInastance(getContext()).getUser().getUser_id(), SharedPrefranceManager.getInastance(getContext()).getUser().getApi_token()), new HandleResponse() {
                @Override
                public void handleTrueResponse(JsonObject mainObject) {
                    ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                    if (productResponse.getStatus()) {
                        if (productResponse.getProducts().size() == 0){
                            getView().findViewById(R.id.favorite_page_empty).setVisibility(View.VISIBLE);
                        }else {
                        favoriteList.setLayoutManager(new LinearLayoutManager(getContext()));
                        FavoriteAdapter favoriteAdapter = new FavoriteAdapter(getActivity(), productResponse.getProducts());
                        favoriteList.setAdapter(favoriteAdapter);
                        hideLoading();
                        }
                    } else {
                        Toast.makeText(MainActivity.mainActivity, productResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void handleEmptyResponse() {
                    hideLoading();
                }

                @Override
                public void handleConnectionErrors(String errorMessage) {
                    hideLoading();
                }
            });
        }catch (Exception e){}
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(getContext(), lang);
        langResources = langContext.getResources();
        try {
            etSearch.setText(langResources.getString(R.string.search));

        } catch (Exception e) {
        }

    }

    private void initViews(View view) {
        favoriteFullScreen = view.findViewById(R.id.favorite_full_screen);
        open_drawer_menu = view.findViewById(R.id.open_drawer_menu);
        etSearch = view.findViewById(R.id.et_search);
        favoriteList = view.findViewById(R.id.favorite_list);
        loadingCard = view.findViewById(R.id.loading_card);

        // navBar = view.findViewById(R.id.nav_bar);
        //  ((MainActivity)getActivity()).setSupportActionBar(navBar);


        open_drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //KeyFrameOneButton clicked create new ConstraintSet
                ConstraintSet constraintSet = new ConstraintSet();
                //Clone positioning information from @key_frame_one.xml in ConstraintSet
                TransitionManager.beginDelayedTransition(constrainLayout);
                if (Locale.getDefault().getLanguage().trim().equals("ar")){
                    constraintSet.clone(getContext(), R.layout.key_frame_one_ar);
                    constraintSet.connect(R.layout.key_frame_one_ar, ConstraintSet.LEFT, R.layout.key_frame_one_ar, ConstraintSet.RIGHT, 70);
                }else {
                    constraintSet.clone(getContext(), R.layout.key_frame_one);
                    constraintSet.connect(R.layout.key_frame_one, ConstraintSet.RIGHT, R.layout.key_frame_one, ConstraintSet.LEFT, 70);
                }
                constraintSet.applyTo(constrainLayout);
                navigation.setVisibility(View.GONE);
                menuFrame.setVisibility(View.VISIBLE);

                //menu back
                MainActivity.menuBack = true;
            }
        });

        // Search
        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SearchActivity.class));
            }
        });
    }

    public void displayFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    public static void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        favoriteList.setVisibility(View.GONE);
        MainActivity.mainActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public static void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        favoriteList.setVisibility(View.VISIBLE);
        MainActivity.mainActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public static void displayLoading2() {
        loadingCard.setVisibility(View.VISIBLE);
        MainActivity.mainActivity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public static void hideLoading2() {
        loadingCard.setVisibility(View.GONE);
        MainActivity.mainActivity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

}
