package itgeeks.info.itgeeksmarket.views.HomeOptions;


import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ColorsFilterAdapter;
import itgeeks.info.itgeeksmarket.adapters.SearchAdapter;
import itgeeks.info.itgeeksmarket.adapters.SizesFilterAdapter;
import org.florescu.android.rangeseekbar.RangeSeekBar;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class SearchBodyFragment extends Fragment implements View.OnClickListener {

    RecyclerView searchList, colorsListFilter, sizesListFilter;
    public static final int SEARCH_LIST = R.layout.item_search_list;
    public static final int SEARCH_GRID = R.layout.item_search_grid;

    TextView list, filters, popularity, tvMinMaxPrice, searchApply, searchClearAll;
    TextView popularityAlpaz, popularityAlpza, popularityLowHigh, popularityHighLow, popularityOldNew, popularityNewOld, popularityAll, sortBy, tvFilter, tvPrice, tvColors, tvSizes;
    RangeSeekBar priceRange;
    public static BottomSheetDialog bottomSheetPopularity, bottomSheetFilter;

    private static int minRange = 1;
    private static int maxRange = Integer.parseInt(langResources.getString(R.string.num_top));
    ;
    private final static int min = 1;
    private final static int max = Integer.parseInt(langResources.getString(R.string.num_top));
    ;

    public SearchBodyFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_body, container, false);

        initViews(view);

        updateView(Paper.book().read("language").toString());

        return view;
    }

    private void initViews(final View view) {
        list = view.findViewById(R.id.list);
        filters = view.findViewById(R.id.filters);
        popularity = view.findViewById(R.id.popularity);

        if (SearchActivity.getSearchList().size() == 0) {
            view.findViewById(R.id.search_page_empty).setVisibility(View.VISIBLE);
            list.setVisibility(View.GONE);
            // view.findViewById(R.id.filter_dev).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.search_page_empty).setVisibility(View.GONE);
            list.setVisibility(View.VISIBLE);
            // view.findViewById(R.id.filter_dev).setVisibility(View.VISIBLE);
        }
        searchList = view.findViewById(R.id.search_list);

        if (SearchActivity.listOrientation.equals("list")) {
            list.setCompoundDrawablesRelativeWithIntrinsicBounds(getContext().getDrawable(R.drawable.ic_apps_black), null, null, null);
            list.setText(langResources.getString(R.string.grid));
            searchList();
        } else if (SearchActivity.listOrientation.equals("grid")) {
            searchGrid();
            list.setCompoundDrawablesRelativeWithIntrinsicBounds(getContext().getDrawable(R.drawable.ic_format_list), null, null, null);
            list.setText(langResources.getString(R.string.list));
        }

        // List & Grid
        list.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                if (SearchActivity.listOrientation.equals("grid")) {
                    SearchActivity.listOrientation = "list";
                    searchList();
                    list.setCompoundDrawablesRelativeWithIntrinsicBounds(getContext().getDrawable(R.drawable.ic_apps_black), null, null, null);
                    list.setText(langResources.getString(R.string.grid));
                } else {
                    SearchActivity.listOrientation = "grid";
                    searchGrid();
                    list.setCompoundDrawablesRelativeWithIntrinsicBounds(getContext().getDrawable(R.drawable.ic_format_list), null, null, null);
                    list.setText(langResources.getString(R.string.list));
                }
            }
        });

        // popularity
        popularity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetPopularity = new BottomSheetDialog(getContext(), R.style.BottomSheetDialogTheme);
                View view = getLayoutInflater().inflate(R.layout.dialog_bottom_sheet_popularity, null);


                popularityAlpaz = view.findViewById(R.id.popularity_alpaz);
                popularityAlpza = view.findViewById(R.id.popularity_alpza);
                popularityLowHigh = view.findViewById(R.id.popularity_low_high);
                popularityHighLow = view.findViewById(R.id.popularity_high_low);
                popularityOldNew = view.findViewById(R.id.popularity_old_new);
                popularityNewOld = view.findViewById(R.id.popularity_new_old);
                popularityAll = view.findViewById(R.id.popularity_all);
                sortBy = view.findViewById(R.id.sort_by);

                updateView(Paper.book().read("language").toString()); // update language

                if (SearchActivity.sortBy == "alpaz")
                    popularityAlpaz.setTextColor(getContext().getResources().getColor(R.color.dark_sky_blue));
                if (SearchActivity.sortBy == "alpza")
                    popularityAlpza.setTextColor(getContext().getResources().getColor(R.color.dark_sky_blue));
                if (SearchActivity.sortBy == "low-high")
                    popularityLowHigh.setTextColor(getContext().getResources().getColor(R.color.dark_sky_blue));
                if (SearchActivity.sortBy == "high-low")
                    popularityHighLow.setTextColor(getContext().getResources().getColor(R.color.dark_sky_blue));
                if (SearchActivity.sortBy == "old-new")
                    popularityOldNew.setTextColor(getContext().getResources().getColor(R.color.dark_sky_blue));
                if (SearchActivity.sortBy == "new-old")
                    popularityNewOld.setTextColor(getContext().getResources().getColor(R.color.dark_sky_blue));
                if (SearchActivity.sortBy == "all")
                    popularityAll.setTextColor(getContext().getResources().getColor(R.color.dark_sky_blue));

                try {
                    popularityAlpaz.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SearchActivity.sortBy = "alpaz";
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                            bottomSheetPopularity.dismiss();
                        }
                    });
                    popularityAlpza.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SearchActivity.sortBy = "alpza";
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                            bottomSheetPopularity.dismiss();
                        }
                    });
                    popularityLowHigh.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SearchActivity.sortBy = "low-high";
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                            bottomSheetPopularity.dismiss();
                        }
                    });
                    popularityHighLow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SearchActivity.sortBy = "high-low";
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                            bottomSheetPopularity.dismiss();
                        }
                    });
                    popularityOldNew.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SearchActivity.sortBy = "old-new";
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                            bottomSheetPopularity.dismiss();
                        }
                    });
                    popularityNewOld.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SearchActivity.sortBy = "new-old";
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                            bottomSheetPopularity.dismiss();
                        }
                    });
                    popularityAll.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SearchActivity.sortBy = "all";
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                            bottomSheetPopularity.dismiss();
                        }
                    });
                } catch (Exception e) {
                }

                bottomSheetPopularity.setContentView(view);
                bottomSheetPopularity.show();

                //close bottom sheet
                view.findViewById(R.id.close_bottom_sheet_filters).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (bottomSheetPopularity.isShowing()) {
                            bottomSheetPopularity.dismiss();
                        } else {
                            bottomSheetPopularity.show();
                        }
                    }
                });
            }
        });

        // filter
        filters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetFilter = new BottomSheetDialog(getContext(), R.style.BottomSheetDialogTheme);
                View view = getLayoutInflater().inflate(R.layout.dialog_bottom_sheet_filter, null);

                priceRange = view.findViewById(R.id.range_price);
                tvMinMaxPrice = view.findViewById(R.id.tv_min_max_price);
                searchApply = view.findViewById(R.id.search_apply);
                searchClearAll = view.findViewById(R.id.search_clear_all);
                tvFilter = view.findViewById(R.id.tv_filter);
                tvPrice = view.findViewById(R.id.tv_price);
                tvColors = view.findViewById(R.id.tv_colors);
                tvSizes = view.findViewById(R.id.tv_sizes);
                colorsListFilter = view.findViewById(R.id.colors_list_filter);
                sizesListFilter = view.findViewById(R.id.sizes_list_filter);

                updateView(Paper.book().read("language").toString()); // update language

                priceRange.setRangeValues(min, max);
                tvMinMaxPrice.setText(langResources.getString(R.string.EGP) + minRange + " - " + maxRange);
                priceRange.setSelectedMaxValue(maxRange);
                priceRange.setSelectedMinValue(minRange);

                bottomSheetFilter.setContentView(view);
                bottomSheetFilter.create();
                bottomSheetFilter.show();

                // set List Of Color && Sizes
                sizesListFilter.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
                SizesFilterAdapter sizesFilterAdapter = new SizesFilterAdapter(getContext(), SearchActivity.sizes);
                sizesListFilter.setAdapter(sizesFilterAdapter);

                colorsListFilter.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false));
                ColorsFilterAdapter colorsFilterAdapter = new ColorsFilterAdapter(getContext(), SearchActivity.colors);
                colorsListFilter.setAdapter(colorsFilterAdapter);

                // close bottom sheet
                view.findViewById(R.id.close_bottom_sheet_filters).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (bottomSheetFilter.isShowing()) {
                            bottomSheetFilter.dismiss();
                        } else {
                            bottomSheetFilter.show();
                        }
                    }
                });
                // range price
                priceRange.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener() {
                    @Override
                    public void onRangeSeekBarValuesChanged(RangeSeekBar bar, Object minValue, Object maxValue) {
                        maxRange = Integer.parseInt(maxValue.toString());
                        minRange = Integer.parseInt(minValue.toString());
                        tvMinMaxPrice.setText(langResources.getString(R.string.EGP) + minValue.toString() + " - " + maxValue.toString());
                        SearchActivity.priceMax = Integer.parseInt(maxValue.toString());
                        SearchActivity.priceMin = Integer.parseInt(minValue.toString());
                    }
                });

                searchApply.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                            bottomSheetFilter.dismiss();
                        } catch (Exception e) {
                        }

                    }
                });

                searchClearAll.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            SearchActivity.priceMax = Integer.parseInt(langResources.getString(R.string.num_top));
                            SearchActivity.priceMin = Integer.parseInt(langResources.getString(R.string.num_bottom));
                             SearchActivity.selectedSizes = "";
                            SearchActivity.selectedColors = "";
                            priceRange.setSelectedMaxValue(Integer.parseInt(langResources.getString(R.string.num_top)));
                            priceRange.setSelectedMinValue(Integer.parseInt(langResources.getString(R.string.num_bottom)));
                            tvMinMaxPrice.setText(langResources.getString(R.string.EGP) + (int) SearchActivity.priceMin + " - " + (int) SearchActivity.priceMax);
                            ((SearchActivity) getContext()).goSearchFromServerByFilter();
                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });

            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(getContext(), lang);
        langResources = langContext.getResources();
        try {
            list.setText(langResources.getString(R.string.list));
            filters.setText(langResources.getString(R.string.filters));
            popularity.setText(langResources.getString(R.string.popularity));
            ///////////// Popularity
            popularityAlpaz.setText(langResources.getString(R.string.alpaz));
            popularityAlpza.setText(langResources.getString(R.string.alpza));
            popularityLowHigh.setText(langResources.getString(R.string.low_high));
            popularityHighLow.setText(langResources.getString(R.string.high_low));
            popularityOldNew.setText(langResources.getString(R.string.old_new));
            popularityNewOld.setText(langResources.getString(R.string.new_old));
            popularityAll.setText(langResources.getString(R.string.all));
            sortBy.setText(langResources.getString(R.string.sort_by));
            //////////// Filter
            searchApply.setText(langResources.getString(R.string.apply));
            searchClearAll.setText(langResources.getString(R.string.clear_all));
            tvFilter.setText(langResources.getString(R.string.filter));
            tvPrice.setText(langResources.getString(R.string.price));
            tvSizes.setText(langResources.getString(R.string.sizes));
            tvColors.setText(langResources.getString(R.string.colors));

        } catch (Exception e) {
        }
    }

    private void searchList() {
        searchList.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        SearchAdapter searchAdapter = new SearchAdapter(getActivity(), SEARCH_LIST, SearchActivity.getSearchList());
        searchList.setAdapter(searchAdapter);
    }

    private void searchGrid() {
        searchList.setLayoutManager(new GridLayoutManager(getActivity(), 2, RecyclerView.VERTICAL, false));
        SearchAdapter searchAdapter = new SearchAdapter(getActivity(), SEARCH_GRID, SearchActivity.getSearchList());
        searchList.setAdapter(searchAdapter);
    }

    @Override
    public void onClick(View v) {

    }
}
