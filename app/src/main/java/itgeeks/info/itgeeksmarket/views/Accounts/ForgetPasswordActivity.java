package itgeeks.info.itgeeksmarket.views.Accounts;

import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class ForgetPasswordActivity extends AppCompatActivity {

    EditText etEmail;
    Button btn_send_mail;
    TextView navName, tvForgetPassHint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_forget_password);

        initViews();

        updateView(Paper.book().read("language").toString());
    }

    private void initViews() {
        navName = findViewById(R.id.nav_name);
        tvForgetPassHint = findViewById(R.id.tv_forget_pass_hint);
        btn_send_mail = findViewById(R.id.btn_send_mail);
        etEmail = findViewById(R.id.et_Email_forget_pass);

        // Nav Name
        navName.setText(R.string.forget_password);

        // Hover Email Shape
        etEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etEmail.setBackground(getDrawable(R.drawable.et_shape_hover));
                    etEmail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_mail_orange, 0, 0, 0);
                } else {
                    etEmail.setBackground(getDrawable(R.drawable.et_shape));
                    etEmail.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_mail, 0, 0, 0);
                }
            }
        });


        // btn Send Mail
        btn_send_mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etEmail.getText().toString().isEmpty()) {
                    etEmail.setError(langResources.getString(R.string.required));
                    etEmail.requestFocus();
                } else {
                    RetrofitClient.getInstance().executeConnectionToServer(ForgetPasswordActivity.this, PopularKeys.FORGET_PASSWORD, SharedPrefranceManager.getInastance(ForgetPasswordActivity.this).getLang(), new Request(etEmail.getText().toString()), new HandleResponse() {
                        @Override
                        public void handleTrueResponse(JsonObject mainObject) {
                            if (mainObject.get("status").getAsBoolean()){
                                Snackbar.make(findViewById(android.R.id.content),mainObject.get("message").getAsString(),3000).show();
                            }else {
                                etEmail.setError(mainObject.get("message").getAsString());
                            }
                        }

                        @Override
                        public void handleEmptyResponse() {

                        }

                        @Override
                        public void handleConnectionErrors(String errorMessage) {

                        }
                    });
                }
            }
        });

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.login));
            etEmail.setHint(langResources.getString(R.string.email_hint));
            btn_send_mail.setText(langResources.getString(R.string.send));
            tvForgetPassHint.setText(langResources.getString(R.string.forget_password_hint));
        } catch (Exception e) {
        }
    }
}
