package itgeeks.info.itgeeksmarket.views.MainFragements;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.NotificationAdapter;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class NotificationFragment extends Fragment {

    TextView navName;
    RecyclerView notificationList;

    public NotificationFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        initViews(view);

        updateView(Paper.book().read("language").toString());

        return view ;
    }

    private void initViews(View view) {
        // activity name
        navName = view.findViewById(R.id.nav_name);

        // init
        notificationList = view.findViewById(R.id.notification_list);

        // add notification to list
        notificationList.setLayoutManager(new LinearLayoutManager(getContext(),RecyclerView.VERTICAL,false));
        NotificationAdapter notificationAdapter = new NotificationAdapter(getContext());
        notificationList.setAdapter(notificationAdapter);

        // back
        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getContext()).MainFragment = new HomeFragment();
                if (((MainActivity)getContext()).MainFragment != null) {
                    ((MainActivity)getContext()).displayFragment(((MainActivity)getContext()).MainFragment);
                }
            }
        });

    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(getContext(), lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.notifications));
        } catch (Exception e) {
        }

    }

}
