package itgeeks.info.itgeeksmarket.views.HomeOptions;

import android.content.Intent;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import com.google.android.material.chip.Chip;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.*;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.ViewModels.SearchHistoryRepository;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import java.util.List;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class SearchActivity extends AppCompatActivity {

    public static EditText etSearch;
    FrameLayout clearSearchInput;
    Handler handler = new Handler();
    Runnable runnable;
    TextView btnBack , notificationNumber;
    private ProgressBar loadingCard;
    private LinearLayout btnFloatingIconCart;

    public static String listOrientation = "grid";

    public static String searchTerm = "";
    public static double priceMax = 300;
    public static double priceMin = 0;
    public static String selectedSizes = "";
    public static String selectedColors = "";
    public static String sortBy = "all";
    public static List<term_colors_sizes> sizes;
    public static List<term_colors_sizes> colors;

    private static List<product> searchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        displayFragment(new SerachHistoryFragment());

        initViews();

        updateView(Paper.book().read("language").toString());

        getSearchColorsAndSizes();

    }

    private void getSearchColorsAndSizes() {
        RetrofitClient.getInstance().executeConnectionToServer(
                this,
                PopularKeys.GET_COLOR_SIZE_LISTS,
                SharedPrefranceManager.getInastance(this).getLang(),
                new Request(SharedPrefranceManager.getInastance(this).getUser().getUser_id(),
                        SharedPrefranceManager.getInastance(this).getUser().getApi_token()),
                new HandleResponse() {
                    @Override
                    public void handleTrueResponse(JsonObject mainObject) {
                        colorsAndSizes colorsAndSizes = new Gson().fromJson(mainObject.get("lists").getAsJsonObject(), colorsAndSizes.class);
                        sizes = colorsAndSizes.getSizes();
                        colors = colorsAndSizes.getColors();
                    }

                    @Override
                    public void handleEmptyResponse() {

                    }

                    @Override
                    public void handleConnectionErrors(String errorMessage) {

                    }
                });
    }

    private void initViews() {
        etSearch = findViewById(R.id.et_search);
        clearSearchInput = findViewById(R.id.clear_search_input);
        loadingCard = findViewById(R.id.loading_card);
        btnFloatingIconCart = findViewById(R.id.btn_floating_icon_cart);
        notificationNumber = findViewById(R.id.notification_number);

        // On Search Change
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etSearch.getText().toString().trim().equals("")) {
                    displayFragment(new SerachHistoryFragment());
                    clearSearchInput.setVisibility(View.GONE);
                } else {
                    clearSearchInput.setVisibility(View.VISIBLE);
                    searchTerm = etSearch.getText().toString();
                    handler.removeCallbacks(runnable);
                    runnable = new Runnable() {
                        @Override
                        public void run() {
                            goSearchFromServerByFilter();
                            // Insert Search History
                            new SearchHistoryRepository(SearchActivity.this).removeSearch(new searchHistory(null, etSearch.getText().toString()));
                            new SearchHistoryRepository(SearchActivity.this).insertSearch(new searchHistory(null, etSearch.getText().toString()));
                        }
                    };

                    handler.postDelayed(runnable, 800);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //Clear Search Input Data
        clearSearchInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
            }
        });

        // go to cart
        btnFloatingIconCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SearchActivity.this,MainActivity.class);
                i.putExtra("backFromSearch","yes");
                startActivity(i);
            }
        });

        // notification number Observe
        new ProductRepository(this).getAllProduct().observe(this, new Observer<List<product>>() {
            @Override
            public void onChanged(List<product> products) {
                if (products.size()==0){
                    notificationNumber.setVisibility(View.GONE);
                }else if (products.size() > 9){
                    notificationNumber.setVisibility(View.VISIBLE);
                    notificationNumber.setText("9+");
                }else {
                    notificationNumber.setVisibility(View.VISIBLE);
                    notificationNumber.setText(String.valueOf(products.size()));
                }
            }
        });

        //back
        btnBack = findViewById(R.id.back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    //    go Search From Server By Filter
    public void goSearchFromServerByFilter() {

        displayLoading();
        RetrofitClient.getInstance().executeConnectionToServer(SearchActivity.this, "productSearchFilter", SharedPrefranceManager.getInastance(SearchActivity.this).getLang(), new Request(SharedPrefranceManager.getInastance(SearchActivity.this).getUser().getApi_token(), SharedPrefranceManager.getInastance(SearchActivity.this).getUser().getUser_id(), searchTerm, selectedSizes, selectedColors, sortBy, priceMax, priceMin),
                new HandleResponse() {
                    @Override
                    public void handleTrueResponse(JsonObject mainObject) {
                        try {
                            ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                            setSearchList(productResponse.getProducts());
                            displayFragment(new SearchBodyFragment());

                        } catch (Exception e) {
                        }
                        hideLoading();
                    }

                    @Override
                    public void handleEmptyResponse() {
                        hideLoading();
                    }

                    @Override
                    public void handleConnectionErrors(String errorMessage) {
                        Toast.makeText(SearchActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        hideLoading();
                    }
                });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            etSearch.setHint(langResources.getString(R.string.search));
            btnBack.setText(langResources.getString(R.string.cancel));
        } catch (Exception e) {
        }
    }

    public static List<product> getSearchList() {
        return searchList;
    }

    public void setSearchList(List<product> searchList) {
        this.searchList = searchList;
    }

    public void displayFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.search_container, fragment);
        fragmentTransaction.commit();
    }

    public void chooseTagItem(View view) {
        Chip vlaue = (Chip) view;
        etSearch.setText(vlaue.getText().toString());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(runnable);
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
