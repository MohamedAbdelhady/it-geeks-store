package itgeeks.info.itgeeksmarket.views.SettingsOptions;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.address;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class AddAddressActivity extends AppCompatActivity {
    TextView navName;
    EditText etBillFirstName, etBillLastName, etBillEmail, etBillAddress, etBillPhoneNumber;
    EditText etshipingFirstName, etshipingLastName, etshipingEmail, etshipingAddress, etshipingPhoneNumber;
    TextView btnSaveAddresses, tvBillAddressHint, tvShippingAddressHint, tvBillFirstName, tvBillLastName, tvBillEmail, tvBillAddress, tvBillPhoneNumber, tvshipingFirstName, tvshipingLastName, tvshipingEmail, tvshipingAddress, tvshipingPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        initView();

        updateView(Paper.book().read("language").toString());
    }

    private void initView() {

        // activity name
        navName = findViewById(R.id.nav_name);

        tvBillAddressHint = findViewById(R.id.tv_bill_address_hint);
        tvShippingAddressHint = findViewById(R.id.tv_shipping_address_hint);

        // init Views
        etBillFirstName = findViewById(R.id.et_bill_first_name);
        etBillLastName = findViewById(R.id.et_bill_last_name);
        etBillEmail = findViewById(R.id.et_bill_email_address);
        etBillAddress = findViewById(R.id.et_bill_address);
        etBillPhoneNumber = findViewById(R.id.et_bill_phone_number);
        etshipingFirstName = findViewById(R.id.et_shiping_first_name);
        etshipingLastName = findViewById(R.id.et_shiping_last_name);
        etshipingEmail = findViewById(R.id.et_shiping_email_address);
        etshipingAddress = findViewById(R.id.et_shiping_address);
        etshipingPhoneNumber = findViewById(R.id.et_shiping_phone_number);
        // hint
        tvBillFirstName = findViewById(R.id.tv_bill_first_name);
        tvBillLastName = findViewById(R.id.tv_bill_last_name);
        tvBillEmail = findViewById(R.id.tv_bill_email_address);
        tvBillAddress = findViewById(R.id.tv_bill_address);
        tvBillPhoneNumber = findViewById(R.id.tv_bill_phone_number);
        tvshipingFirstName = findViewById(R.id.tv_shiping_first_name);
        tvshipingLastName = findViewById(R.id.tv_shiping_last_name);
        tvshipingEmail = findViewById(R.id.tv_shiping_email_address);
        tvshipingAddress = findViewById(R.id.tv_shiping_address);
        tvshipingPhoneNumber = findViewById(R.id.tv_shiping_phone_number);

        // Update Data From Cach
        if (SharedPrefranceManager.getInastance(this).isLoggedIn()) {
            if (SharedPrefranceManager.getInastance(this).isAddressSaved()) { // If address saved before
                etBillFirstName.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_bill_first_name());
                etBillLastName.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_bill_last_name());
                etBillEmail.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_bill_email_address());
                etBillAddress.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_bill_address_line_1());
                etBillPhoneNumber.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_bill_phone_number());

                etshipingFirstName.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_first_name());
                etshipingLastName.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_last_name());
                etshipingEmail.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_email_address());
                etshipingAddress.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_address_line_1());
                etshipingPhoneNumber.setText(SharedPrefranceManager.getInastance(this).getAddress().getAccount_shipping_phone_number());
            } else {
                etBillFirstName.setText(SharedPrefranceManager.getInastance(this).getUser().getDisplay_name());
                etBillEmail.setText(SharedPrefranceManager.getInastance(this).getUser().getEmail());

                etshipingFirstName.setText(SharedPrefranceManager.getInastance(this).getUser().getDisplay_name());
                etshipingEmail.setText(SharedPrefranceManager.getInastance(this).getUser().getEmail());
            }
        } else {
            if (SharedPrefranceManager.getInastance(this).isAddressGuestSaved()) {
                etBillFirstName.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_first_name());
                etBillLastName.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_last_name());
                etBillEmail.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_email_address());
                etBillAddress.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_address_line_1());
                etBillPhoneNumber.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_bill_phone_number());

                etshipingFirstName.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_first_name());
                etshipingLastName.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_last_name());
                etshipingEmail.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_email_address());
                etshipingAddress.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_address_line_1());
                etshipingPhoneNumber.setText(SharedPrefranceManager.getInastance(this).getAddressGuest().getAccount_shipping_phone_number());
            }
        }

        btnSaveAddresses = findViewById(R.id.btn_save_addresses);
        btnSaveAddresses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etBillFirstName.getText().toString().isEmpty()) {
                    etBillFirstName.setError(langResources.getString(R.string.required));
                    etBillFirstName.requestFocus();
                } else if (etBillLastName.getText().toString().isEmpty()) {
                    etBillLastName.setError(langResources.getString(R.string.required));
                    etBillLastName.requestFocus();
                } else if (etBillEmail.getText().toString().isEmpty()) {
                    etBillEmail.setError(langResources.getString(R.string.required));
                    etBillEmail.requestFocus();
                } else if (etBillAddress.getText().toString().isEmpty()) {
                    etBillAddress.setError(langResources.getString(R.string.required));
                    etBillAddress.requestFocus();
                } else if (etBillPhoneNumber.getText().toString().isEmpty()) {
                    etBillPhoneNumber.setError(langResources.getString(R.string.required));
                    etBillPhoneNumber.requestFocus();
                } else if (etshipingFirstName.getText().toString().isEmpty()) {
                    etshipingFirstName.setError(langResources.getString(R.string.required));
                    etshipingFirstName.requestFocus();
                } else if (etshipingLastName.getText().toString().isEmpty()) {
                    etshipingLastName.setError(langResources.getString(R.string.required));
                    etshipingLastName.requestFocus();
                } else if (etshipingEmail.getText().toString().isEmpty()) {
                    etshipingEmail.setError(langResources.getString(R.string.required));
                    etshipingEmail.requestFocus();
                } else if (etshipingAddress.getText().toString().isEmpty()) {
                    etshipingAddress.setError(langResources.getString(R.string.required));
                    etshipingAddress.requestFocus();
                } else if (etshipingPhoneNumber.getText().toString().isEmpty()) {
                    etshipingPhoneNumber.setError(langResources.getString(R.string.required));
                    etshipingPhoneNumber.requestFocus();
                } else {
                    goUpdateDataFromServer(); // send request
                }
            }
        });


        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void goUpdateDataFromServer() {
        if (SharedPrefranceManager.getInastance(this).isLoggedIn()) {
            RetrofitClient.getInstance().executeConnectionToServer(
                    this,
                    PopularKeys.SET_USER_ADDRESS,
                    SharedPrefranceManager.getInastance(this).getLang(),
                    new Request(
                            SharedPrefranceManager.getInastance(AddAddressActivity.this).getUser().getApi_token(),
                            SharedPrefranceManager.getInastance(AddAddressActivity.this).getUser().getUser_id(),
                            etBillFirstName.getText().toString(),
                            etBillLastName.getText().toString(),
                            etBillEmail.getText().toString(),
                            etBillPhoneNumber.getText().toString(),
                            etBillAddress.getText().toString(),
                            etshipingFirstName.getText().toString(),
                            etshipingLastName.getText().toString(),
                            etshipingEmail.getText().toString(),
                            etshipingPhoneNumber.getText().toString(),
                            etshipingAddress.getText().toString()
                    ), new HandleResponse() {
                        @Override
                        public void handleTrueResponse(JsonObject mainObject) {
                            address address = new Gson().fromJson(mainObject.get("address").getAsJsonObject(), address.class);
                            SharedPrefranceManager.getInastance(AddAddressActivity.this).saveAddess(address);
                            Toast.makeText(AddAddressActivity.this, mainObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();

                            backToCheckout();
                        }

                        @Override
                        public void handleEmptyResponse() {
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_CANCELED, returnIntent);
                        }

                        @Override
                        public void handleConnectionErrors(String errorMessage) {
                            Toast.makeText(AddAddressActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_CANCELED, returnIntent);
                        }
                    });
        } else {
            SharedPrefranceManager.getInastance(AddAddressActivity.this).saveAddessGuest(new address(
                    etBillFirstName.getText().toString(),
                    etBillLastName.getText().toString(),
                    etBillEmail.getText().toString(),
                    etBillPhoneNumber.getText().toString(),
                    etBillAddress.getText().toString(),
                    etshipingFirstName.getText().toString(),
                    etshipingLastName.getText().toString(),
                    etshipingEmail.getText().toString(),
                    etshipingPhoneNumber.getText().toString(),
                    etshipingAddress.getText().toString()));
            Toast.makeText(AddAddressActivity.this, MainActivity.langResources.getString(R.string.address_saved_as_guest), Toast.LENGTH_SHORT).show();
            backToCheckout();
        }

    }

    private void backToCheckout() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("addressUpdated", "addressUpdated");
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        navName.setText(langResources.getString(R.string.my_addresses));

        tvBillAddressHint.setText(langResources.getString(R.string.bill));
        tvShippingAddressHint.setText(langResources.getString(R.string.shipping));

        etBillFirstName.setHint(langResources.getString(R.string.first_name_hint));
        etBillLastName.setHint(langResources.getString(R.string.last_name_hint));
        etBillEmail.setHint(langResources.getString(R.string.email));
        etBillAddress.setHint(langResources.getString(R.string.address));
        etBillPhoneNumber.setHint(langResources.getString(R.string.phone_hint));

        etshipingFirstName.setHint(langResources.getString(R.string.first_name_hint));
        etshipingLastName.setHint(langResources.getString(R.string.last_name_hint));
        etshipingEmail.setHint(langResources.getString(R.string.email));
        etshipingAddress.setHint(langResources.getString(R.string.address));
        etshipingPhoneNumber.setHint(langResources.getString(R.string.phone_hint));

        tvBillFirstName.setText(langResources.getString(R.string.first_name_hint));
        tvBillLastName.setText(langResources.getString(R.string.last_name_hint));
        tvBillEmail.setText(langResources.getString(R.string.email));
        tvBillAddress.setText(langResources.getString(R.string.address));
        tvBillPhoneNumber.setText(langResources.getString(R.string.phone_hint));

        tvshipingFirstName.setText(langResources.getString(R.string.first_name_hint));
        tvshipingLastName.setText(langResources.getString(R.string.last_name_hint));
        tvshipingEmail.setText(langResources.getString(R.string.email));
        tvshipingAddress.setText(langResources.getString(R.string.address));
        tvshipingPhoneNumber.setText(langResources.getString(R.string.phone_hint));

        btnSaveAddresses.setText(langResources.getString(R.string.save));
    }
}
