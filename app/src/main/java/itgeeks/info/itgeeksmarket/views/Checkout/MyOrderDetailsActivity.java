package itgeeks.info.itgeeksmarket.views.Checkout;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.ProductResponse;
import itgeeks.info.itgeeksmarket.Models.order;
import itgeeks.info.itgeeksmarket.Models.orderDatails;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.adapters.MyOrderDetailsAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.MainFragements.HomeFragment;
import itgeeks.info.itgeeksmarket.views.ProductActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class MyOrderDetailsActivity extends AppCompatActivity {

    private TextView navName, orderSummary, tvShipment, tvShippedOn, tvOrdered, tvProcessing, tvShipped, tvDelivered, tvItemsInOrder;
    private TextView orderKey, orderDate, tvSubTotal, tvShipping, tvTotal, tvCoupon, tvBalance, tvSubTotalHint, tvShippingHint, tvTotalHint, tvCouponHint, tvBalanceHint;
    private RecyclerView myOrderList;
    private ProgressBar loadingCard;
    SeekBar seekBarOrderStatus;
    private int orderID;
    private String orderProcessKey;

    TextView btnScreenShot, tvShoppingAddress, tvAddressName, tvAddressPhone, tvAddressInfo,tvAddressNameHint, tvAddressPhoneHint, tvAddressInfoHint, btnReOrderAllProducts;

    private PointF staringPoint = new PointF();
    private PointF pointerPoint = new PointF();
    private GestureDetector gestureDetector;
    private int screenHeight, screenWidth;

    private List<product> orderProductsList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_details);

        initViews();

        updateView(Paper.book().read("language").toString());

        getData();
    }

    private void initViews() {
        // activity name
        navName = findViewById(R.id.nav_name);

        //init
        btnScreenShot = findViewById(R.id.btn_screen_shot);

        myOrderList = findViewById(R.id.my_order_list);
        loadingCard = findViewById(R.id.loading_card);
        orderSummary = findViewById(R.id.order_summary);

        orderKey = findViewById(R.id.tv_order_process_key);
        orderDate = findViewById(R.id.tv_order_date);

        tvSubTotal = findViewById(R.id.tv_sub_total);
        tvShipping = findViewById(R.id.tv_shiping);
        tvTotal = findViewById(R.id.tv_total);
        tvCoupon = findViewById(R.id.tv_coupon);
        tvBalance = findViewById(R.id.tv_balance);

        tvSubTotalHint = findViewById(R.id.tv_sub_total_hint);
        tvShippingHint = findViewById(R.id.tv_shiping_hint);
        tvTotalHint = findViewById(R.id.tv_total_hint);
        tvCouponHint = findViewById(R.id.tv_coupon_hint);
        tvBalanceHint = findViewById(R.id.tv_balance_hint);

        tvShipment = findViewById(R.id.tv_shipment);
        tvShippedOn = findViewById(R.id.tv_shipped_on);
        tvOrdered = findViewById(R.id.tv_ordered);
        tvProcessing = findViewById(R.id.tv_processing);
        tvShipped = findViewById(R.id.tv_shipped);
        tvBalanceHint = findViewById(R.id.tv_balance_hint);
        tvDelivered = findViewById(R.id.tv_delivered);
        tvItemsInOrder = findViewById(R.id.tv_items_in_order);

        tvShoppingAddress = findViewById(R.id.tv_shopping_address);
        tvAddressName = findViewById(R.id.tv_address_name);
        tvAddressPhone = findViewById(R.id.tv_address_phone);
        tvAddressInfo = findViewById(R.id.tv_address_info);
        tvAddressNameHint = findViewById(R.id.tv_address_name_hint);
        tvAddressPhoneHint = findViewById(R.id.tv_address_phone_hint);
        tvAddressInfoHint = findViewById(R.id.tv_address_info_hint);

//        Re-Order All Products
        btnReOrderAllProducts = findViewById(R.id.btn_re_order_all_products);
        btnReOrderAllProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (product product : orderProductsList) {
                    new ProductRepository(MyOrderDetailsActivity.this).insertProduct(product);
                }
                Intent i = new Intent(MyOrderDetailsActivity.this, MainActivity.class);
                i.putExtra("backFromSearch", "yes");
                startActivity(i);
            }
        });

        seekBarOrderStatus = findViewById(R.id.seekbar_order_status);
        seekBarOrderStatus.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });

        // btn Screenshot
        btnScreenShot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getScreen(); // screenShot
            }
        });


        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.order_details));

            orderSummary.setText(langResources.getString(R.string.order_summary));
            tvSubTotalHint.setText(langResources.getString(R.string.subtotal));
            tvShippingHint.setText(langResources.getString(R.string.shipping));
            tvTotalHint.setText(langResources.getString(R.string.total));
            tvCouponHint.setText(langResources.getString(R.string.coupons));
            tvBalanceHint.setText(langResources.getString(R.string.balance));

            tvShipment.setText(langResources.getString(R.string.shipment));
            tvShippedOn.setText(langResources.getString(R.string.shipped_on));
            tvOrdered.setText(langResources.getString(R.string.ordered));
            tvProcessing.setText(langResources.getString(R.string.processing));
            tvShipped.setText(langResources.getString(R.string.shipped));
            tvDelivered.setText(langResources.getString(R.string.delivered));
            tvItemsInOrder.setText(langResources.getString(R.string.itemـinـyourـorder));
            btnScreenShot.setText(langResources.getString(R.string.take_screen_shot));
            tvShoppingAddress.setText(langResources.getString(R.string.shopping_address));
            btnReOrderAllProducts.setText(langResources.getString(R.string.re_order_all));

            tvAddressNameHint.setText(langResources.getString(R.string.name));
            tvAddressPhoneHint.setText(langResources.getString(R.string.phone_hint));
            tvAddressInfoHint.setText(langResources.getString(R.string.address));

        } catch (Exception e) {
        }
    }

    private void getData() {
        orderID = Integer.parseInt(getIntent().getExtras().get("order_id").toString());
        orderProcessKey = getIntent().getExtras().get("order_process_key").toString();
        displayLoading();
        RetrofitClient.getInstance().executeConnectionToServer(this, PopularKeys.GET_MY_ORDER_DETAILS, SharedPrefranceManager.getInastance(this).getLang(), new Request(SharedPrefranceManager.getInastance(this).getUser().getUser_id(), SharedPrefranceManager.getInastance(this).getUser().getApi_token(), orderID, orderProcessKey), new HandleResponse() {
            @Override
            public void handleTrueResponse(JsonObject mainObject) {
                hideLoading();
                ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                orderProductsList.addAll(productResponse.getOrder().getProducts());
                if (productResponse.getStatus()) {
                    if (productResponse.getOrder().getProducts().size() == 0) {
                    findViewById(R.id.order_page_empty).setVisibility(View.VISIBLE);
                    findViewById(R.id.full_page_data).setVisibility(View.GONE);
                } else {
                    setDataToViews(productResponse.getOrder().getOrder_datails());
                    // add products to list
                    myOrderList.setLayoutManager(new LinearLayoutManager(MyOrderDetailsActivity.this, RecyclerView.VERTICAL, false));
                    MyOrderDetailsAdapter myOrderDetailsAdapter = new MyOrderDetailsAdapter(MyOrderDetailsActivity.this, productResponse.getOrder().getProducts());
                    myOrderList.setAdapter(myOrderDetailsAdapter);
                }

                }else {
                    findViewById(R.id.order_page_empty).setVisibility(View.VISIBLE);
                    findViewById(R.id.full_page_data).setVisibility(View.GONE);
                }
            }

            @Override
            public void handleEmptyResponse() {
                hideLoading();

            }

            @Override
            public void handleConnectionErrors(String errorMessage) {
                hideLoading();
                findViewById(R.id.order_page_empty).setVisibility(View.VISIBLE);
                findViewById(R.id.full_page_data).setVisibility(View.GONE);
            }
        });
    }


    private void setDataToViews(orderDatails orderDatails) {
        orderKey.setText(MainActivity.langResources.getString(R.string.order_number) + " : " + orderDatails.getOrder_id() + "");
        orderDate.setText(MainActivity.langResources.getString(R.string.order_date) + " : " + orderDatails.getDate());
        tvSubTotal.setText(Double.parseDouble(orderDatails.getSubtotal()) + " " + MainActivity.langResources.getString(R.string.currency));
        tvShipping.setText(Double.parseDouble(orderDatails.getShipping_cost()) + " " + MainActivity.langResources.getString(R.string.currency));
        tvTotal.setText((Double.parseDouble(orderDatails.getSubtotal()) + Double.parseDouble(orderDatails.getShipping_cost())) + " " + MainActivity.langResources.getString(R.string.currency));
        tvCoupon.setText(Double.parseDouble(orderDatails.getDiscount()) + " " + MainActivity.langResources.getString(R.string.currency));
        tvBalance.setText(Double.parseDouble(orderDatails.getTotal()) + " " + MainActivity.langResources.getString(R.string.currency));
        tvAddressName.setText(orderDatails.getName());
        tvAddressPhone.setText(orderDatails.getPhone());
        tvAddressInfo.setText(orderDatails.getAddress());
        switch (orderDatails.getStatus().trim()) {
            case "on-hold":
                seekBarOrderStatus.setProgress(0);
                break;
            case "processing":
                seekBarOrderStatus.setProgress(1);
                break;
            case "shipping":
                seekBarOrderStatus.setProgress(2);
                break;
            case "completed":
                seekBarOrderStatus.setProgress(3);
                break;
        }
    }

    // Tack Screenshot
    private void getScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 99001);
        } else {
            takeScreenshot();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 99001 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            takeScreenshot();
        }
    }

    private void takeScreenshot() {
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        View v = getWindow().getDecorView().getRootView();
        v.setDrawingCacheEnabled(true);
        Bitmap bmp = Bitmap.createBitmap(v.getDrawingCache());
        v.setDrawingCacheEnabled(false);
        Toast.makeText(this, "Screenshot Done!", Toast.LENGTH_SHORT).show();

        try {
            File file = new File(Environment.getExternalStorageDirectory().toString() + "/DCIM/Screenshots/", "BayaaScreen" + now + ".jpg");
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            openScreenshot(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void openScreenshot(File imageFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        Uri uri = Uri.fromFile(imageFile);
        intent.setDataAndType(Uri.parse(String.valueOf(uri).replace("file", "content")), "image/*");
        startActivity(intent);
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        myOrderList.setVisibility(View.GONE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        myOrderList.setVisibility(View.VISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
