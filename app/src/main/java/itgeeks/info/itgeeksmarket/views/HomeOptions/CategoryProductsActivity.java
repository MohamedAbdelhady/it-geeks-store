package itgeeks.info.itgeeksmarket.views.HomeOptions;

import android.content.IntentFilter;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.ProductResponse;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.CategoryProductsAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Receivers.ConnectionChangeReceiver;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class CategoryProductsActivity extends AppCompatActivity {
    RecyclerView categoryProductsList;
    private ProgressBar loadingCard;
    TextView navName , tvEmptyData;
    // connection receiver
    private View snackContainer;
    private ConnectionChangeReceiver connectionChangeReceiver = new ConnectionChangeReceiver();
    LinearLayoutManager linearLayoutManager;

    // intent
    String caegoryName;
    String caegorySlug;

    // load more
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private static boolean loading = true;
    private int PageNum = 1;
    private int totalPages;
    CategoryProductsAdapter categoryProductsAdapter;
    private List<product> categoryProductsMainList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_products);

        initView();

        getCategoryData();
    }

    private void initView() {
        // set Username
        navName = findViewById(R.id.nav_name);
        registerReceiver(connectionChangeReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        // init
        categoryProductsList = findViewById(R.id.sale_product_list);
        loadingCard = findViewById(R.id.loading_card);
        tvEmptyData = findViewById(R.id.tv_empty_data);
        tvEmptyData.setText(MainActivity.langResources.getString(R.string.empty_data));

        linearLayoutManager = new LinearLayoutManager(CategoryProductsActivity.this, RecyclerView.VERTICAL, false);
        categoryProductsList.setLayoutManager(linearLayoutManager);
        categoryProductsAdapter = new CategoryProductsAdapter(CategoryProductsActivity.this, categoryProductsMainList);

        // Load More Data
        categoryProductsList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (PageNum <= totalPages){
                                getMoreProductFromServer();
                                PageNum++;
                            }

                        }
                    }

                }
            }
        });

        //back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void getCategoryData() {
        caegoryName = getIntent().getExtras().get("category_name").toString();
        caegorySlug = getIntent().getExtras().get("category_slug").toString();

        // activity name
        navName.setText(caegoryName);
        displayLoading();

        // get products
        RetrofitClient.getInstance().executeConnectionToServerLoadMore(this, PopularKeys.GET_PRODUCT_BY_CATEGORY_SLUG, SharedPrefranceManager.getInastance(this).getLang(),PageNum, new Request(SharedPrefranceManager.getInastance(this).getUser().getUser_id(), SharedPrefranceManager.getInastance(this).getUser().getApi_token(), caegorySlug), new HandleResponse() {
            @Override
            public void handleTrueResponse(JsonObject mainObject) {
                ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                hideLoading();

                totalPages = productResponse.getTotal();
                categoryProductsMainList.addAll(productResponse.getProducts());
                categoryProductsList.setAdapter(categoryProductsAdapter);

                // if empty data
                if (productResponse.getProducts().size() == 0){
                    categoryProductsList.setVisibility(View.GONE);
                    tvEmptyData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void handleEmptyResponse() {
                hideLoading();
            }

            @Override
            public void handleConnectionErrors(String errorMessage) {
                hideLoading();
            }
        });
    }

    private void getMoreProductFromServer() {
        findViewById(R.id.load_more_loading).setVisibility(View.VISIBLE);

        // get products
        RetrofitClient.getInstance().executeConnectionToServerLoadMore(this, PopularKeys.GET_PRODUCT_BY_CATEGORY_SLUG, SharedPrefranceManager.getInastance(this).getLang(),PageNum, new Request(SharedPrefranceManager.getInastance(this).getUser().getUser_id(), SharedPrefranceManager.getInastance(this).getUser().getApi_token(), caegorySlug), new HandleResponse() {
            @Override
            public void handleTrueResponse(JsonObject mainObject) {
                ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                categoryProductsMainList.addAll(productResponse.getProducts());
                categoryProductsAdapter.notifyDataSetChanged();
                loading = true;
                if (PageNum >= productResponse.getTotal()) loading = false;
                findViewById(R.id.load_more_loading).setVisibility(View.GONE);
            }

            @Override
            public void handleEmptyResponse() {
                hideLoading();
            }

            @Override
            public void handleConnectionErrors(String errorMessage) {
                hideLoading();
            }
        });
    }

    public View getSnackBarContainer() {
        if (snackContainer == null) {
            snackContainer = findViewById(R.id.snackbar_container);
        }
        return snackContainer;
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        categoryProductsList.setVisibility(View.GONE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        categoryProductsList.setVisibility(View.VISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(connectionChangeReceiver);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
