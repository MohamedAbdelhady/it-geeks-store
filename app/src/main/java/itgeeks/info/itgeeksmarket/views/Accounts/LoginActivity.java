package itgeeks.info.itgeeksmarket.views.Accounts;

import android.content.Intent;
import android.os.Build;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.user;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;


public class LoginActivity extends AppCompatActivity {

    Button BtnLogin;
    ImageView showPassIcon;
    EditText etUsername, etPassword;
    TextView navName , btForgetPassword ,btCreateAccount ,tvLoginHint1,tvLoginHint2,tvLoginHint3;
    private static boolean SHOW_PASS_STATUS;
    private ProgressBar loadingCard;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_login);

        initViews();

        updateView(Paper.book().read("language").toString());

    }

    private void initViews() {
        navName = findViewById(R.id.nav_name);
        BtnLogin = findViewById(R.id.btn_login);
        showPassIcon = findViewById(R.id.show_pass_icon);
        etUsername = findViewById(R.id.username);
        etPassword = findViewById(R.id.password);
        loadingCard = findViewById(R.id.loading_card);
        tvLoginHint1 = findViewById(R.id.tv_login_hint_1);
        tvLoginHint2 = findViewById(R.id.tv_login_hint_2);
        tvLoginHint3 = findViewById(R.id.tv_login_hint_3);

        // Nav Name
        navName.setText(getString(R.string.login));

        // Hover Email Shape
        etUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etUsername.setBackground(getDrawable(R.drawable.et_shape_hover));
                    etUsername.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_person_orange, 0, 0, 0);
                } else {
                    etUsername.setBackground(getDrawable(R.drawable.et_shape));
                    etUsername.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_person, 0, 0, 0);
                }
            }
        });

        // Hover Pass Shape
        etPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    etPassword.setBackground(getDrawable(R.drawable.et_shape_hover));
                    etPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_lock_orange, 0, 0, 0);
                } else {
                    etPassword.setBackground(getDrawable(R.drawable.et_shape));
                    etPassword.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_lock, 0, 0, 0);
                }
            }
        });

        // Login Event
        BtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUsername.getText().toString().isEmpty()) {
                    etUsername.setError(langResources.getString(R.string.required));
                    etUsername.requestFocus();
                } else if (etPassword.getText().toString().isEmpty()) {
                    etPassword.setError(langResources.getString(R.string.required));
                    etPassword.requestFocus();
                } else {
                    displayLoading();
                    RetrofitClient.getInstance().executeConnectionToServer(LoginActivity.this, PopularKeys.LOGIN_ACTION, SharedPrefranceManager.getInastance(LoginActivity.this).getLang(), new Request(etUsername.getText().toString(), etPassword.getText().toString()), new HandleResponse() {
                        @Override
                        public void handleTrueResponse(JsonObject mainObject) {
                            if (mainObject.get("status").getAsBoolean()) {
                                user user = new Gson().fromJson(mainObject.get("user"), user.class);
                                SharedPrefranceManager.getInastance(LoginActivity.this).saveUser(user);
                                Intent next = new Intent(LoginActivity.this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                        | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(next);
                                finish();

                            }
                            Toast.makeText(LoginActivity.this, mainObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            hideLoading();
                        }

                        @Override
                        public void handleEmptyResponse() {
                            hideLoading();
                        }

                        @Override
                        public void handleConnectionErrors(String errorMessage) {
                            Toast.makeText(LoginActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            hideLoading();
                        }
                    });

                }
            }
        });

        // Forget Password
        btForgetPassword = findViewById(R.id.bt_forget_password);
        btForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgetPasswordActivity.class));
            }
        });

        // create account
        btCreateAccount = findViewById(R.id.bt_create_account);
        btCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
            }
        });

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.login));
            etUsername.setHint(langResources.getString(R.string.user_name));
            etPassword.setHint(langResources.getString(R.string.password));
            BtnLogin.setText(langResources.getString(R.string.login));
            btForgetPassword.setText(langResources.getString(R.string.forget_pass));
            btCreateAccount.setText(langResources.getString(R.string.create_account));
            tvLoginHint1.setText(langResources.getString(R.string.choose_login_page_hint));
            tvLoginHint2.setText(langResources.getString(R.string.login_now));
            tvLoginHint3.setText(langResources.getString(R.string.hint_login));
        } catch (Exception e) {
        }
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
    
}
