package itgeeks.info.itgeeksmarket.views.Checkout;

import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.ProductResponse;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.MyOrderDetailsAdapter;
import itgeeks.info.itgeeksmarket.adapters.MyOrdersAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class MyOrdersActivity extends AppCompatActivity {

    TextView navName,tvOrderProcessKeyHint, tvOrderDateHint, tvOrderStatusHint, tvOrderTotalHint;
    RecyclerView myOrderList;
    private ProgressBar loadingCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_orders);

        initViews();

        updateView(Paper.book().read("language").toString());

        getData();

    }

    private void initViews() {
        // activity name
        navName = findViewById(R.id.nav_name);
        tvOrderProcessKeyHint = findViewById(R.id.tv_order_process_key_hint);
        tvOrderDateHint = findViewById(R.id.tv_order_date_hint);
        tvOrderStatusHint = findViewById(R.id.tv_order_status_hint);
        tvOrderTotalHint = findViewById(R.id.tv_order_total_hint);

        //init
        myOrderList = findViewById(R.id.my_orders_list);
        loadingCard = findViewById(R.id.loading_card);

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.my_orders));
            tvOrderProcessKeyHint.setText(langResources.getString(R.string.order_process_key));
            tvOrderDateHint.setText(langResources.getString(R.string.order_date));
            tvOrderStatusHint.setText(langResources.getString(R.string.order_status));
            tvOrderTotalHint.setText(langResources.getString(R.string.order_total));
        }catch (Exception e){}
    }

    private void getData() {
        displayLoading();
        RetrofitClient.getInstance().executeConnectionToServer(this, PopularKeys.GET_MY_ORDERS, SharedPrefranceManager.getInastance(this).getLang(), new Request(SharedPrefranceManager.getInastance(this).getUser().getUser_id(), SharedPrefranceManager.getInastance(this).getUser().getApi_token()), new HandleResponse() {
            @Override
            public void handleTrueResponse(JsonObject mainObject) {
                ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                if (productResponse.getStatus()) {
                    try {
                        if (productResponse.getOrders().size() == 0) {
                            findViewById(R.id.order_page_empty).setVisibility(View.VISIBLE);
                        } else {
                            hideLoading();
                            // add products to list
                            myOrderList.setLayoutManager(new LinearLayoutManager(MyOrdersActivity.this, RecyclerView.VERTICAL, false));
                            MyOrdersAdapter myOrdersAdapter = new MyOrdersAdapter(MyOrdersActivity.this, productResponse.getOrders());
                            myOrderList.setAdapter(myOrdersAdapter);
                        }
                    } catch (Exception e) {
                        findViewById(R.id.order_page_empty).setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void handleEmptyResponse() {
                hideLoading();
            }

            @Override
            public void handleConnectionErrors(String errorMessage) {
                hideLoading();
            }
        });
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        myOrderList.setVisibility(View.GONE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        myOrderList.setVisibility(View.VISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

}
