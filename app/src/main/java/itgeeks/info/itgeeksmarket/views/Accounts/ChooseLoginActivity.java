package itgeeks.info.itgeeksmarket.views.Accounts;

import android.content.Intent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class ChooseLoginActivity extends AppCompatActivity {

    TextView tvSkipLogin , tvChooseLoginHint1 ,tvChooseLoginHint2;
    Button btnLogin , btnRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.activity_choose_login);

        intiViews();

        updateView(Paper.book().read("language").toString());
    }

    private void intiViews() {
        tvSkipLogin = findViewById(R.id.tv_skip_login);
        tvChooseLoginHint1 = findViewById(R.id.tv_choose_login_hint_1);
        tvChooseLoginHint2 = findViewById(R.id.tv_choose_login_hint_2);
        btnLogin = findViewById(R.id.btn_login);
        btnRegister = findViewById(R.id.btn_register);

        // skip screen && open Home
        findViewById(R.id.skip_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChooseLoginActivity.this, MainActivity.class));
                finish();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChooseLoginActivity.this,LoginActivity.class));
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ChooseLoginActivity.this,RegisterActivity.class));
            }
        });

    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            tvSkipLogin.setText(langResources.getString(R.string.skip_login));
            tvChooseLoginHint1.setText(langResources.getString(R.string.choose_login_page_hint));
            tvChooseLoginHint2.setText(langResources.getString(R.string.choose_register_with_us));
            btnLogin.setText(langResources.getString(R.string.login));
            btnRegister.setText(langResources.getString(R.string.register));
        } catch (Exception e) {
        }

    }

}
