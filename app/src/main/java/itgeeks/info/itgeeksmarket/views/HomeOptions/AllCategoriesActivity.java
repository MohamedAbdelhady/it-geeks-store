package itgeeks.info.itgeeksmarket.views.HomeOptions;

import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.ProductResponse;
import itgeeks.info.itgeeksmarket.Models.categories;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.AllCategoriesAdapter;
import itgeeks.info.itgeeksmarket.adapters.SearchAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class AllCategoriesActivity extends AppCompatActivity {
    TextView navName, tvCategoryProducts, tvSubCategory;
    RecyclerView allCategoriesList, categoryProductsList;

    private List<product> categoryProductsMainList = new ArrayList<>();
    private List<categories> categoryData = new ArrayList<>();
    String categorySlug;

    private ProgressBar loadingCard;

    // load more
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private static boolean loading = true;
    private int PageNum = 1;
    private int totalPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);

        initViews();

    }

    private void initViews() {

        // activity name
        navName = findViewById(R.id.nav_name);
        navName.setText(getIntent().getExtras().getString("category_name"));

        //init
        allCategoriesList = findViewById(R.id.all_categories_list);
        categoryProductsList = findViewById(R.id.all_product_list);
        loadingCard = findViewById(R.id.loading_card);
        tvSubCategory = findViewById(R.id.tv_sub_category);
        tvCategoryProducts = findViewById(R.id.tv_category_products);

        // get Intent Data
        categoryData = (ArrayList<categories>) getIntent().getSerializableExtra("category");
        categorySlug = getIntent().getExtras().get("category_slug").toString();

        // initTextToViews
        tvSubCategory.setText(MainActivity.langResources.getString(R.string.sub_categories));
        tvCategoryProducts.setText(getIntent().getExtras().getString("category_name"));

        // add products to list
        addCategoriesToList();

        // add Product to list if i have a products
        addProductsToList();

        // back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void addProductsToList() {
        if (!categorySlug.trim().equals("null")) {

            // Title of Sub Category List
            tvSubCategory.setVisibility(View.VISIBLE);

            displayLoading();
            // get products
            RetrofitClient.getInstance().executeConnectionToServerLoadMore(this, PopularKeys.GET_PRODUCT_BY_CATEGORY_SLUG, SharedPrefranceManager.getInastance(this).getLang(), PageNum, new Request(SharedPrefranceManager.getInastance(this).getUser().getUser_id(), SharedPrefranceManager.getInastance(this).getUser().getApi_token(), categorySlug), new HandleResponse() {
                @Override
                public void handleTrueResponse(JsonObject mainObject) {
                    ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                    // Title of Products List
                    if (productResponse.getProducts().size() > 0) tvCategoryProducts.setVisibility(View.VISIBLE);

                    categoryProductsList.setLayoutManager(new GridLayoutManager(AllCategoriesActivity.this, 2, RecyclerView.VERTICAL, false));
                    SearchAdapter allCategoriesAdapter = new SearchAdapter(AllCategoriesActivity.this, R.layout.item_search_grid, productResponse.getProducts());
                    categoryProductsList.setAdapter(allCategoriesAdapter);
                    hideLoading();
                }

                @Override
                public void handleEmptyResponse() {
                    hideLoading();
                }

                @Override
                public void handleConnectionErrors(String errorMessage) {
                    hideLoading();
                }
            });
        }
    }

    // add product to List
    private void addCategoriesToList() {
        allCategoriesList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        AllCategoriesAdapter allCategoriesAdapter = new AllCategoriesAdapter(this, categoryData);
        allCategoriesList.setAdapter(allCategoriesAdapter);
    }

    // show loading
    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        categoryProductsList.setVisibility(View.GONE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    // hide loading
    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        categoryProductsList.setVisibility(View.VISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


}
