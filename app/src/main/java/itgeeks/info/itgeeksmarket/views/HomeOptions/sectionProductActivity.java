package itgeeks.info.itgeeksmarket.views.HomeOptions;

import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.ProductResponse;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.sectionProductsAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import java.util.ArrayList;
import java.util.List;

public class sectionProductActivity extends AppCompatActivity {
    private static final String TAG = "sectionProductActivity";
    RecyclerView ProductList;
    TextView navName, empty;
    private ProgressBar loadingCard;
    String categoryName;
    String requestAction = "";
    LinearLayoutManager linearLayoutManager;
    // load more
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private static boolean loading = true;
    private int PageNum = 1;
    private int totalPages;
    sectionProductsAdapter sectionProductsAdapter;
    private List<product> sectionMainList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_product);


        initView();

        if (getProductData()) { // get Product Data From Intent
            navName.setText(categoryName);
            // send Requset
            sendRequest();
        }
    }

    private void initView() {
        // activity name
        navName = findViewById(R.id.nav_name);

        // init
        ProductList = findViewById(R.id.new_product_list);
        empty = findViewById(R.id.section_page_empty);
        loadingCard = findViewById(R.id.loading_card);

        linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        sectionProductsAdapter = new sectionProductsAdapter(this, sectionMainList);
        ProductList.setLayoutManager(linearLayoutManager);

        // Load More Data
        ProductList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisiblesItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            if (PageNum <= totalPages){
                                getMoreProductFromServer();
                                PageNum++;
                            }

                        }
                    }
                }
            }
        });

        //back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void sendRequest() {
        // loading
        displayLoading();

        if (requestAction.equals(PopularKeys.GET_ALL_PRODUCT_RELATED)) {
            int productID = Integer.parseInt(getIntent().getExtras().get("product_id").toString());
            RetrofitClient.getInstance().executeConnectionToServerLoadMore(MainActivity.mainActivity, requestAction, SharedPrefranceManager.getInastance(this).getLang(), PageNum, new Request(SharedPrefranceManager.getInastance(sectionProductActivity.this).getUser().getUser_id(), SharedPrefranceManager.getInastance(sectionProductActivity.this).getUser().getApi_token(), productID), new HandleResponse() {
                @Override
                public void handleTrueResponse(JsonObject mainObject) {
                    ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                    setProductList(productResponse);
                    hideLoading();
                }

                @Override
                public void handleEmptyResponse() {
                    hideLoading();
                }

                @Override
                public void handleConnectionErrors(String errorMessage) {
                    hideLoading();
                }
            });
        } else {
            RetrofitClient.getInstance().executeConnectionToServerLoadMore(MainActivity.mainActivity, requestAction, SharedPrefranceManager.getInastance(this).getLang(), PageNum, new Request(SharedPrefranceManager.getInastance(sectionProductActivity.this).getUser().getUser_id(), SharedPrefranceManager.getInastance(sectionProductActivity.this).getUser().getApi_token()), new HandleResponse() {
                @Override
                public void handleTrueResponse(JsonObject mainObject) {
                    ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                    setProductList(productResponse);
                    hideLoading();
                }

                @Override
                public void handleEmptyResponse() {
                    hideLoading();
                }

                @Override
                public void handleConnectionErrors(String errorMessage) {
                    hideLoading();
                }
            });
        }
    }


    private boolean getProductData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            categoryName = extras.get("category_name").toString();
            requestAction = extras.get("request_action").toString();
        }
        return requestAction != null;
    }


    private void setProductList(final ProductResponse productData) {
        if (productData.getProducts().size() == 0) {
            empty.setText(MainActivity.langResources.getString(R.string.empty_data));
            empty.setVisibility(View.VISIBLE);
            ProductList.setVisibility(View.GONE);
        } else {
            empty.setVisibility(View.GONE);
            ProductList.setVisibility(View.VISIBLE);
            totalPages = productData.getTotal();
            sectionMainList.addAll(productData.getProducts());

            ProductList.setAdapter(sectionProductsAdapter);

        }
    }

    private void getMoreProductFromServer() {
        // loading
        displayLoading();
        findViewById(R.id.load_more_loading).setVisibility(View.VISIBLE);

        if (requestAction.equals(PopularKeys.GET_ALL_PRODUCT_RELATED)) {
            int productID = Integer.parseInt(getIntent().getExtras().get("product_id").toString());
            RetrofitClient.getInstance().executeConnectionToServerLoadMore(MainActivity.mainActivity, requestAction, SharedPrefranceManager.getInastance(this).getLang(), PageNum, new Request(SharedPrefranceManager.getInastance(sectionProductActivity.this).getUser().getUser_id(), SharedPrefranceManager.getInastance(sectionProductActivity.this).getUser().getApi_token(), productID), new HandleResponse() {
                @Override
                public void handleTrueResponse(JsonObject mainObject) {
                    ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                    sectionMainList.addAll(productResponse.getProducts());
                    hideLoading();
                    sectionProductsAdapter.notifyDataSetChanged();

                    loading = true;
                    if (PageNum >= productResponse.getTotal()) loading = false;
                    findViewById(R.id.load_more_loading).setVisibility(View.GONE);
                }

                @Override
                public void handleEmptyResponse() {
                    hideLoading();
                }

                @Override
                public void handleConnectionErrors(String errorMessage) {
                    hideLoading();
                }
            });
        } else {
            RetrofitClient.getInstance().executeConnectionToServerLoadMore(MainActivity.mainActivity, requestAction, SharedPrefranceManager.getInastance(this).getLang(), PageNum, new Request(SharedPrefranceManager.getInastance(sectionProductActivity.this).getUser().getUser_id(), SharedPrefranceManager.getInastance(sectionProductActivity.this).getUser().getApi_token()), new HandleResponse() {
                @Override
                public void handleTrueResponse(JsonObject mainObject) {
                    ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                    sectionMainList.addAll(productResponse.getProducts());
                    hideLoading();
                    sectionProductsAdapter.notifyDataSetChanged();
                    loading = true;
                    if (PageNum >= productResponse.getTotal()) loading = false;

                    findViewById(R.id.load_more_loading).setVisibility(View.GONE);
                }

                @Override
                public void handleEmptyResponse() {
                    hideLoading();
                }

                @Override
                public void handleConnectionErrors(String errorMessage) {
                    hideLoading();
                }
            });
        }
    }


    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }
}
