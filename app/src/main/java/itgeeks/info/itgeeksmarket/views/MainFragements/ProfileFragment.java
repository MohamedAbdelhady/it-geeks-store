package itgeeks.info.itgeeksmarket.views.MainFragements;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.transition.TransitionManager;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.Accounts.ChooseLoginActivity;
import itgeeks.info.itgeeksmarket.views.Checkout.SearchOrderDetailsActivity;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.SearchActivity;

import java.util.Locale;

import static itgeeks.info.itgeeksmarket.views.MainActivity.*;


public class ProfileFragment extends Fragment {
    ImageView open_drawer_menu;
    TextView tv_country, tv_address, tvLang, tv_profile, tvShopSettings, tvAccountSettings, tvTrackingOrder, etSearch, tvLogin, tvLogout;
    TextView langAR, langEN;

    TextView tvCloseAlertLang, tvTitleAlertLang;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        initViews(view);

        updateView(Paper.book().read("language").toString());

        return view;
    }

    private void initViews(final View view) {
        open_drawer_menu = view.findViewById(R.id.open_drawer_menu);
        etSearch = view.findViewById(R.id.et_search);
        tv_profile = view.findViewById(R.id.tv_profile);
        tv_address = view.findViewById(R.id.tv_my_addresses);
        tv_country = view.findViewById(R.id.tv_country);
        tvLang = view.findViewById(R.id.tv_lang);
        tvAccountSettings = view.findViewById(R.id.tv_account_settings);
        tvTrackingOrder = view.findViewById(R.id.tv_tracking_order);
        tvShopSettings = view.findViewById(R.id.tv_shop_settings);
        tvLogin = view.findViewById(R.id.tv_login);
        tvLogout = view.findViewById(R.id.tv_logout);

        if (!SharedPrefranceManager.getInastance(getContext()).isLoggedIn()) {
            view.findViewById(R.id.account_settings).setVisibility(View.GONE);
            tvLogin.setVisibility(View.VISIBLE);
            tvLogout.setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.account_settings).setVisibility(View.VISIBLE);
            tvLogin.setVisibility(View.GONE);
            tvLogout.setVisibility(View.VISIBLE);
        }


        // Login
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getContext()).logIn();
            }
        });

        // Logout
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getContext()).logOut();
            }
        });

        // navBar = view.findViewById(R.id.nav_bar);
        //  ((MainActivity)getActivity()).setSupportActionBar(navBar);


        open_drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //KeyFrameOneButton clicked create new ConstraintSet
                ConstraintSet constraintSet = new ConstraintSet();
                //Clone positioning information from @key_frame_one.xml in ConstraintSet
                TransitionManager.beginDelayedTransition(constrainLayout);
                if (Locale.getDefault().getLanguage().trim().equals("ar")){
                    constraintSet.clone(getContext(), R.layout.key_frame_one_ar);
                    constraintSet.connect(R.layout.key_frame_one_ar, ConstraintSet.LEFT, R.layout.key_frame_one_ar, ConstraintSet.RIGHT, 70);
                }else {
                    constraintSet.clone(getContext(), R.layout.key_frame_one);
                    constraintSet.connect(R.layout.key_frame_one, ConstraintSet.RIGHT, R.layout.key_frame_one, ConstraintSet.LEFT, 70);
                }
                constraintSet.applyTo(constrainLayout);
                navigation.setVisibility(View.GONE);
                menuFrame.setVisibility(View.VISIBLE);

                //menu back
                MainActivity.menuBack = true;
            }
        });

        // Search
        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SearchActivity.class));
            }
        });

        tv_country.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_country, null);
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setView(view);
                alert.create();
                alert.show();
            }
        });

        tvTrackingOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), SearchOrderDetailsActivity.class));
            }
        });

        tvLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = LayoutInflater.from(getContext()).inflate(R.layout.dialog_lang, null);
                langAR = view.findViewById(R.id.lang_ar);
                langEN = view.findViewById(R.id.lang_en);
                final AlertDialog alert = new AlertDialog.Builder(getContext()).create();
                alert.setView(view);
                alert.create();
                alert.show();

                // dismiss
                tvCloseAlertLang = view.findViewById(R.id.close_lang);
                tvTitleAlertLang = view.findViewById(R.id.title_lang_alert);
                tvTitleAlertLang.setText(langResources.getString(R.string.language));

                tvCloseAlertLang.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });
                tvCloseAlertLang.setText(langResources.getString(R.string.close));
                // Arabic
                langAR.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                        SharedPrefranceManager.getInastance(getContext()).saveLang("ar");
                        Paper.book().write("language", "ar");
                        updateView(Paper.book().read("language").toString());

                        ((MainActivity) getContext()).recreate();
                    }
                });

                // English
                langEN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                        SharedPrefranceManager.getInastance(getContext()).saveLang("en");
                        Paper.book().write("language", "en");
                        updateView(Paper.book().read("language").toString());
                        // ((MainActivity)getContext()).initLanguage();
                        ((MainActivity) getContext()).recreate();
                    }
                });
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(getContext(), lang);
        langResources = langContext.getResources();

        tvLang.setText(langResources.getString(R.string.language));
        tv_country.setText(langResources.getString(R.string.country));
        tv_profile.setText(langResources.getString(R.string.nav_profile));
        tv_address.setText(langResources.getString(R.string.my_addresses));
        tvAccountSettings.setText(langResources.getString(R.string.account_settings));
        tvShopSettings.setText(langResources.getString(R.string.shop_settings));
        tvTrackingOrder.setText(langResources.getString(R.string.tracking_orders));
        etSearch.setText(langResources.getString(R.string.search));
        tvLogin.setText(langResources.getString(R.string.login));
        tvLogout.setText(langResources.getString(R.string.log_out));

    }
}
