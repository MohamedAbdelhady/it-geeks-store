package itgeeks.info.itgeeksmarket.views.MainFragements;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.*;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.Models.recently;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductViewModel;
import itgeeks.info.itgeeksmarket.ViewModels.RecentlyRepository;
import itgeeks.info.itgeeksmarket.adapters.CartAdapter;
import itgeeks.info.itgeeksmarket.adapters.RecentlyAdapter;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.Accounts.LoginActivity;
import itgeeks.info.itgeeksmarket.views.Checkout.CheckoutActivity;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.SearchActivity;
import itgeeks.info.itgeeksmarket.views.ProductActivity;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static itgeeks.info.itgeeksmarket.views.MainActivity.*;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;


public class CartFragment extends Fragment {

    ImageView open_drawer_menu;
    Toolbar navBar;
    RecyclerView cartList, recentlyList;
    TextView etSearch, btnComplateYourOrder, cartTotalPrice, tvTotal, tvSeeAll, tvRecentlyViewed, tvEmptyCart;
    private ProgressBar loadingCard;
    private NestedScrollView cartFullScreen;
    ProductViewModel productViewModel;
    private Double total = 0.0;
    public static List<product> ListOfOrders = new ArrayList<>();

    public CartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);

        initViews(view);

        updateView(Paper.book().read("language").toString());

        return view;
    }

    private void initViews(final View view) {
        cartFullScreen = view.findViewById(R.id.cart_full_screen);
        loadingCard = view.findViewById(R.id.loading_card);
        open_drawer_menu = view.findViewById(R.id.open_drawer_menu);
        etSearch = view.findViewById(R.id.et_search);
        btnComplateYourOrder = view.findViewById(R.id.btn_complate_your_order);
        cartTotalPrice = view.findViewById(R.id.cart_total_price);
        tvEmptyCart = view.findViewById(R.id.tv_empty_cart);

        tvTotal = view.findViewById(R.id.tv_total);
        tvSeeAll = view.findViewById(R.id.tv_see_all);
        tvRecentlyViewed = view.findViewById(R.id.tv_recently_viewed);

        // navBar = view.findViewById(R.id.nav_bar);
        //  ((MainActivity)getActivity()).setSupportActionBar(navBar);

        cartList = view.findViewById(R.id.cart_list);
        recentlyList = view.findViewById(R.id.recently_list);

        // observ Product
        productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        productViewModel.getProducts().observe(this, new Observer<List<product>>() {
            @Override
            public void onChanged(List<product> product) {
                try {
                    ListOfOrders.clear();
                    ListOfOrders.addAll(product);
                    total = 0.0;
                    cartList.setLayoutManager(new LinearLayoutManager(getContext()));
                    CartAdapter CartAdapter = new CartAdapter(getContext(), product);
                    cartList.setAdapter(CartAdapter);
                    for (int i = 0; i < product.size(); i++) {
                        if (String.valueOf(product.get(i).getProduct_price()).isEmpty())
                            Toast.makeText(getContext(), "product number : " + product.get(i).getProduct_id() + " => have an none price", Toast.LENGTH_SHORT).show();
                        else
                            total += product.get(i).getProduct_price() * product.get(i).getQuantity();
                    }
                    cartTotalPrice.setText(total + " " +MainActivity.langResources.getString(R.string.currency));

                    // If Cart Empty
                    if (product.size() == 0) view.findViewById(R.id.div_no_item_in_cart).setVisibility(View.VISIBLE);
                    else view.findViewById(R.id.div_no_item_in_cart).setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // complate order
        btnComplateYourOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ListOfOrders.size() == 0) {
                    Toast.makeText(getContext(), MainActivity.langResources.getString(R.string.cart_is_empty), Toast.LENGTH_SHORT).show();
                } else {
                    Intent i = new Intent(new Intent(getContext(), CheckoutActivity.class));
                    i.putExtra("total", total);
                    i.putExtra("Previous_Page", "cart");
                    startActivity(i);
                }
            }
        });

        // Recently Viewed Observe From Caching
        new RecentlyRepository(getContext()).getAllProduct().observe(this, new Observer<List<recently>>() {
            @Override
            public void onChanged(List<recently> recently) {
                if (recently.size() != 0) {
                    recentlyList.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
                    RecentlyAdapter recentlyAdapter = new RecentlyAdapter(getContext(), recently);
                    recentlyList.setAdapter(recentlyAdapter);
                }
            }
        });


        open_drawer_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //KeyFrameOneButton clicked create new ConstraintSet
                ConstraintSet constraintSet = new ConstraintSet();
                //Clone positioning information from @key_frame_one.xml in ConstraintSet
                TransitionManager.beginDelayedTransition(constrainLayout);
                if (Locale.getDefault().getLanguage().trim().equals("ar")){
                    constraintSet.clone(getContext(), R.layout.key_frame_one_ar);
                    constraintSet.connect(R.layout.key_frame_one_ar, ConstraintSet.LEFT, R.layout.key_frame_one_ar, ConstraintSet.RIGHT, 70);
                }else {
                    constraintSet.clone(getContext(), R.layout.key_frame_one);
                    constraintSet.connect(R.layout.key_frame_one, ConstraintSet.RIGHT, R.layout.key_frame_one, ConstraintSet.LEFT, 70);
                }
                constraintSet.applyTo(constrainLayout);
                navigation.setVisibility(View.GONE);
                menuFrame.setVisibility(View.VISIBLE);

                //menu back
                MainActivity.menuBack = true;
            }
        });

        // Search
        etSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), SearchActivity.class));
            }
        });
    }

    public void displayFragment(Fragment fragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.main_frame, fragment);
        fragmentTransaction.commit();
    }

    public void displayLoading() {
        loadingCard.setVisibility(View.VISIBLE);
        cartFullScreen.setVisibility(View.GONE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideLoading() {
        loadingCard.setVisibility(View.GONE);
        cartFullScreen.setVisibility(View.VISIBLE);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(getContext(), lang);
        langResources = langContext.getResources();
        try {
            tvTotal.setText(langResources.getString(R.string.total));
            tvSeeAll.setText(langResources.getString(R.string.see_all));
            tvRecentlyViewed.setText(langResources.getString(R.string.recently_viewed));
            etSearch.setText(langResources.getString(R.string.search));
            btnComplateYourOrder.setText(langResources.getString(R.string.complete_your_ordar));
            tvEmptyCart.setText(langResources.getString(R.string.cart_is_empty));
        } catch (Exception e) {
        }

    }
}


