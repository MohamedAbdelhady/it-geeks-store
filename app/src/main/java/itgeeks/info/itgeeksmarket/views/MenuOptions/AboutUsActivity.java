package itgeeks.info.itgeeksmarket.views.MenuOptions;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import io.paperdb.Paper;
import itgeeks.info.itgeeksmarket.General.LocaleHelper;
import itgeeks.info.itgeeksmarket.R;

import static itgeeks.info.itgeeksmarket.views.MainActivity.langContext;
import static itgeeks.info.itgeeksmarket.views.MainActivity.langResources;

public class AboutUsActivity extends AppCompatActivity {
    TextView navName , tvAboutUsHint1,tvAboutUsHint2;
    Button btnContactUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        initView();

        updateView(Paper.book().read("language").toString());
    }

    private void initView() {
        // activity name
        tvAboutUsHint1 = findViewById(R.id.tv_about_us_hint_1);
        tvAboutUsHint2 = findViewById(R.id.tv_about_us_hint_2);
        btnContactUs = findViewById(R.id.btn_contact_us);
        btnContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AboutUsActivity.this,ContactUsActivity.class));
            }
        });


        navName = findViewById(R.id.nav_name);
        navName.setText(R.string.about_us);

        //back
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updateView(String lang) {
        langContext = LocaleHelper.setLocale(this, lang);
        langResources = langContext.getResources();
        try {
            navName.setText(langResources.getString(R.string.about_us));
            tvAboutUsHint1.setText(langResources.getString(R.string.about_us_hint1));
            tvAboutUsHint2.setText(langResources.getString(R.string.about_us_hint2));
            btnContactUs.setText(langResources.getString(R.string.contact_us));
        } catch (Exception e) {
        }
    }

}
