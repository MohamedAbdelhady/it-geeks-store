package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class MyOrdersViewHolder extends RecyclerView.ViewHolder {

    public TextView tvOrderProcessKey, tvOrderDate, tvOrderStatus, tvOrderTotal;

    public MyOrdersViewHolder(@NonNull View itemView, Context context) {
        super(itemView);

        initViews();

    }

    private void initViews() {
        tvOrderProcessKey = itemView.findViewById(R.id.tv_order_process_key);
        tvOrderDate = itemView.findViewById(R.id.tv_order_date);
        tvOrderStatus = itemView.findViewById(R.id.tv_order_status);
        tvOrderTotal = itemView.findViewById(R.id.tv_order_total);
    }

}
