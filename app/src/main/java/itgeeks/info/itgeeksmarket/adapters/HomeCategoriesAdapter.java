package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.Models.categories;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.HomeCategoriesViewHolder;
import itgeeks.info.itgeeksmarket.views.HomeOptions.AllCategoriesActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.CategoryProductsActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.sectionProductActivity;

import java.util.List;

public class HomeCategoriesAdapter extends RecyclerView.Adapter<HomeCategoriesViewHolder> {
    Context context;
    List<categories> categoriesList;

    public HomeCategoriesAdapter(Context context, List<categories> categoriesList) {
        this.context = context;
        this.categoriesList = categoriesList;
    }

    @NonNull
    @Override
    public HomeCategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_home_categories, parent, false);
        HomeCategoriesViewHolder homeCategoriesViewHolder = new HomeCategoriesViewHolder(view);
        return homeCategoriesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeCategoriesViewHolder holder, int position) {
        final categories categories = categoriesList.get(position);
    try {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, sectionProductActivity.class));
            }
        });

        holder.homeCategoryTitle.setText(categories.getName());
        Picasso.with(context).load(categories.getImage()).resize(250, 250).placeholder(R.drawable.progress_animation).into(holder.homeCategoryImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categories.getSub().size() == 0) { // if not have categories -> open Products
                    Intent intent = new Intent(context, CategoryProductsActivity.class);
                    intent.putExtra("category_name", categories.getName());
                    intent.putExtra("category_slug", categories.getSlug());
                    context.startActivity(intent);
                } else { //  if have Categories
                    Intent intent = new Intent(context, AllCategoriesActivity.class);
                    intent.putExtra("category_name", categories.getName());
                    intent.putExtra("category", categories.getSub());
                    intent.putExtra("category_slug", categories.getSlug());
                    context.startActivity(intent);
                }
            }
        });
    }catch (Exception e){e.printStackTrace();}
    }

    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
