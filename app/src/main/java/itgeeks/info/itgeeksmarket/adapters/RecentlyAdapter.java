package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.Models.recently;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.RecentlyViewHolder;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.ProductActivity;

import java.util.List;

public class RecentlyAdapter extends RecyclerView.Adapter<RecentlyViewHolder> {
    Context context;
    List<recently> recentlyList;

    public RecentlyAdapter(Context context, List<recently> recentlyList) {
        this.context = context;
        this.recentlyList = recentlyList;
    }

    @NonNull
    @Override
    public RecentlyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recently,parent,false);
        RecentlyViewHolder recentlyViewHolder = new RecentlyViewHolder(view);
        return recentlyViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecentlyViewHolder holder, int position) {
        final recently recently = recentlyList.get(position);

        holder.productTitle.setText(recently.getProduct_title());
        Picasso.with(context).load(recently.getProduct_img()).resize(250, 250).placeholder(R.drawable.progress_animation).into(holder.productImage);

        if (recently.getProduct_sale_price() == 0) {
            holder.newProductPrice.setText(recently.getProduct_price() +" "+MainActivity.langResources.getString(R.string.currency));
            holder.oldProductPrice.setVisibility(View.GONE);
        } else {
            holder.oldProductPrice.setVisibility(View.VISIBLE);
            holder.oldProductPrice.setText(recently.getProduct_sale_price() + "");
            holder.newProductPrice.setText(recently.getProduct_price() +" "+ MainActivity.langResources.getString(R.string.currency));
        }

        // open product
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("product_id", recently.getProduct_id());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recentlyList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
