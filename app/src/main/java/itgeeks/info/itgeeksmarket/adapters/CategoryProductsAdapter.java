package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.ViewModels.ProductViewModel;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.CategoryProductsViewHolder;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.Accounts.LoginActivity;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.ProductActivity;

import java.util.List;

public class CategoryProductsAdapter extends RecyclerView.Adapter<CategoryProductsViewHolder> {
    Context context;
    List<product> productList;
    // add product status
    private boolean isAddedToCart = false;

    public CategoryProductsAdapter(Context context, List<product> productList) {
        this.context = context;
        this.productList = productList;
    }

    @NonNull
    @Override
    public CategoryProductsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_search_list, parent, false);
        CategoryProductsViewHolder categoryProductsViewHolder = new CategoryProductsViewHolder(view);
        return categoryProductsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryProductsViewHolder holder, int position) {
        final product product = productList.get(position);

        holder.productBrands.setText(product.getProduct_brands());
        holder.productTitle.setText(product.getProduct_title());
        holder.productRateTotal.setText("( " + product.getProduct_rate_total() + " )");
        holder.rating.setRating(product.getProduct_rate());

        if (product.getProduct_sale_price() == 0) {
            holder.newProductPrice.setText(product.getProduct_price() +" "+MainActivity.langResources.getString(R.string.currency));
            holder.oldProductPrice.setVisibility(View.GONE);
        } else {
            holder.oldProductPrice.setVisibility(View.VISIBLE);
            holder.oldProductPrice.setText(product.getProduct_sale_price() + "");
            holder.newProductPrice.setText(product.getProduct_price() +" "+ MainActivity.langResources.getString(R.string.currency));
        }

        Picasso.with(context).load(product.getProduct_img()).placeholder(R.drawable.progress_animation).into(holder.imgProduct);

        final Boolean[] isWished = {product.isWished()};
        // if added to favorite before
        if (isWished[0]) {
            holder.btnAddToFavorite.setBackground(context.getDrawable(R.drawable.ic_favorite_red));
        }

        ProductViewModel productViewModel = ViewModelProviders.of((FragmentActivity) context).get(ProductViewModel.class);
        productViewModel.getProductByID(product.getProduct_id()).observe(((FragmentActivity) context), new Observer<product>() {
            @Override
            public void onChanged(product product) {
                if (product != null) {
                    holder.BtnAddToCart.setBackground(context.getDrawable(R.drawable.btn_shoping_cart_added));
                    holder.BtnAddToCart.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_shopping_cart_white, 0, 0, 0);
                } else {
                    holder.BtnAddToCart.setBackground(context.getDrawable(R.drawable.btn_shoping_cart));
                    holder.BtnAddToCart.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_add_white_24dp, 0, 0, 0);
                }
            }
        });

        holder.BtnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    // Check if product added to cart
                    if (isAddedToCart) {
                        product.setQuantity(1);
                        new ProductRepository(context).removeProduct(product);
                        isAddedToCart = false;
                    } else {
                        product.setQuantity(1);
                        product.setProduct_type("simple_product");
                        product.setVarition_id(0);
                        new ProductRepository(context).insertProduct(product);
                        isAddedToCart = true;
                    }
            }
        });

        // add to favorite
        holder.btnAddToFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SharedPrefranceManager.getInastance(context).isLoggedIn()) {
                    //context.startActivity(new Intent(context, LoginActivity.class));
                    Snackbar snack = Snackbar.make(holder.btnAddToFavorite,MainActivity.langResources.getString(R.string.need_to_be_user),2000);
                    View view = snack.getView();
                    FrameLayout.LayoutParams params =(FrameLayout.LayoutParams)view.getLayoutParams();
                    params.setMargins(params.leftMargin,
                            params.topMargin,
                            params.rightMargin,
                            params.bottomMargin + 100);
                    view.setLayoutParams(params);
                    snack.show();
                } else {
                    if (isWished[0] == false) {
                        //   Add To Favorite
                        RetrofitClient.getInstance().executeConnectionToServer(context, PopularKeys.SET_FAVORITE_PRODUCT, SharedPrefranceManager.getInastance(context).getLang(), new Request(SharedPrefranceManager.getInastance(context).getUser().getUser_id(), SharedPrefranceManager.getInastance(context).getUser().getApi_token(), product.getProduct_id()), new HandleResponse() {
                            @Override
                            public void handleTrueResponse(JsonObject mainObject) {
                                holder.btnAddToFavorite.setBackground(context.getDrawable(R.drawable.ic_favorite_red));
                                new ProductRepository(context).updateFavoriteIcon(true, product.getProduct_id());
                                isWished[0] = true;
                            }

                            @Override
                            public void handleEmptyResponse() {

                            }

                            @Override
                            public void handleConnectionErrors(String errorMessage) {

                            }
                        });
                    }
                }
            }
        });


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("product_id", product.getProduct_id());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
