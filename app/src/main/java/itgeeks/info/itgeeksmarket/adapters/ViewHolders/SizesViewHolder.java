package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class SizesViewHolder extends RecyclerView.ViewHolder {
    public TextView tvSize;

    public SizesViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }

    private void initViews() {
        tvSize = itemView.findViewById(R.id.tv_size);
    }
}
