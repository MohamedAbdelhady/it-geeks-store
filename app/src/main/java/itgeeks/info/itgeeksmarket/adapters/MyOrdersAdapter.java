package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.JsonObject;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.orders;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.MyOrdersViewHolder;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.Checkout.MyOrderDetailsActivity;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import java.util.List;

public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersViewHolder> {
    Context context;
    List<orders> oredersList;


    public MyOrdersAdapter(Context context, List<orders> oredersList) {
        this.context = context;
        this.oredersList = oredersList;
    }

    @NonNull
    @Override
    public MyOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_my_orders, parent, false);
        MyOrdersViewHolder myOrdersViewHolder = new MyOrdersViewHolder(view, context);
        return myOrdersViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrdersViewHolder holder, int position) {
        final orders orders = oredersList.get(position);

        holder.tvOrderProcessKey.setText(orders.getOrder_id()+"");
        holder.tvOrderDate.setText(orders.getOrder_date());
        holder.tvOrderStatus.setText(orders.getOrder_status());
        holder.tvOrderTotal.setText(orders.getOrder_total() +" "+MainActivity.langResources.getString(R.string.currency));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MyOrderDetailsActivity.class);
                intent.putExtra("order_id", orders.getOrder_id());
                intent.putExtra("order_process_key", orders.getOrder_process_key());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return oredersList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    private void initRateProduct(int productID, float rate, String comment) {
        RetrofitClient.getInstance().executeConnectionToServer(context, PopularKeys.SET_USER_REVIEW, SharedPrefranceManager.getInastance(context).getLang(),
                new Request(
                        SharedPrefranceManager.getInastance(context).getUser().getUser_id(),
                        SharedPrefranceManager.getInastance(context).getUser().getApi_token(),
                        productID,
                        comment,
                        rate),
                new HandleResponse() {
                    @Override
                    public void handleTrueResponse(JsonObject mainObject) {
                        if (mainObject.get("status").getAsBoolean()) {
                            Toast.makeText(context, mainObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void handleEmptyResponse() {

                    }

                    @Override
                    public void handleConnectionErrors(String errorMessage) {

                    }
                });
    }
}
