package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.Models.comments;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.ReviewViewHolder;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import java.util.List;

import static android.media.CamcorderProfile.get;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewViewHolder> {
    Context context;
    List<comments> reviewsList;

    public ReviewAdapter(Context context, List<comments> reviewsList) {
        this.context = context;
        this.reviewsList = reviewsList;
    }

    @NonNull
    @Override
    public ReviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_review,parent,false);
        ReviewViewHolder reviewViewHolder = new ReviewViewHolder(view);
        return reviewViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewViewHolder holder, int position) {
        comments comments = reviewsList.get(position);
        holder.reviewMessage.setText(comments.getComment());
        holder.reviewUsername.setText(comments.getUser());
        holder.reviewDate.setText(comments.getDate());
        holder.reviewRate.setRating(comments.getRate());
        holder.tvBy.setText(MainActivity.langResources.getString(R.string.by));
    }

    @Override
    public int getItemCount() {
        return reviewsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
