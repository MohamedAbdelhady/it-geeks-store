package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.Models.categories;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.SubCategoriesViewHolder;
import itgeeks.info.itgeeksmarket.views.HomeOptions.AllCategoriesActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.CategoryProductsActivity;

import java.util.List;

public class SubCategoriesAdapter extends RecyclerView.Adapter<SubCategoriesViewHolder> {
    Context context;
    List<categories> categoriesList;
    private static boolean isSubCategoryOpen = false;

    public SubCategoriesAdapter(Context context, List<categories> categoriesList) {
        this.context = context;
        this.categoriesList = categoriesList;
    }

    @NonNull
    @Override
    public SubCategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SubCategoriesViewHolder(LayoutInflater.from(context).inflate(R.layout.item_sub_category, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final SubCategoriesViewHolder holder, final int position) {
        final categories categories = categoriesList.get(position);

        holder.titleAllCategory.setText(categories.getName());
        holder.countSubCategories.setText("( " + categories.getProducts_count() + " )");

        // open next activity
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNextActivity(categories);
            }
        });

        // hide first line in list
        if (position == 0) holder.subCategoryLineH.setVisibility(View.GONE);


    }

    private void openNextActivity(categories categories) {
        if (categories.getSub().size() == 0) { // if not have categories -> open Products
            Intent intent = new Intent(context, CategoryProductsActivity.class);
            intent.putExtra("category_name", categories.getName());
            intent.putExtra("category_slug", categories.getSlug());
            context.startActivity(intent);
        } else { //  if have Categories
            Intent intent = new Intent(context, AllCategoriesActivity.class);
            intent.putExtra("category_name", categories.getName());
            intent.putExtra("category", categories.getSub());
            intent.putExtra("category_slug", categories.getSlug());
            context.startActivity(intent);
        }
    }


    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
