package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class FavoriteViewHolder extends RecyclerView.ViewHolder {

    public TextView productTitle , newProductPrice , oldProductPrice , btnRemove , btnAddToCart , productBrand;
    public ImageView productImage ;
    public FavoriteViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }

    private void initViews() {
        productImage = itemView.findViewById(R.id.product_image);
        productBrand = itemView.findViewById(R.id.product_brands);
        productTitle = itemView.findViewById(R.id.product_title);
        newProductPrice = itemView.findViewById(R.id.new_product_price);
        oldProductPrice = itemView.findViewById(R.id.old_product_price);
        btnRemove = itemView.findViewById(R.id.remove_from_cart);
        btnAddToCart = itemView.findViewById(R.id.btn_add_to_cart);

        // Visibility
        btnRemove.setVisibility(View.VISIBLE);
        btnAddToCart.setVisibility(View.VISIBLE);

        oldProductPrice.setPaintFlags(oldProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);


    }

}
