package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class MyOrderDetailsViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgProduct;
    public TextView productBrands, productTitle, newProductPrice , oldProductPrice;
    public TextView rateOrder , countDown, countUp ;
//    public TextView reOrder;
    public EditText counterProductNum;
    Context context;

    public MyOrderDetailsViewHolder(@NonNull View itemView, Context context) {
        super(itemView);
        this.context = context;

        initViews();

    }

    private void initViews() {
        rateOrder = itemView.findViewById(R.id.rate_order);
//        reOrder = itemView.findViewById(R.id.btn_add_to_cart);
        imgProduct = itemView.findViewById(R.id.product_image);
        productBrands = itemView.findViewById(R.id.product_brands);
        productTitle = itemView.findViewById(R.id.product_title);
        oldProductPrice = itemView.findViewById(R.id.old_product_price);
        newProductPrice = itemView.findViewById(R.id.new_product_price);
        countDown = itemView.findViewById(R.id.count_down);
        countUp = itemView.findViewById(R.id.count_up);
        counterProductNum = itemView.findViewById(R.id.counter_product_num);

        //  line over a Old Price
        oldProductPrice.setPaintFlags(oldProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

    }

}
