package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.graphics.Paint;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class SummaryViewHolder extends RecyclerView.ViewHolder {
    LinearLayout orderOptions;
    public ImageView imgProduct ;
    public TextView productBrands, productTitle, newProductPrice, oldProductPrice;
    public TextView countDown, countUp ;
    public EditText counterProductNum;
    public SummaryViewHolder(@NonNull View itemView) {
        super(itemView);

        initViews();

    }

    private void initViews() {
        imgProduct = itemView.findViewById(R.id.product_image);
        productBrands = itemView.findViewById(R.id.product_brands);
        productTitle = itemView.findViewById(R.id.product_title);
        oldProductPrice = itemView.findViewById(R.id.old_product_price);
        newProductPrice = itemView.findViewById(R.id.new_product_price);

        orderOptions = itemView.findViewById(R.id.order_options);
        orderOptions.setVisibility(View.GONE);

        countDown = itemView.findViewById(R.id.count_down);
        countUp = itemView.findViewById(R.id.count_up);
        counterProductNum = itemView.findViewById(R.id.counter_product_num);

        //  line over a Old Price
        oldProductPrice.setPaintFlags(oldProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }
}
