package itgeeks.info.itgeeksmarket.adapters.SliderViewPagers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class SliderDotsAdapter extends RecyclerView.Adapter<SliderDotsAdapter.DotsHolder> {
    Context context;
    int count, currentItem;

    public SliderDotsAdapter(Context context, int count, int currentItem) {
        this.context = context;
        this.count = count;
        this.currentItem = currentItem;
    }

    @NonNull
    @Override
    public DotsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_dots, parent, false);
        DotsHolder dotsHolder = new DotsHolder(view);
        return dotsHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull DotsHolder holder, int position) {
        if (position == currentItem)
            holder.dot.setBackground(context.getDrawable(R.drawable.cercle_orange));
    }

    @Override
    public int getItemCount() {
        return count;
    }

    class DotsHolder extends RecyclerView.ViewHolder {
        FrameLayout dot;

        public DotsHolder(@NonNull View itemView) {
            super(itemView);
            dot = itemView.findViewById(R.id.dot);
        }
    }
}
