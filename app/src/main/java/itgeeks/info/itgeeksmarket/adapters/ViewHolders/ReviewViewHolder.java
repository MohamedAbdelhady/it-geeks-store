package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class ReviewViewHolder extends RecyclerView.ViewHolder {
    public TextView reviewMessage, reviewUsername, reviewDate , tvBy;
    public RatingBar reviewRate;

    public ReviewViewHolder(@NonNull View itemView) {
        super(itemView);

        initViews();
    }

    private void initViews() {
        reviewMessage = itemView.findViewById(R.id.review_message);
        reviewUsername = itemView.findViewById(R.id.review_username);
        reviewDate = itemView.findViewById(R.id.review_date);
        reviewRate = itemView.findViewById(R.id.review_rate);
        tvBy = itemView.findViewById(R.id.tv_by);
    }

}
