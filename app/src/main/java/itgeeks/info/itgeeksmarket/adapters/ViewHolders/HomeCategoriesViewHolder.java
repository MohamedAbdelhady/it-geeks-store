package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class HomeCategoriesViewHolder extends RecyclerView.ViewHolder {

    public ImageView homeCategoryImage;
    public TextView homeCategoryTitle;

    public HomeCategoriesViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }

    private void initViews() {
        homeCategoryImage = itemView.findViewById(R.id.home_categories_image);
        homeCategoryTitle = itemView.findViewById(R.id.home_categories_title);
    }

}
