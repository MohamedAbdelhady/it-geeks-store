package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class sectionProductViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgProduct ,btnAddToFavorite;
    public TextView productBrands, productTitle, newProductPrice, oldProductPrice, productRateTotal , btnAddToCart;
    public RatingBar rating;

    public sectionProductViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }


    private void initViews() {
        imgProduct = itemView.findViewById(R.id.product_image);
        productBrands = itemView.findViewById(R.id.product_brands);
        productTitle = itemView.findViewById(R.id.product_title);
        oldProductPrice = itemView.findViewById(R.id.old_product_price);
        newProductPrice = itemView.findViewById(R.id.new_product_price);
        productRateTotal = itemView.findViewById(R.id.product_rate_total);
        rating = itemView.findViewById(R.id.product_rate);
        btnAddToFavorite = itemView.findViewById(R.id.btn_add_to_favorite);
        btnAddToCart = itemView.findViewById(R.id.btn_add_to_cart);

        //  line over a Old Price
        oldProductPrice.setPaintFlags(oldProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

}
