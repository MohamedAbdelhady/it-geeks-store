package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.Models.term_colors_sizes;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.ColorsViewHolder;

import java.util.List;

public class ColorsAdapter extends RecyclerView.Adapter<ColorsViewHolder> {
    Context context;
    List<term_colors_sizes> sizesList;

    public ColorsAdapter(Context context, List<term_colors_sizes> sizesList) {
        this.context = context;
        this.sizesList = sizesList;
    }

    @NonNull
    @Override
    public ColorsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_color,parent,false);
        ColorsViewHolder colorsViewHolder = new ColorsViewHolder(view);
        return colorsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ColorsViewHolder holder, int position) {
        term_colors_sizes color = sizesList.get(position);
        holder.productColors.setCardBackgroundColor(Color.parseColor("#"+color.getColor()));

    }

    @Override
    public int getItemCount() {
        return sizesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
