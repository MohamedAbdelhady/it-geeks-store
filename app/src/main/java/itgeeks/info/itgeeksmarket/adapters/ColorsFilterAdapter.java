package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.Models.term_colors_sizes;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.ColorsFilterViewHolder;
import itgeeks.info.itgeeksmarket.views.HomeOptions.SearchActivity;

import java.util.List;

public class ColorsFilterAdapter extends RecyclerView.Adapter<ColorsFilterViewHolder> {
    Context context;
    List<term_colors_sizes> colorsList;
    public static int colorIndex;
    public ColorsFilterAdapter(Context context, List<term_colors_sizes> colorsList) {
        this.context = context;
        this.colorsList = colorsList;
    }

    @NonNull
    @Override
    public ColorsFilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_color,parent,false);
        ColorsFilterViewHolder colorsFilterViewHolder = new ColorsFilterViewHolder(view);
        return colorsFilterViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ColorsFilterViewHolder holder, final int position) {
        final term_colors_sizes color = colorsList.get(position);
        holder.productColors.setCardBackgroundColor(Color.parseColor("#"+color.getColor()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // SearchActivity.selectedColors = color.getSlug();
                colorIndex = position;
                notifyDataSetChanged();
                SearchActivity.selectedColors = color.getSlug();
            }
        });
        if (colorIndex == position) holder.selectedColor.setVisibility(View.VISIBLE);
        else holder.selectedColor.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return colorsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
