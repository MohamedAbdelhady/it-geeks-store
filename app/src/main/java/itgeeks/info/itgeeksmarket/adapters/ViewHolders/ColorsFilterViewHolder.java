package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.view.View;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class ColorsFilterViewHolder extends RecyclerView.ViewHolder {
    public CardView productColors;
    public ImageView selectedColor;

    public ColorsFilterViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }

    private void initViews() {
        productColors = itemView.findViewById(R.id.img_product_color);
        selectedColor = itemView.findViewById(R.id.selected_color);
    }
}
