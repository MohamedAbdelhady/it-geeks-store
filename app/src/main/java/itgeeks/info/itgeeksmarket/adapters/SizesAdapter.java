package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.Models.term_colors_sizes;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.SizesViewHolder;

import java.util.List;

public class SizesAdapter extends RecyclerView.Adapter<SizesViewHolder> {
    Context context;
    List<term_colors_sizes> sizesList;

    public SizesAdapter(Context context, List<term_colors_sizes> sizesList) {
        this.context = context;
        this.sizesList = sizesList;
    }

    @NonNull
    @Override
    public SizesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_size,parent,false);
        SizesViewHolder sizesViewHolder = new SizesViewHolder(view);
        return sizesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SizesViewHolder holder, int position) {
        term_colors_sizes size = sizesList.get(position);
        holder.tvSize.setText(size.getName()+"");
    }

    @Override
    public int getItemCount() {
        return sizesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
