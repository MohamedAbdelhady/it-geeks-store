package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.CartViewHolder;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.ProductActivity;

import java.util.List;

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder> {
    Context context;
    List<product> productList;

    public CartAdapter(Context context, List<product> productList) {
        this.context = context;
        this.productList = productList;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_favorite_cart,parent,false);
        CartViewHolder cartViewHolder = new CartViewHolder(view);
        return cartViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final CartViewHolder holder, final int position) {
        final product product = productList.get(position);

        holder.productBrand.setText(product.getProduct_brands());
        holder.productTitle.setText(product.getProduct_title());
        Picasso.with(context).load(product.getProduct_img()).resize(250, 250).placeholder(R.drawable.progress_animation).into(holder.productImage);
        holder.counterProductNum.setText(product.getQuantity()+"");
        if (product.getProduct_sale_price() == 0) {
            holder.newProductPrice.setText(product.getProduct_price() +" "+MainActivity.langResources.getString(R.string.currency));
            holder.oldProductPrice.setVisibility(View.GONE);
        } else {
            holder.oldProductPrice.setVisibility(View.VISIBLE);
            holder.oldProductPrice.setText(product.getProduct_sale_price() + "");
            holder.newProductPrice.setText(product.getProduct_price() +" "+MainActivity.langResources.getString(R.string.currency));
        }

        holder.counterProductNum.setText(product.getQuantity()+"");

        // counter Up & Down
        holder.countUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.getQuantity() >= 999)
                    holder.counterProductNum.setText(999+"");
                else {
                    new ProductRepository(context).updateProductCount(product.getQuantity()+1,product.getProduct_id());
                }
            }
        });
        //counter Down
        holder.countDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.getQuantity() <= 1)
                    holder.counterProductNum.setText(1+"");
                else {
                    new ProductRepository(context).updateProductCount(product.getQuantity()-1,product.getProduct_id());
                }
            }
        });

        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initRemoveItemDialog(position, product);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("product_id", product.getProduct_id());
                context.startActivity(intent);
            }
        });

        final Boolean[] isWished = {product.isWished()};
        // if added to favorite before
        if (SharedPrefranceManager.getInastance(context).isLoggedIn()){
            if(isWished[0]){
                holder.btnAddToFavorite.setBackground(context.getDrawable(R.drawable.ic_favorite_red));
            }
        }

        // add to favorite
        holder.btnAddToFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SharedPrefranceManager.getInastance(context).isLoggedIn()) {
                    //context.startActivity(new Intent(context, LoginActivity.class));
                    Snackbar snack = Snackbar.make(holder.btnAddToFavorite, MainActivity.langResources.getString(R.string.need_to_be_user), 2000);
                    View view = snack.getView();
                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
                    params.setMargins(params.leftMargin,
                            params.topMargin,
                            params.rightMargin,
                            params.bottomMargin + 100);
                    view.setLayoutParams(params);
                    snack.show();
                } else {
                    if (isWished[0] == false) {
                        //   Add To Favorite
                        RetrofitClient.getInstance().executeConnectionToServer(context, PopularKeys.SET_FAVORITE_PRODUCT, SharedPrefranceManager.getInastance(context).getLang(), new Request(SharedPrefranceManager.getInastance(context).getUser().getUser_id(), SharedPrefranceManager.getInastance(context).getUser().getApi_token(), product.getProduct_id()), new HandleResponse() {
                            @Override
                            public void handleTrueResponse(JsonObject mainObject) {
                                holder.btnAddToFavorite.setBackground(context.getDrawable(R.drawable.ic_favorite_red));
                                new ProductRepository(context).updateFavoriteIcon(true, product.getProduct_id());
                                isWished[0] = true;
                            }

                            @Override
                            public void handleEmptyResponse() {

                            }

                            @Override
                            public void handleConnectionErrors(String errorMessage) {

                            }
                        });
                    }
                }
            }
        });

    }

    private void initRemoveItemDialog(final int position, final product product) {
        androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(context);
        alert.setMessage(MainActivity.langResources.getString(R.string.question_delete_product));
        alert.setPositiveButton(MainActivity.langResources.getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new ProductRepository(context).removeProduct(product);
            }
        });
        alert.setNegativeButton(MainActivity.langResources.getString(R.string.cancel), null);
        alert.show();
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
