package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.Models.searchHistory;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.SearchHistoryRepository;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.SearchHistoryViewHolder;
import itgeeks.info.itgeeksmarket.views.HomeOptions.SearchActivity;
import itgeeks.info.itgeeksmarket.views.MainActivity;

import java.util.List;

public class SearchHistoryAdapter extends RecyclerView.Adapter<SearchHistoryViewHolder> {
    Context context;
    List<searchHistory> searchHistoryList;

    public SearchHistoryAdapter(Context context, List<searchHistory> searchHistoryList) {
        this.context = context;
        this.searchHistoryList = searchHistoryList;
    }

    @NonNull
    @Override
    public SearchHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_search_tag,parent,false);
        SearchHistoryViewHolder sizesViewHolder = new SearchHistoryViewHolder(view);
        return sizesViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchHistoryViewHolder holder, int position) {
        final searchHistory searchHistory = searchHistoryList.get(position);
        holder.tvSearchTag.setText(searchHistory.getSearchValue());
        holder.tvSearchTag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchActivity.etSearch.setText(searchHistory.getSearchValue()); //  search again
               // new SearchHistoryRepository(context).removeSearch(searchHistory);
            }
        });
        holder.tvSearchTag.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                new SearchHistoryRepository(context).removeSearch(searchHistory);
                Toast.makeText(context, MainActivity.langResources.getString(R.string.tag_deleted), Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return searchHistoryList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
