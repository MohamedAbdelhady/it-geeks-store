package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.Models.term_colors_sizes;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.SizesFilterViewHolder;
import itgeeks.info.itgeeksmarket.views.HomeOptions.SearchActivity;

import java.util.List;

public class SizesFilterAdapter extends RecyclerView.Adapter<SizesFilterViewHolder> {
    Context context;
    List<term_colors_sizes> sizesList;
    public static int sizeIndex;

    public SizesFilterAdapter(Context context, List<term_colors_sizes> sizesList) {
        this.context = context;
        this.sizesList = sizesList;
    }

    @NonNull
    @Override
    public SizesFilterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_size,parent,false);
        SizesFilterViewHolder sizesFilterViewHolder = new SizesFilterViewHolder(view);
        return sizesFilterViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final SizesFilterViewHolder holder, final int position) {
        final term_colors_sizes size = sizesList.get(position);
        holder.tvSize.setText(size.getName()+"");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // SearchActivity.selectedSizes = size.getSlug();
                sizeIndex = position;
                notifyDataSetChanged();
                SearchActivity.selectedSizes = size.getSlug();
            }
        });
        if (sizeIndex == position) holder.tvSize.setTextColor(context.getResources().getColor(R.color.dark_sky_blue));
        else holder.tvSize.setTextColor(context.getResources().getColor(R.color.black));
    }

    @Override
    public int getItemCount() {
        return sizesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
