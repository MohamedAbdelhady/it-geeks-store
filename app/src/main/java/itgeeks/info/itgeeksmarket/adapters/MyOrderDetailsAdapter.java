package itgeeks.info.itgeeksmarket.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.MyOrderDetailsViewHolder;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.ProductActivity;

import java.util.List;

public class MyOrderDetailsAdapter extends RecyclerView.Adapter<MyOrderDetailsViewHolder> {
    private Context context;
    private List<product> oredersList;


    public MyOrderDetailsAdapter(Context context, List<product> oredersList) {
        this.context = context;
        this.oredersList = oredersList;
    }

    @NonNull
    @Override
    public MyOrderDetailsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_my_order_details, parent, false);
        MyOrderDetailsViewHolder myOrderDetailsViewHolder = new MyOrderDetailsViewHolder(view, context);
        return myOrderDetailsViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyOrderDetailsViewHolder holder, int position) {
        final product product = oredersList.get(position);

        holder.rateOrder.setText(MainActivity.langResources.getString(R.string.rate_this));
//        holder.reOrder.setText(MainActivity.langResources.getString(R.string.re_order));

        holder.productBrands.setText(product.getProduct_brands());
        holder.productTitle.setText(product.getProduct_title());
        Picasso.with(context).load(product.getProduct_img()).placeholder(R.drawable.progress_animation).into(holder.imgProduct);

        if (product.getProduct_sale_price() == 0) {
            holder.newProductPrice.setText(product.getProduct_price() + "");
            holder.oldProductPrice.setVisibility(View.GONE);
        } else {
            holder.oldProductPrice.setVisibility(View.VISIBLE);
            holder.oldProductPrice.setText(product.getProduct_sale_price() + "");
            holder.newProductPrice.setText(product.getProduct_price() + "");
        }

        //counter
        holder.counterProductNum.setText(product.getQuantity()+"");

        // counter Up & Down
        holder.countUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.getQuantity() >= 999)
                    holder.counterProductNum.setText(999+"");
                else {
                    product.setQuantity(product.getQuantity()+1);
                    holder.counterProductNum.setText(product.getQuantity()+"");
                }
            }
        });
        //counter Down
        holder.countDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (product.getQuantity() <= 1)
                    holder.counterProductNum.setText(1+"");
                else {
                    product.setQuantity(product.getQuantity()-1);
                    holder.counterProductNum.setText(product.getQuantity()+"");
                }
            }
        });

////        Re-Order Single Product
//        holder.reOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                product.setQuantity(Integer.parseInt(holder.counterProductNum.getText().toString()));
//                new ProductRepository(context).insertProduct(product);
//                // go to cart
//                        Intent i = new Intent(context, MainActivity.class);
//                        i.putExtra("backFromSearch", "yes");
//                        context.startActivity(i);
//            }
//        });


//        Rate order *****
        holder.rateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final View view = LayoutInflater.from(context).inflate(R.layout.dialog_rate_product, null);
                final RatingBar ratingBar = view.findViewById(R.id.add_rate_to_order);
                final EditText comment = view.findViewById(R.id.add_comment_to_order);
                final TextView tvRateTitleHint = view.findViewById(R.id.tv_rate_title_hint);
                final TextView btnClose = view.findViewById(R.id.close_rate);
                final TextView btnSendRate = view.findViewById(R.id.btn_send_rate);

                comment.setHint(MainActivity.langResources.getString(R.string.Enter_your_product_comment));
                btnSendRate.setText(MainActivity.langResources.getString(R.string.send));
                btnClose.setText(MainActivity.langResources.getString(R.string.close));
                tvRateTitleHint.setText(MainActivity.langResources.getString(R.string.rate_a_product));

                final AlertDialog alertRate = new AlertDialog.Builder(context).create();
                alertRate.setView(view);
                alertRate.setCancelable(false);
                alertRate.create();
                alertRate.show();

                btnSendRate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (comment.getText().toString().trim().isEmpty()) {
                            comment.setError(MainActivity.langResources.getString(R.string.required));
                        } else {
                            initRateProduct(product.getProduct_id(), ratingBar.getRating(), comment.getText().toString());
                            alertRate.dismiss();
                        }

                    }
                });

                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertRate.dismiss();
                    }
                });
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("product_id", product.getProduct_id());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return oredersList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    private void initRateProduct(int productID, float rate, String comment) {
        RetrofitClient.getInstance().executeConnectionToServer(context, PopularKeys.SET_USER_REVIEW, SharedPrefranceManager.getInastance(context).getLang(),
                new Request(
                        SharedPrefranceManager.getInastance(context).getUser().getUser_id(),
                        SharedPrefranceManager.getInastance(context).getUser().getApi_token(),
                        productID,
                        comment,
                        rate),
                new HandleResponse() {
                    @Override
                    public void handleTrueResponse(JsonObject mainObject) {
                        if (mainObject.get("status").getAsBoolean()) {
                            Toast.makeText(context, mainObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void handleEmptyResponse() {

                    }

                    @Override
                    public void handleConnectionErrors(String errorMessage) {

                    }
                });
    }
}
