package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.BestForYouViewHolder;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.ProductActivity;

import java.util.List;

public class BestForYouAdapter extends RecyclerView.Adapter<BestForYouViewHolder> {
    Context context;
    List<product> bestForYouList;

    public BestForYouAdapter(Context context, List<product> bestForYouList) {
        this.context = context;
        this.bestForYouList = bestForYouList;
    }

    @NonNull
    @Override
    public BestForYouViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recently,parent,false);
        BestForYouViewHolder bestForYouViewHolder = new BestForYouViewHolder(view);
        return bestForYouViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BestForYouViewHolder holder, int position) {
        final product product = bestForYouList.get(position);

        holder.productTitle.setText(product.getProduct_title());
        Picasso.with(context).load(product.getProduct_img()).resize(250, 250).placeholder(R.drawable.progress_animation).into(holder.productImage);

        if (product.getProduct_sale_price() == 0) {
            holder.newProductPrice.setText(product.getProduct_price() +" "+MainActivity.langResources.getString(R.string.currency));
            holder.oldProductPrice.setVisibility(View.GONE);
        } else {
            holder.oldProductPrice.setVisibility(View.VISIBLE);
            holder.oldProductPrice.setText(product.getProduct_sale_price() + "");
            holder.newProductPrice.setText(product.getProduct_price() +" "+ MainActivity.langResources.getString(R.string.currency));
        }

        // open product
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("product_id", product.getProduct_id());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bestForYouList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
