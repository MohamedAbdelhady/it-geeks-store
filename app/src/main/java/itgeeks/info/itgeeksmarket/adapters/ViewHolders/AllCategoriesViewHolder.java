package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class AllCategoriesViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgAllCategory , toggleArrowIcon;
    public TextView titleAllCategory , countAllCategories,noDataToSee;
    public LinearLayout categoryItem;
    public RecyclerView subCatigoriesList;
    public RelativeLayout btnToggleSubCategory;

    public AllCategoriesViewHolder(@NonNull View itemView) {
        super(itemView);
        imgAllCategory = itemView.findViewById(R.id.img_all_category);
        titleAllCategory = itemView.findViewById(R.id.title_all_categories);
        countAllCategories = itemView.findViewById(R.id.count_all_categories);
        categoryItem = itemView.findViewById(R.id.category_item);
        subCatigoriesList = itemView.findViewById(R.id.sub_catigories_list);
        btnToggleSubCategory = itemView.findViewById(R.id.btn_toggle_sub_category);
        toggleArrowIcon = itemView.findViewById(R.id.toggle_arrow_icon);
        noDataToSee = itemView.findViewById(R.id.no_data_to_see);

    }

}
