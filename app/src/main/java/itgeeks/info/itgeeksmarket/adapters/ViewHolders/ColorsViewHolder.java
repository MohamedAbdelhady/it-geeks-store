package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import itgeeks.info.itgeeksmarket.R;

public class ColorsViewHolder extends RecyclerView.ViewHolder {
    public CardView productColors;

    public ColorsViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }

    private void initViews() {
        productColors = itemView.findViewById(R.id.img_product_color);
    }
}
