package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.Models.categories;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.AllCategoriesViewHolder;
import itgeeks.info.itgeeksmarket.views.HomeOptions.AllCategoriesActivity;
import itgeeks.info.itgeeksmarket.views.HomeOptions.CategoryProductsActivity;

import java.util.List;

public class AllCategoriesAdapter extends RecyclerView.Adapter<AllCategoriesViewHolder> {
    Context context;
    List<categories> categoriesList;
    boolean isSubCategoryOpen = false;

    public AllCategoriesAdapter(Context context, List<categories> categoriesList) {
        this.context = context;
        this.categoriesList = categoriesList;
    }

    @NonNull
    @Override
    public AllCategoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AllCategoriesViewHolder(LayoutInflater.from(context).inflate(R.layout.item_all_categories, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final AllCategoriesViewHolder holder, final int position) {
        final categories categories = categoriesList.get(position);

        holder.titleAllCategory.setText(categories.getName());
        holder.countAllCategories.setText("( " + categories.getProducts_count() + " )");

        if (categories.getImage() == null) { // TODO CHECK IT > Will Delete Later
            Picasso.with(context).load(categories.getCategory_image()).resize(400, 400).placeholder(R.drawable.progress_animation).into(holder.imgAllCategory);
        } else {
            Picasso.with(context).load(categories.getImage()).resize(400, 400).placeholder(R.drawable.progress_animation).into(holder.imgAllCategory);
        }


        // open next activity
        holder.categoryItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openNextActivity(categories);
            }
        });

        holder.subCatigoriesList.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        SubCategoriesAdapter subCategoriesAdapter = new SubCategoriesAdapter(context, categories.getSub());
        holder.subCatigoriesList.setAdapter(subCategoriesAdapter);

        // If Have No Sub Category
        if (categories.getSub().size() == 0) {
            holder.btnToggleSubCategory.setEnabled(false);
            holder.toggleArrowIcon.setBackground(context.getDrawable(R.drawable.ic_arrow_right_mirror_disable));
        }

        // open Toggle Sub Categories
        holder.btnToggleSubCategory.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isSubCategoryOpen) {
                    isSubCategoryOpen = false;
                    holder.subCatigoriesList.setVisibility(View.GONE);
                    holder.noDataToSee.setVisibility(View.GONE);
                    holder.toggleArrowIcon.setBackground(context.getDrawable(R.drawable.ic_arrow_right_mirror));
                } else {
                    isSubCategoryOpen = true;
                    holder.subCatigoriesList.setVisibility(View.VISIBLE);
                    holder.toggleArrowIcon.setBackground(context.getDrawable(R.drawable.ic_expand_more_black_24dp));
                    // show message if List Empty
                    if (categories.getSub().size() == 0) holder.noDataToSee.setVisibility(View.VISIBLE);
                    else holder.noDataToSee.setVisibility(View.GONE);
                }
            }
        });

    }

    private void openNextActivity(categories categories) {
        if (categories.getSub().size() == 0) { // if not have categories -> open Products
            Intent intent = new Intent(context, CategoryProductsActivity.class);
            intent.putExtra("category_name", categories.getName());
            intent.putExtra("category_slug", categories.getSlug());
            context.startActivity(intent);
        } else { //  if have Categories
            Intent intent = new Intent(context, AllCategoriesActivity.class);
            intent.putExtra("category_name", categories.getName());
            intent.putExtra("category", categories.getSub());
            intent.putExtra("category_slug", categories.getSlug());
            context.startActivity(intent);
        }
    }


    @Override
    public int getItemCount() {
        return categoriesList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
