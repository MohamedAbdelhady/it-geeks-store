package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class SubCategoriesViewHolder extends RecyclerView.ViewHolder {


    public TextView titleAllCategory, countSubCategories ;
    public LinearLayout categoryItem;
    public View subCategoryLineH;

    public SubCategoriesViewHolder(@NonNull View itemView) {
        super(itemView);
        titleAllCategory = itemView.findViewById(R.id.title_all_categories);
        categoryItem = itemView.findViewById(R.id.category_item);
        subCategoryLineH = itemView.findViewById(R.id.sub_category_line_h);
        countSubCategories = itemView.findViewById(R.id.count_sub_categories);
    }

}
