package itgeeks.info.itgeeksmarket.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.General.PopularKeys;
import itgeeks.info.itgeeksmarket.Models.ProductResponse;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;
import itgeeks.info.itgeeksmarket.ViewModels.ProductViewModel;
import itgeeks.info.itgeeksmarket.adapters.ViewHolders.FavoriteViewHolder;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.repository.restfulApi.HandleResponse;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RetrofitClient;
import itgeeks.info.itgeeksmarket.repository.restfulApi.Storage.SharedPrefranceManager;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.MainFragements.FavoriteFragment;
import itgeeks.info.itgeeksmarket.views.ProductActivity;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteViewHolder> {
    Context context;
    List<product> favoriteList;
    // add product status
    private boolean isAddedToCart = false;

    public FavoriteAdapter(Context context, List<product> favoriteList) {
        this.context = context;
        this.favoriteList = favoriteList;
    }

    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_favorite_cart, parent, false);
        FavoriteViewHolder holder = new FavoriteViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final FavoriteViewHolder holder, final int position) {

        final product product = favoriteList.get(position);

        holder.productBrand.setText(product.getProduct_brands());
        holder.productTitle.setText(product.getProduct_title());
        Picasso.with(context).load(product.getProduct_img()).resize(250, 250).placeholder(R.drawable.progress_animation).into(holder.productImage);

        if (product.getProduct_sale_price() == 0) {
            holder.newProductPrice.setText(product.getProduct_price() +" "+MainActivity.langResources.getString(R.string.currency));
            holder.oldProductPrice.setVisibility(View.GONE);
        } else {
            holder.oldProductPrice.setText(product.getProduct_price() + "");
            holder.oldProductPrice.setVisibility(View.VISIBLE);
            holder.newProductPrice.setText(product.getProduct_sale_price() +" "+MainActivity.langResources.getString(R.string.currency));
        }

        ProductViewModel productViewModel = ViewModelProviders.of((FragmentActivity) context).get(ProductViewModel.class);
        productViewModel.getProductByID(product.getProduct_id()).observe(((FragmentActivity) context), new Observer<product>() {
            @Override
            public void onChanged(product product) {
                if (product != null){
                    holder.btnAddToCart.setBackground(context.getDrawable(R.drawable.btn_shoping_cart_added));
                    holder.btnAddToCart.setText(MainActivity.langResources.getString(R.string.added_to_cart));
                    holder.btnAddToCart.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_shopping_cart_white, 0, 0, 0);
                }else {
                    holder.btnAddToCart.setBackground(context.getResources().getDrawable(R.drawable.btn_shoping_cart));
                    holder.btnAddToCart.setText(MainActivity.langResources.getString(R.string.add_to_cart));
                    holder.btnAddToCart.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_add_white_24dp, 0, 0, 0);
                }

            }
        });

        // add to cart
        holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check if product added to cart
                if (isAddedToCart) {
                    product.setQuantity(1);
                    new ProductRepository(context).removeProduct(product);
                    isAddedToCart = false;
                } else {
                    product.setQuantity(1);
                    product.setProduct_type("simple_product");
                    product.setVarition_id(0);
                    new ProductRepository(context).insertProduct(product);
                    isAddedToCart = true;
                }
            }
        });

        // remove
        holder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initRemoveItemDialog(position, product);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProductActivity.class);
                intent.putExtra("product_id", product.getProduct_id());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }


    private void initRemoveItemDialog(final int position, final product product) {
        androidx.appcompat.app.AlertDialog.Builder alert = new androidx.appcompat.app.AlertDialog.Builder(context);
        alert.setMessage(MainActivity.langResources.getString(R.string.question_delete_product));
        alert.setPositiveButton(MainActivity.langResources.getString(R.string.delete), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FavoriteFragment.displayLoading2();
                RetrofitClient.getInstance().executeConnectionToServer(context, PopularKeys.REMOVE_FAVORITE_PRODUCT, SharedPrefranceManager.getInastance(context).getLang(), new Request(SharedPrefranceManager.getInastance(context).getUser().getUser_id(), SharedPrefranceManager.getInastance(context).getUser().getApi_token(), product.getProduct_id()), new HandleResponse() {
                    @Override
                    public void handleTrueResponse(JsonObject mainObject) {
                        ProductResponse productResponse = new Gson().fromJson(mainObject, ProductResponse.class);
                        if (productResponse.getStatus()) {
                            removeAt(position);
                            FavoriteFragment.hideLoading2();
                            new ProductRepository(context).updateFavoriteIcon(false, product.getProduct_id());

                        }
                    }

                    @Override
                    public void handleEmptyResponse() {
                        FavoriteFragment.hideLoading2();
                    }

                    @Override
                    public void handleConnectionErrors(String errorMessage) {
                        FavoriteFragment.hideLoading2();
                    }
                });
            }
        });
        alert.setNegativeButton(MainActivity.langResources.getString(R.string.cancel), null);
        alert.show();
    }

    public void removeAt(int position) {
        favoriteList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, favoriteList.size());
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


}
