package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.view.View;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class SearchHistoryViewHolder extends RecyclerView.ViewHolder {
    public TextView tvSearchTag;

    public SearchHistoryViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }

    private void initViews() {
        tvSearchTag = itemView.findViewById(R.id.tv_search_tag);
    }
}
