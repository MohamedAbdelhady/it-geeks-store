package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.graphics.Paint;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.ViewModels.ProductRepository;

public class CartViewHolder extends RecyclerView.ViewHolder {

    public TextView productTitle, productPrice, btnRemove, countDown, countUp , newProductPrice, oldProductPrice , productBrand;
    public ImageView productImage, btnAddToFavorite;
    public View lineH;
    public LinearLayout counter;
    public EditText counterProductNum;

    public CartViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }

    private void initViews() {
        productTitle = itemView.findViewById(R.id.product_title);
        productPrice = itemView.findViewById(R.id.product_price);
        btnRemove = itemView.findViewById(R.id.remove_from_cart);
        btnAddToFavorite = itemView.findViewById(R.id.favorite_cart_page);
        productImage = itemView.findViewById(R.id.product_image);
        productBrand = itemView.findViewById(R.id.product_brands);
        oldProductPrice = itemView.findViewById(R.id.old_product_price);
        newProductPrice = itemView.findViewById(R.id.new_product_price);
        lineH = itemView.findViewById(R.id.favorite_line_h);
        counter = itemView.findViewById(R.id.counter_product);
        countDown = itemView.findViewById(R.id.count_down);
        countUp = itemView.findViewById(R.id.count_up);
        counterProductNum = itemView.findViewById(R.id.counter_product_num);

        oldProductPrice.setPaintFlags(oldProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        // Visibility
        btnRemove.setVisibility(View.VISIBLE);
        btnAddToFavorite.setVisibility(View.VISIBLE);
        counter.setVisibility(View.VISIBLE);
        lineH.setVisibility(View.VISIBLE);


    }

}
