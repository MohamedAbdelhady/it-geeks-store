package itgeeks.info.itgeeksmarket.adapters.ViewHolders;

import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import itgeeks.info.itgeeksmarket.R;

public class RecentlyViewHolder extends RecyclerView.ViewHolder {

    public ImageView productImage;
    public TextView productTitle , newProductPrice ,oldProductPrice;

    public RecentlyViewHolder(@NonNull View itemView) {
        super(itemView);
        initViews();
    }

    private void initViews() {
        productImage = itemView.findViewById(R.id.best_product_image);
        productTitle = itemView.findViewById(R.id.best_product_title);
        newProductPrice = itemView.findViewById(R.id.new_product_price);
        oldProductPrice = itemView.findViewById(R.id.old_product_price);

        //  line over a Old Price
        oldProductPrice.setPaintFlags(oldProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

}
