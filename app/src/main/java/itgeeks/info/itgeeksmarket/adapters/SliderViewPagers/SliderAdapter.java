package itgeeks.info.itgeeksmarket.adapters.SliderViewPagers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import com.squareup.picasso.Picasso;
import itgeeks.info.itgeeksmarket.R;

import java.util.List;


public class SliderAdapter extends PagerAdapter {
    Context context;
    LayoutInflater LayoutInFlateR;
    List<String> sliderList;
    RecyclerView SliderDots;
    ImageView slideImageView;


    public SliderAdapter(Context context , List<String> sliderList ,RecyclerView sliderDots) {
        this.context = context;
        this.sliderList = sliderList;
        this.SliderDots = sliderDots;

    }


    @Override
    public int getCount() {
        return sliderList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup container, int position) {
        LayoutInFlateR = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View v = LayoutInFlateR.inflate(R.layout.item_slider,container,false);

        slideImageView = v.findViewById(R.id.image_slider);

        Picasso.with(context).load(sliderList.get(position)).resize(400,400).placeholder(R.drawable.progress_animation).into(slideImageView);

        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((FrameLayout)object);
    }
}
