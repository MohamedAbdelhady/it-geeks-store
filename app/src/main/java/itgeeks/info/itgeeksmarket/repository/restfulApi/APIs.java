package itgeeks.info.itgeeksmarket.repository.restfulApi;

import com.google.gson.JsonObject;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.RequestMainBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIs {

  @POST("master")
  Call<JsonObject> request(@Body RequestMainBody requestMainBody);
}