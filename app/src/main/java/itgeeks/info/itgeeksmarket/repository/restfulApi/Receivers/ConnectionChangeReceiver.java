package itgeeks.info.itgeeksmarket.repository.restfulApi.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import itgeeks.info.itgeeksmarket.R;
import itgeeks.info.itgeeksmarket.views.HomeOptions.CategoryProductsActivity;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import itgeeks.info.itgeeksmarket.views.MainFragements.HomeFragment;

public class ConnectionChangeReceiver extends BroadcastReceiver {

    Snackbar snackbar;


    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();

        initSnackBar(context);

        if (activeNetInfo != null) {
            connectedSnack(context);
        } else {
            unConnectedSnack(context);
        }
    }

    public void initSnackBar(Context context) {
        if (snackbar == null) {
            if (context.getClass().equals(MainActivity.class)) {
                snackbar = Snackbar.make(((MainActivity) context).getSnackBarContainer(), context.getString(R.string.no_connection), Snackbar.LENGTH_INDEFINITE);
            } else if (context.getClass().equals(CategoryProductsActivity.class)) {
                snackbar = Snackbar.make(((CategoryProductsActivity) context).getSnackBarContainer(), context.getString(R.string.no_connection), Snackbar.LENGTH_INDEFINITE);
            }

            View snackBarView = snackbar.getView();

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) snackBarView.getLayoutParams();
            params.setMargins(params.leftMargin, 0, params.rightMargin, 0);
            snackBarView.setLayoutParams(params);

            snackBarView.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));

            TextView tv = (TextView) snackBarView.findViewById(R.id.snackbar_text);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_size_small));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            } else {
                tv.setGravity(Gravity.CENTER_HORIZONTAL);
            }
        }
    }

    private void connectedSnack(Context context) {
        if (snackbar.isShown()) {
            snackbar.getView().setBackgroundColor(context.getResources().getColor(R.color.jade));
            snackbar.setText("CONNECTED");
            snackbar.setDuration(500).show();
            MainActivity.noInternet.setVisibility(View.GONE);
            MainActivity.btnTryAgain.setVisibility(View.GONE);
            MainActivity.menuFrame.setVisibility(View.VISIBLE);
            MainActivity.constrainLayout.setVisibility(View.VISIBLE);
            try {
                ((MainActivity) context).recreate();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void unConnectedSnack(Context context) {
        snackbar.getView().setBackgroundColor(context.getResources().getColor(R.color.red));
        snackbar.setText("NO CONNECTION");
        snackbar.setDuration(BaseTransientBottomBar.LENGTH_INDEFINITE).show();
        MainActivity.noInternet.setVisibility(View.VISIBLE);
        MainActivity.btnTryAgain.setVisibility(View.VISIBLE);
        MainActivity.menuFrame.setVisibility(View.GONE);
        MainActivity.constrainLayout.setVisibility(View.GONE);
    }
}
