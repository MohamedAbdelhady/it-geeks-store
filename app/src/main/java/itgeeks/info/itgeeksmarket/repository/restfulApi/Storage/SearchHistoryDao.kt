package itgeeks.info.itgeeksmarket.repository.restfulApi.Storage

import androidx.lifecycle.LiveData
import androidx.room.*
import itgeeks.info.itgeeksmarket.Models.product
import itgeeks.info.itgeeksmarket.Models.searchHistory

@Dao
interface SearchHistoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertProduct(searchHistory: searchHistory)

    @Delete
    abstract fun removeProduct(searchHistory: searchHistory)

    @Delete
    abstract fun removeSearchHistoriesList(searchHistory: List<searchHistory>)

    @Query("SELECT * FROM searchHistory")
    abstract fun getSearchHistory(): LiveData<List<searchHistory>>

    @Query("SELECT * FROM searchHistory WHERE id = :id")
    abstract fun getSearchHistoryByID(id: Int): LiveData<searchHistory>

    @Query("DELETE FROM searchHistory")
    abstract fun removeAllSearchHistory()
}