package itgeeks.info.itgeeksmarket.repository.restfulApi.Storage;

import android.content.Context;
import android.content.SharedPreferences;
import itgeeks.info.itgeeksmarket.Models.address;
import itgeeks.info.itgeeksmarket.Models.user;

public class SharedPrefranceManager {
    Context context;
    private static final String SHARED_PREF_USER = "user_itgeeks_store";
    private static final String SHARED_PREF_LANG = "lang_itgeeks_store";
    private static final String SHARED_PREF_ADDRESS_USER = "address_itgeeks_store_user";
    private static final String SHARED_PREF_ADDRESS_GUEST = "address_itgeeks_store_guest";
    private SharedPreferences sharedPreferences;

    private static SharedPrefranceManager sharedPrefranceManager;

    private SharedPrefranceManager(Context context) {
        this.context = context;
    }

    public synchronized static SharedPrefranceManager getInastance(Context context){
        if (sharedPrefranceManager == null){
            sharedPrefranceManager = new SharedPrefranceManager(context);
        }
        return sharedPrefranceManager;
    }

    public SharedPrefranceManager() {
    }

    //--------------- user -------------//
    public void saveUser(user user) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.clear();

        editor.putInt("userId", user.getUser_id());
        editor.putString("userToken", user.getApi_token());
        editor.putString("userEmail", user.getEmail());
        editor.putString("userName", user.getUsername());
        editor.putString("displayName", user.getDisplay_name());
        editor.putString("userImage", user.getImage());

        editor.putBoolean("userLogged", true);

        editor.apply();
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_USER, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("userLogged", false);
    }

    public user getUser() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_USER, Context.MODE_PRIVATE);
        return new user(
                sharedPreferences.getInt("userId", 0),
                sharedPreferences.getString("userToken", "0"),
                sharedPreferences.getString("userEmail", ""),
                sharedPreferences.getString("userName", ""),
                sharedPreferences.getString("displayName", ""),
                sharedPreferences.getString("userImage", "")
        );
    }
    public void clearUser() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("userLogged", false);
        editor.clear();
        editor.apply();
    }

    // ------------ Lang ------------- //
    public void saveLang(String lang) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_LANG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.putString("appLang", lang);
        editor.apply();
    }

    public String getLang() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_LANG, Context.MODE_PRIVATE);
        return sharedPreferences.getString("appLang", "en");
    }

    // ---------- Address For USER ---------- //
    public void saveAddess(address address) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_ADDRESS_USER +getInastance(context).getUser().getUser_id(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.clear();

        editor.putString("accountBillFirstName", address.getAccount_bill_first_name());
        editor.putString("accountBillLastName", address.getAccount_bill_last_name());
        editor.putString("accountBillEmailAddress", address.getAccount_bill_email_address());
        editor.putString("accountBillPhoneNumber", address.getAccount_bill_phone_number());
        editor.putString("accountBillAdddressLine1", address.getAccount_bill_address_line_1());
        editor.putString("accountShippingFirstName", address.getAccount_shipping_first_name());
        editor.putString("accountShippingLastName", address.getAccount_shipping_last_name());
        editor.putString("accountShippingEmailAddress", address.getAccount_shipping_email_address());
        editor.putString("accountShippingPhoneNumber", address.getAccount_shipping_phone_number());
        editor.putString("accountShippingAddressLine1", address.getAccount_shipping_address_line_1());


        editor.putBoolean("addressSaved", true);

        editor.apply();
    }

    public address getAddress() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_ADDRESS_USER +getInastance(context).getUser().getUser_id(), Context.MODE_PRIVATE);
        return new address(
                sharedPreferences.getString("accountBillFirstName", ""),
                sharedPreferences.getString("accountBillLastName", ""),
                sharedPreferences.getString("accountBillEmailAddress", ""),
                sharedPreferences.getString("accountBillPhoneNumber", ""),
                sharedPreferences.getString("accountBillAdddressLine1", ""),
                sharedPreferences.getString("accountShippingFirstName", ""),
                sharedPreferences.getString("accountShippingLastName", ""),
                sharedPreferences.getString("accountShippingEmailAddress", ""),
                sharedPreferences.getString("accountShippingPhoneNumber", ""),
                sharedPreferences.getString("accountShippingAddressLine1", "")
        );
    }

    public boolean isAddressSaved() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_ADDRESS_USER +getInastance(context).getUser().getUser_id(), Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("addressSaved", false);
    }

    public void clearAddress() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_ADDRESS_USER +getInastance(context).getUser().getUser_id(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }


    // ---------- Address For GUEST ---------- //
    public void saveAddessGuest(address address) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_ADDRESS_GUEST +getInastance(context).getUser().getUser_id(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.clear();

        editor.putString("accountBillFirstName", address.getAccount_bill_first_name());
        editor.putString("accountBillLastName", address.getAccount_bill_last_name());
        editor.putString("accountBillEmailAddress", address.getAccount_bill_email_address());
        editor.putString("accountBillPhoneNumber", address.getAccount_bill_phone_number());
        editor.putString("accountBillAdddressLine1", address.getAccount_bill_address_line_1());
        editor.putString("accountShippingFirstName", address.getAccount_shipping_first_name());
        editor.putString("accountShippingLastName", address.getAccount_shipping_last_name());
        editor.putString("accountShippingEmailAddress", address.getAccount_shipping_email_address());
        editor.putString("accountShippingPhoneNumber", address.getAccount_shipping_phone_number());
        editor.putString("accountShippingAddressLine1", address.getAccount_shipping_address_line_1());


        editor.putBoolean("addressSaved", true);

        editor.apply();
    }

    public address getAddressGuest() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_ADDRESS_GUEST +getInastance(context).getUser().getUser_id(), Context.MODE_PRIVATE);
        return new address(
                sharedPreferences.getString("accountBillFirstName", ""),
                sharedPreferences.getString("accountBillLastName", ""),
                sharedPreferences.getString("accountBillEmailAddress", ""),
                sharedPreferences.getString("accountBillPhoneNumber", ""),
                sharedPreferences.getString("accountBillAdddressLine1", ""),
                sharedPreferences.getString("accountShippingFirstName", ""),
                sharedPreferences.getString("accountShippingLastName", ""),
                sharedPreferences.getString("accountShippingEmailAddress", ""),
                sharedPreferences.getString("accountShippingPhoneNumber", ""),
                sharedPreferences.getString("accountShippingAddressLine1", "")
        );
    }

    public boolean isAddressGuestSaved() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_ADDRESS_GUEST +getInastance(context).getUser().getUser_id(), Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("addressSaved", false);
    }

    public void clearAddressGuest() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREF_ADDRESS_GUEST +getInastance(context).getUser().getUser_id(), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
