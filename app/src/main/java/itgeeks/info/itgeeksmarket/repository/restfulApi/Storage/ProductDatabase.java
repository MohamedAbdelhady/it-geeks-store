package itgeeks.info.itgeeksmarket.repository.restfulApi.Storage;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;
import itgeeks.info.itgeeksmarket.Models.product;
import itgeeks.info.itgeeksmarket.Models.recently;
import itgeeks.info.itgeeksmarket.Models.searchHistory;

@Database(entities = {product.class, recently.class, searchHistory.class}, version = 1, exportSchema = false)
public abstract class ProductDatabase extends RoomDatabase {

    private static ProductDatabase INSTANCE;
    private static final String DB_NAME = "Cart_Database.db";

    // singleton initialization
    public static ProductDatabase getInstance(Context application) {
//        application.getApplicationContext().deleteDatabase(DB_NAME); //<<<< ADDED To delete database before building Database.
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(application, ProductDatabase.class, DB_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration() // resolve this before release -> use migration <-
//                    .addMigrations()
                    .build();

        }
        return INSTANCE;
    }

    // DAOs
    public abstract ProductDao productDao();
    public abstract RecentlyDao recentlyDao();
    public abstract SearchHistoryDao searchHistoryDao();

}
