package itgeeks.info.itgeeksmarket.repository.restfulApi;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;

import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Data;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.Request;
import itgeeks.info.itgeeksmarket.repository.restfulApi.RequestBody.RequestMainBody;
import itgeeks.info.itgeeksmarket.views.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static itgeeks.info.itgeeksmarket.repository.restfulApi.ParseResponses.ParseErrors.parseServerErrors;

public class RetrofitClient {

    private static RetrofitClient mInstance;
    private Retrofit retrofit;

    private Call<JsonObject> call;

    private RetrofitClient() {
        this.retrofit = new Retrofit.Builder()
                // .client(new OkHttpClient.Builder().connectTimeout(20, TimeUnit.SECONDS).readTimeout(20, TimeUnit.SECONDS).writeTimeout(20, TimeUnit.SECONDS).build()) // time out
                .baseUrl(selectBaseUrl())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        // on creation
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    private String selectBaseUrl() {

        String BASE_URL = "https://bayaa.online/api/v1/";
//          String BASE_URL = "https://192.168.1.12/itgeeks-store/api/v1/";

        return BASE_URL;
    }

    public void executeConnectionToServer(Context context, String action, String lang, Request req, HandleResponse HandleResponse) {
        call = getInstance().getAPI().request(new RequestMainBody(new Data(action, lang), req));
        call.enqueue(createWebserviceCallback(HandleResponse, context));
    }

    public void executeConnectionToServerLoadMore(Context context, String action, String lang, int page, Request req, HandleResponse HandleResponse) {
        call = getInstance().getAPI().request(new RequestMainBody(new Data(action, lang, page), req));
        call.enqueue(createWebserviceCallback(HandleResponse, context));
    }

    private APIs getAPI() {
        return retrofit.create(APIs.class);
    }

    private Callback<JsonObject> createWebserviceCallback(final HandleResponse HandleResponse, final Context context) {
        return new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful()) { // code == 200

                    try {//
                        // dynamic with each call
                        HandleResponse.handleTrueResponse(response.body());

                        Log.d("body", response.body().toString());

                        // Connected Data
                        MainActivity.noInternet.setVisibility(View.GONE);
                        MainActivity.menuFrame.setVisibility(View.VISIBLE);
                        MainActivity.constrainLayout.setVisibility(View.VISIBLE);
                        MainActivity.btnTryAgain.setVisibility(View.GONE);

                    } catch (NullPointerException e) { // errors of response body 'maybe response body has changed';
                        Log.e("onResponse: ", e.getMessage());
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    } catch (UnsupportedOperationException e) {
                        Log.e("onResponse: ", e.getMessage());
                    }
                } else { // code != 200
                    try {
                        // Not Connected Data
                        MainActivity.noInternet.setVisibility(View.VISIBLE);
                        MainActivity.menuFrame.setVisibility(View.GONE);
                        MainActivity.constrainLayout.setVisibility(View.GONE);
                        MainActivity.btnTryAgain.setVisibility(View.VISIBLE);

                        JsonObject errorObj = new JsonParser().parse(response.errorBody().string()).getAsJsonObject();
                        String serverError = parseServerErrors(errorObj);


//                        // TODO: check codes instead of strings
//                        if (serverError.contains("not logged in") || serverError.contains("api token")) {
//                            context.startActivity(new Intent(context, ChooseLoginActivity.class)
//                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
//
//                            SharedPrefManager.getInstance(context).clearUser();
//                        }

                        // notify user
                        Toast.makeText(context, serverError, Toast.LENGTH_SHORT).show();

                        Log.d("!successful: ", serverError);
                        // dynamic with each call

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JsonSyntaxException e) {
                        e.printStackTrace();
                    } catch (RuntimeException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // dynamic with each call
                HandleResponse.handleEmptyResponse();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) { // connection errors
                Log.d("onFailure: ", t.getMessage() + "");
                // dynamic with each call
                // HandleResponse.handleConnectionErrors(context.getString(R.string.no_connection));
                HandleResponse.handleConnectionErrors(t.getMessage());

                // Not Connected Data
                MainActivity.noInternet.setVisibility(View.VISIBLE);
                MainActivity.menuFrame.setVisibility(View.GONE);
                MainActivity.constrainLayout.setVisibility(View.GONE);
                MainActivity.btnTryAgain.setVisibility(View.VISIBLE);
            }
        };
    }

    public void cancelCall() {
        if (call != null) {
            if (!call.isCanceled()) {
                call.cancel();
            }
        }
    }
}