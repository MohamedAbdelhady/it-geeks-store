package itgeeks.info.itgeeksmarket.repository.restfulApi.Storage

import androidx.lifecycle.LiveData
import androidx.room.*
import itgeeks.info.itgeeksmarket.Models.recently

@Dao
interface RecentlyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertProduct(product: recently)

    @Delete
    abstract fun removeProduct(recently: recently)

    @Delete
    abstract fun removeProductsList(recently: List<recently>)

    @Query("SELECT * FROM recently")
    abstract fun getProducts(): LiveData<List<recently>>

    @Query("SELECT * FROM recently WHERE product_id = :productID")
    abstract fun getProductByID(productID: Int): LiveData<recently>

    @Query("UPDATE recently SET isWished = :isWished WHERE product_id = :productID; ")
    abstract fun updateFavoriteIcon(isWished: Boolean, productID: Int)

    @Query("UPDATE recently SET quantity = :productCount WHERE product_id = :productID; ")
    abstract fun updateProductCount(productCount : Int, productID: Int)
}