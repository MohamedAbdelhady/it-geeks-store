package itgeeks.info.itgeeksmarket.repository.restfulApi.Storage

import androidx.lifecycle.LiveData
import androidx.room.*
import itgeeks.info.itgeeksmarket.Models.product

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertProduct(product: product)

    @Delete
    abstract fun removeProduct(product: product)

    @Delete
    abstract fun removeProductsList(product: List<product>)

    @Query("SELECT * FROM product")
    abstract fun getProducts(): LiveData<List<product>>

    @Query("SELECT * FROM product WHERE product_id = :productID")
    abstract fun getProductByID(productID: Int): LiveData<product>

    @Query("UPDATE product SET isWished = :isWished WHERE product_id = :productID; ")
    abstract fun updateFavoriteIcon(isWished: Boolean, productID: Int)

    @Query("UPDATE product SET quantity = :productCount WHERE product_id = :productID; ")
    abstract fun updateProductCount(productCount : Int, productID: Int)
}