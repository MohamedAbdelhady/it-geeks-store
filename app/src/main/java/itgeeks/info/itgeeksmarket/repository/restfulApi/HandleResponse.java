package itgeeks.info.itgeeksmarket.repository.restfulApi;

import com.google.gson.JsonObject;

public interface HandleResponse {

    void handleTrueResponse(JsonObject mainObject);

    void handleEmptyResponse();

    void handleConnectionErrors(String errorMessage);

}
